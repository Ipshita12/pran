package com.prangroup.thirdEyePlus.Utils;

public interface DirectionListener {
    public void onDirectionsData(DirectionUtils utils);
}
