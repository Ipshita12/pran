package com.prangroup.thirdEyePlus.Utils;

public interface ConnectionCallback {
    void hasActiveConnection(boolean hasActiveConnection);
}
