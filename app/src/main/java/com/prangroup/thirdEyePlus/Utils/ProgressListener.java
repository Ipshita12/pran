package com.prangroup.thirdEyePlus.Utils;

public interface ProgressListener {
    public void onProgressChanged(boolean state);
}
