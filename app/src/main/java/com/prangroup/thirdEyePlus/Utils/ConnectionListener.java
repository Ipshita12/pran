package com.prangroup.thirdEyePlus.Utils;

interface ConnectionListener {
    void onWifiTurnedOn();
    void onWifiTurnedOff();
}
