package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;


public class UserMenuFragment extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    public static Context mContext;
    public static Activity mActivity;
    Fragment fragment;
    private OnFragmentInteractionListener mListener;
    private ConnectionDetector connectionDetector;
    public static LinearLayout lllt,llrt,llir,llvir,llcc,llbidreq;
    public UserMenuFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static UserMenuFragment newInstance(String param1, String param2) {
        UserMenuFragment fragment = new UserMenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        rootView=inflater.inflate(R.layout.fragment_vehin, container, false);
        String menutxt = getResources().getString(R.string.menu_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+menutxt+" </font>"));

        initialization();

        lllt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment=new LiveFragment();
                redirect(fragment);
            }
        });





        return rootView;
    }



    private void initialization() {
        mContext=getContext();
        mActivity=getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);
        lllt=(LinearLayout) rootView.findViewById(R.id.lllt);


    }

    private void redirect(Fragment fragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
