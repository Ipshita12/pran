package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Model.VehicleUnloadListDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.ApiLinks;
import com.prangroup.thirdEyePlus.ServerOperation.LoadUnloadUtilitiesSearchOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;

public class LoadUnloadEndFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private View rootView;
    private OnFragmentInteractionListener mListener;
    public Context mContext;
    public Activity mActivity;
    Fragment fragment;
    private ConnectionDetector connectionDetector;
    private ListView listView;
    private EditText etSearch;

    public LoadUnloadEndFragment() {
        // Required empty public constructor
    }

    public static LoadUnloadEndFragment newInstance(String param1, String param2) {
        LoadUnloadEndFragment fragment = new LoadUnloadEndFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_load_unload_end, container, false);
        String menu_txt = getResources().getString(R.string.str_load_unload_end);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#01396C'>" + menu_txt + " </font>"));

        MainActivity.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.back_btn), PorterDuff.Mode.SRC_ATOP);

        initialization();

        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);

        listView = rootView.findViewById(R.id.listViewUnload);
        etSearch = rootView.findViewById(R.id.etSearch);

        final LoadUnloadEndListAdapter listAdapter = new LoadUnloadEndListAdapter(MenuFragment.vehicleUnloadListData);
        listView.setAdapter(listAdapter);

        etSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int start, int before,
                                      int count) {
                String text = etSearch.getText().toString().toLowerCase(Locale.getDefault());
                listAdapter.filter(text);
//                listAdapter.getCount();
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    class LoadUnloadEndListAdapter extends BaseAdapter {
        LayoutInflater mInflater;
        ArrayList<VehicleUnloadListDataModel> myList;
        private ArrayList<VehicleUnloadListDataModel> privateArray;

        public LoadUnloadEndListAdapter(ArrayList<VehicleUnloadListDataModel> vehicleUnloadMyListData) {
            this.myList = new ArrayList<>();
            this.myList = vehicleUnloadMyListData;
            mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            privateArray = new ArrayList<>();
            privateArray.addAll(myList);
        }

        @Override
        public int getCount() {
            return myList.size();//listview item count.
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder vh;

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.load_unload_end_list_row_layout, parent, false);
                //inflate custom layour
                vh = new ViewHolder();

                vh.tvRegNum = convertView.findViewById(R.id.tvRegNum);
                vh.tvLocationBuilding = convertView.findViewById(R.id.tvLocationBuilding);
                vh.tvLoadingBay = convertView.findViewById(R.id.tvLoadingBay);
                vh.tvTime = convertView.findViewById(R.id.tvTime);
                vh.btnCross = convertView.findViewById(R.id.btnCross);

                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();
            }

            vh.tvRegNum.setText(MenuFragment.vehicleUnloadListData.get(position).getRegistration_number());
            vh.tvLocationBuilding.setText(MenuFragment.vehicleUnloadListData.get(position).getLocation_building_name());
            vh.tvLoadingBay.setText(MenuFragment.vehicleUnloadListData.get(position).getLoading_bay_name());
            vh.tvTime.setText(MenuFragment.vehicleUnloadListData.get(position).getUser_type()
                    + " started at: " + MenuFragment.vehicleUnloadListData.get(position).getStart_date_time());

            vh.btnCross.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    boolean res = connectionDetector.isConnectedToInternet();
                    if (res) {
//                        int pk_loading_bay_utilization = MenuFragment.vehicleUnloadListData.get(position).getPk_loading_bay_utilization();
//                        handleLoadUnloadEndSearch(pk_loading_bay_utilization);
                        showCustomDialogConfirm(position);
                    } else {
                        HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                    }
                }
            });
            return convertView;
        }

        class ViewHolder {
            TextView tvRegNum, tvLocationBuilding, tvLoadingBay, tvTime;
            ImageView btnCross;
        }

        // Filter Class
        public void filter(String charText) {

            charText = charText.toLowerCase(Locale.getDefault());
            myList.clear();
            if (charText.length() == 0) {
                myList.addAll(privateArray);
            } else {
                for (VehicleUnloadListDataModel v : privateArray) {
                    if (v.getRegistration_number().toLowerCase().contains(charText)) {
                        myList.add(v);
                    } else if (v.getLocation_building_name().toLowerCase().contains(charText)) {
                        myList.add(v);
                    } else if (v.getLoading_bay_name().toLowerCase().contains(charText)) {
                        myList.add(v);
                    }
                }
            }
            notifyDataSetChanged();


        }
    }

    public void handleLoadUnloadEndSearch(int pk_loading_bay_utilization) {
        String pk_user = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_id, "user not found");

        String version = "" + MainActivity.versionCode;
        String bay = "" + pk_loading_bay_utilization;

        LoadUnloadUtilitiesSearchOperation.getLoadUnloadEndSearchData(mActivity, version, bay, pk_user, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getLoadData) {
                try {
                    String status = getLoadData.getString("flag");
                    if (status.equalsIgnoreCase("success")) {
                        showCustomDialogSuccess();
                    } else {
                        String msg = getLoadData.getString("message");
                        showCustomDialogError(msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void showCustomDialogConfirm(int dataPosition) {
        final int position = dataPosition;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_confirm, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvConfirmMsg);
        tvMsg.setText("Are you sure you want to end?");

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                int pk_loading_bay_utilization = MenuFragment.vehicleUnloadListData.get(position).getPk_loading_bay_utilization();
                handleLoadUnloadEndSearch(pk_loading_bay_utilization);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.buttonCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }

    private void showCustomDialogSuccess() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_success, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText("এন্ট্রি সফল হয়েছে।");

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                fragment = new MenuFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment);
                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }

    private void showCustomDialogError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_error, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText(message);

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
//                fragment = new MenuFragment();
//                FragmentManager fm = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fm.beginTransaction();
//                fragmentTransaction.replace(R.id.frameLayout, fragment);
//                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }

}