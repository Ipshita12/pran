package com.prangroup.thirdEyePlus.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.prangroup.thirdEyePlus.Adapter.AutoCompleteAdapter;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DefaultFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DefaultFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DefaultFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    public static ArrayList<String>vehicleCapacityNameFilterdDB=new ArrayList<String>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    public static Button tvdestination_point, tvsource_point,setAsPickPoint,setAsDestination;
    MapView mMapView;
    public static GoogleMap mMap;
    public static Context context;
    public static Activity activity;
    //VehicleTypeSpinnerAdapter aVehicleTypeSpinnerAdapter, aVehicleCapacitySpinnerAdapter, aVehiclelngthSpinnerAdapter,aVehicleNameSpinnerAdapter, aVehicledriverNameSpinnerAdapter;
    public static TextView tvgenerate, tvcreate, tvwhen, tvInstant, tvIndividual, tvadditionaldepo, tvadditionaldepolist, tvdepolistset;
    public static EditText etmobno, etquantity, etagentmoney, etdrivermoney, etnote,txtMobileNo;
    public static Spinner spvt, spvc,spvl, spvehicle, spdriver;
    public static LinearLayout indLayout, insLayout, indinsLayout;
    public static AlertDialog.Builder dialogBuilderr, calDialogBuilderr, timeDialogBuilderr, adDepoDialogBuilderr;
    public static AlertDialog searchAlertDialogg, calendarAlertDialogg, timeAlertDialogg, adDepoAlertDialogg;
    public static String PICUPTITLE = "Pickup location", DESTINATIONLOCATION = "Destination Location", dateTime, time, pmam, depolist = "";
    LatLng camCurPos;
    private static final int ZOOMLVL = 15;
    int animationSourceBottomToBottom;
    public static TextView tvgps, tvdepolist, tvGoogleMap;
    public static RadioGroup radioGroup;
    double lat, lng;
    String address = "";
    ImageView pickContact;
    final int RESULT_PICK_CONTACT=100;
    AutoCompleteTextView pickdepolistauto, desdepolistauto, additionaldepolistauto;
    public static AutoCompleteAdapter lAdapter;
    public static ImageView imgPointer;
    public static String startingLatitude, startingLongitude, startingLatLongText, finishingLatitude, finishingLongitude, finishingLatLongText;
    public static String vehiclename = "", drivername = "", note = "", agentmoney = "", drivermoney = "", fieldName;
    public static String vehicleType = "", vehicleCap = "", quantity = "", when = "", mob = "", additionalDepoIds = "no depo", depoIdFrom = "no", depoIdTo = "no";
    public static RadioButton radioOptionOne, radioOptionTwo;
    private FusedLocationProviderClient mFusedLocationClient;
    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    Bitmap pickImageBitmap, destImageBitmap;
    Bitmap pickResizedBitmap, destResizedBitmap;
    public static LatLng pickloc, destloc;
    int action, mapClickAction;
    public static ArrayList<Marker> picMarkersDB, desMarkersDB;
    public static String l1,l2,l3;
    public LinearLayout lineersource_point, lineerdestination_point;
    public RelativeLayout phoneNoLayout;
    private OnFragmentInteractionListener mListener;
    //public static ArrayList<VehicleCapacityListModel>vehicleCapacityDBLocal=new ArrayList<VehicleCapacityListModel>();
    //public static ArrayList<VehicleLengthListModel>vehicleLengthDBLocal=new ArrayList<VehicleLengthListModel>();
    public DefaultFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static DefaultFragment newInstance(String param1, String param2) {
        DefaultFragment fragment = new DefaultFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_default, container, false);


        initialization();

        tvsource_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //HelpingLib.showmessage(getContext(),"source");

            }
        });
        tvdestination_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //HelpingLib.showmessage(getContext(),"destination");
                imgPointer.setVisibility(View.GONE);
                setAsPickPoint.setVisibility(View.GONE);
                if (pickloc==null){
                    String pickpointErrTxt = context.getString(R.string.pickpoint_err_txt);
                    HelpingLib.showmessage1(context,pickpointErrTxt);
                }else {
                    if (MainActivity.userType.equalsIgnoreCase("User_InternalAgent")){

                    }else if (MainActivity.userType.equalsIgnoreCase("TransportAgent")){
                        //destinationDialog();
                        dialogBuilderr = new AlertDialog.Builder(getContext());
                        imgPointer.setVisibility(View.VISIBLE);
                        setAsDestination.setVisibility(View.VISIBLE);
                        action=3;
                        mapClickAction=2;

                    }else  if (MainActivity.userType.equalsIgnoreCase("EndLevelRequisitionAgent")){
                        //destinationDialog();
                        dialogBuilderr = new AlertDialog.Builder(getContext());
                        imgPointer.setVisibility(View.VISIBLE);
                        setAsDestination.setVisibility(View.VISIBLE);
                        action=3;
                        mapClickAction=2;

                    }

                }

            }
        });

        pickloc = null;
        destloc = null;
        context = getContext();
        activity = getActivity();
        //DepoListOperation.sendData(activity, context);

        picMarkersDB = new ArrayList<Marker>();
        desMarkersDB = new ArrayList<Marker>();
        action = 1;
        String menutxt = getResources().getString(R.string.menu_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + menutxt + " </font>"));

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();
        MapsInitializer.initialize(getActivity().getApplicationContext());
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                mMap = googleMap;
                action=2;


                googleMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
                    @Override
                    public void onCameraIdle() {
                         camCurPos=mMap.getCameraPosition().target;
                    }
                });

            }
        });
    setAsPickPoint.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            action=2;
            double lat=camCurPos.latitude;
            double lng=camCurPos.longitude;

            for (int i=0;i<picMarkersDB.size();i++){
                picMarkersDB.get(i).remove();

            }
            pickloc=camCurPos;

            //String address=HelpingLib.getAddressByLatLng(context,lat,lng);
            tvsource_point.setText(address);
            startingLatitude=""+camCurPos.latitude;
            startingLongitude=""+camCurPos.longitude;
            startingLatLongText=address;
            //String add=HelpingLib.getAddressByLatLng(context,lat,lng);
            //Log.e("camCurPos",""+camCurPos+"  "+add);
        }
    });

        setAsDestination.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                action=3;
                double lat=camCurPos.latitude;
                double lng=camCurPos.longitude;

                for (int i=0;i<desMarkersDB.size();i++){
                    desMarkersDB.get(i).remove();

                }
                //getLatLngInfoFromGmap

//                tvdestination_point.setText(HelpingLib.getAddressByLatLng(context,lat,lng));
//                String add=HelpingLib.getAddressByLatLng(context,lat,lng);
//                finishingLatitude=""+camCurPos.latitude;
//                finishingLongitude=""+camCurPos.longitude;
//                finishingLatLongText=add;
//                Log.e("camCurPos",""+camCurPos+"  "+add);

            }
        });
        return rootView;
    }
    private void initialization() {
        radioGroup = (RadioGroup) rootView.findViewById(R.id.radioOption);
        radioOptionOne = (RadioButton) rootView.findViewById(R.id.radioOptionOne);
        radioOptionTwo = (RadioButton) rootView.findViewById(R.id.radioOptionTwo);
//        radioOptionOne.setTag(1);
//        radioOptionTwo.setTag(2);
        tvsource_point = (Button) rootView.findViewById(R.id.tvsource_point);
        tvdestination_point = (Button) rootView.findViewById(R.id.tvdestination_point);
        setAsPickPoint= (Button) rootView.findViewById(R.id.setAsPickPoint);
        setAsDestination= (Button) rootView.findViewById(R.id.setAsDestination);
        imgPointer= (ImageView) rootView.findViewById(R.id.imgPointer);

        lineersource_point = (LinearLayout) rootView.findViewById(R.id.lineersource_point);
        lineerdestination_point = (LinearLayout) rootView.findViewById(R.id.lineerdestination_point);

        mMapView = (MapView) rootView.findViewById(R.id.mapView);



    }


















    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onFragmentInteraction(uri);
//        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

}
