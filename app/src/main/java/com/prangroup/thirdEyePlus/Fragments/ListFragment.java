package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.SearchView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;

import static com.prangroup.thirdEyePlus.Default.MainActivity.fragmentManager;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    public Context mContext;
    public Activity mActivity;
    private View rootView;
    private VisitorCreateFragment.OnFragmentInteractionListener mListener;
    SearchView searchView;
    ListView listView;
    private static ArrayAdapter adapter;

    public ListFragment() {
        // Required empty public constructor
    }

    public static ListFragment newInstance(String param1, String param2) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView =  inflater.inflate(R.layout.fragment_list, container, false);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + VisitorCreateFragment.menuName + " </font>"));
        initialization();
        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof VisitorCreateFragment.OnFragmentInteractionListener) {
            mListener = (VisitorCreateFragment.OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        searchView = rootView.findViewById(R.id.searchBar);
        listView = rootView.findViewById(R.id.list_item);

        if(VisitorCreateFragment.selectorType == 1) {
            adapter = new ArrayAdapter(mActivity, R.layout.spinner_list, MenuFragment.companyArrayList);
        } else if(VisitorCreateFragment.selectorType == 2) {
            adapter = new ArrayAdapter(mActivity, R.layout.spinner_list, MenuFragment.departmentArrayList);
        } else {
            adapter = new ArrayAdapter(mActivity, R.layout.spinner_list, MenuFragment.purposeArrayList);
        }
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = adapterView.getItemAtPosition(i).toString();
                if(VisitorCreateFragment.selectorType == 1) {

                    for(int j = 0; j < MenuFragment.companyListData.size(); j++) {
                       if(MenuFragment.companyListData.get(j).getName().equals(txt)) {
                           VisitorCreateFragment.companyId = MenuFragment.companyListData.get(j).getId();
                           VisitorCreateFragment.companyStr = txt;
                           break;
                       }
                    }
                } else if(VisitorCreateFragment.selectorType == 2) {

                    for(int j = 0; j < MenuFragment.deptListData.size(); j++) {
                        if(MenuFragment.deptListData.get(j).getName().equals(txt)) {
                            VisitorCreateFragment.departmentId = MenuFragment.deptListData.get(j).getId();
                            VisitorCreateFragment.departmentStr = txt;
                            break;
                        }
                    }
                } else {

                    for(int j = 0; j < MenuFragment.purposeListData.size(); j++) {
                        if(MenuFragment.purposeListData.get(j).getName().equals(txt)) {
                            VisitorCreateFragment.purposeId = MenuFragment.purposeListData.get(j).getId();
                            VisitorCreateFragment.purposeStr = txt;
                            break;
                        }
                    }
                }

                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                if (fragmentManager == null) return;
                fragmentManager.popBackStack();
                Bundle bundle = new Bundle();
                bundle.putString("bundle_filter", txt);
                fragmentManager.findFragmentByTag("Fragment 2 here").setArguments(bundle);
            }
        });

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                adapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                adapter.getFilter().filter(newText);
                return false;
            }
        });
    }
}