package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.ApiLinks;
import com.prangroup.thirdEyePlus.ServerOperation.VisitorExitSearchOperation;
import com.prangroup.thirdEyePlus.ServerOperation.VisitorInfoSearchOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;

public class VisitorExitFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private View rootView;
    private OnFragmentInteractionListener mListener;

    public static Context mContext;
    public static Activity mActivity;
    Fragment fragment;
    private ConnectionDetector connectionDetector;

    String data_array[];
    private ListView listView;

    public VisitorExitFragment() {
        // Required empty public constructor
    }

    public static VisitorExitFragment newInstance(String param1, String param2) {
        VisitorExitFragment fragment = new VisitorExitFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_visitor_exit, container, false);
        String menu_txt = getResources().getString(R.string.str_visitor_exit_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + menu_txt + " </font>"));

        initialization();

        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        connectionDetector = new ConnectionDetector(MainActivity.context);

        listView = rootView.findViewById(R.id.listViewVisitorExitList);
        ExitListAdapter listAdapter = new ExitListAdapter();
        listView.setAdapter(listAdapter);
    }

    class ExitListAdapter extends BaseAdapter {
        LayoutInflater mInflater;

        public ExitListAdapter() {
            mInflater = (LayoutInflater) mActivity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @Override
        public int getCount() {
            return MenuFragment.visitorListData.size();//listview item count.
        }

        @Override
        public Object getItem(int position) {
            return position;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            ViewHolder vh;


            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.exit_list_row_layout, parent, false);
                //inflate custom layour
                vh = new ViewHolder();

                vh.tvName = convertView.findViewById(R.id.etVisitorName);
                vh.tvPhn = convertView.findViewById(R.id.etVisitorPhn);
                vh.tvTime = convertView.findViewById(R.id.etVisitorTime);
                vh.imageView = convertView.findViewById(R.id.ivUser);
                vh.btnExit = convertView.findViewById(R.id.buttonExit);
                vh.linearLayout = convertView.findViewById(R.id.linearLayoutRow);

                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();
            }

            Picasso.get().load(ApiLinks.base_visitor + MenuFragment.visitorListData.get(position).getImage())
                    .into(vh.imageView);
            vh.tvName.setText(MenuFragment.visitorListData.get(position).getName());
            vh.tvPhn.setText("Contact:\n" + MenuFragment.visitorListData.get(position).getPhn());

            if(MenuFragment.visitorListData.get(position).getOutTime().equalsIgnoreCase("null")) {
                vh.tvTime.setText("In Time:\n" + MenuFragment.visitorListData.get(position).getInTime());
                //set text of second textview based on position
                vh.btnExit.setVisibility(View.VISIBLE);
                vh.btnExit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        boolean res = HelpingLib.isInternetConnected(mContext);
                        if (res) {
                            showCustomDialogConfirm(position);
                        } else {
                            HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                        }
                    }
                });
                vh.linearLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.white));
            } else {
                vh.tvTime.setText("In Time:\n" + MenuFragment.visitorListData.get(position).getInTime()
                 + "\nOut Time:\n" + MenuFragment.visitorListData.get(position).getOutTime());
                vh.btnExit.setVisibility(View.GONE);
                vh.linearLayout.setBackgroundColor(ContextCompat.getColor(getActivity(), R.color.disabled));
            }
            return convertView;
        }

        class ViewHolder {
            TextView tvName, tvPhn, tvTime;
            Button btnExit;
            ImageView imageView;
            CardView linearLayout;
        }
    }

    private void handleVisitorExit(int position) {
        String location = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.location, "location not found");
        String id = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_id, "user not found");

        VisitorExitSearchOperation.sendVisitorExitData(mActivity, position, location, id, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getVisitorLogData) {
                        try {
                            String outTime = getVisitorLogData.getString("visitor_out_time");
                            String updatedMsg = getVisitorLogData.getString("update");

                            showCustomDialog(outTime);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void showCustomDialog(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        //before inflating the custom alert dialog layout, we will get the current activity viewgroup
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        //then we will inflate the custom alert dialog xml that we created
        View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_success, viewGroup, false);
        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();
        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText("Visitor exit request submitted.\nExit time: " + msg);
        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

                fragment = new MenuFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment);
                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }

    private void showCustomDialogConfirm(int dataPosition) {
        final int position = dataPosition;
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_confirm, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvConfirmMsg);
        tvMsg.setText("Are you sure you want to exit?");

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                handleVisitorExit(position);
            }
        });

        Button btnCancel = dialogView.findViewById(R.id.buttonCancel);
        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();

            }
        });
        alertDialog.show();
    }
}