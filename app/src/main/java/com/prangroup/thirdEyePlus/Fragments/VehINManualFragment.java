package com.prangroup.thirdEyePlus.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.zxing.Result;
import com.prangroup.thirdEyePlus.DataHandler.HandleINManReasonJsonData;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Default.MiddleWearUserSelectActivity;
import com.prangroup.thirdEyePlus.Model.VehINReassonDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.GetVechileGroupData;
import com.prangroup.thirdEyePlus.ServerOperation.VehGetInOperation;
import com.prangroup.thirdEyePlus.ServerOperation.VehGetInSearchOperation;
import com.prangroup.thirdEyePlus.ServerOperation.VehUtilityOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;


public class VehINManualFragment extends Fragment implements ZXingScannerView.ResultHandler {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    public static Context mContext;
    public static Activity mActivity;
    Fragment fragment;
    private OnFragmentInteractionListener mListener;
    private ConnectionDetector connectionDetector;
    public static LinearLayout lllt, ll2, vehsel;
    Button btnsubmit;
    Button btn_search;
    TextView tvresult;
    public static String disval, metroval = "", regval, veh1, veh2, fullvehname, resval, resval2, loadstatus;
    public static String scanresult, SearchText, loadingStatus, datetime, IsFormatedRegistrationNumber = "no";
    public EditText etveh1, etveh2, edt_search;

    Spinner spload, spinreason;
    Spinner sp1, sp11, sp2, sp3, spGroup;
    ArrayAdapter<String> resonAdapter, loadAdapter, adt1, adt11, adt2, adt3;
    public static ArrayList<VehINReassonDataModel> vehInReassonDB = new ArrayList<VehINReassonDataModel>();
    public static ArrayList<String> vehInReassonNameDB = new ArrayList<String>();
    public static ArrayList<String> vehInloadingDB = new ArrayList<String>();
    List<String> list1, list11, list2, list3;
    public static String fullvehname_11;
    SharedPreferences appData;
    public static String v_type_id;
    public static String v_type_name;
    ArrayAdapter dataAdapter;
    public ArrayList<String> V_TYPE_ID = new ArrayList<String>();
    public ArrayList<String> V_TYPE_NAME = new ArrayList<String>();

    public VehINManualFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static VehINManualFragment newInstance(String param1, String param2) {
        VehINManualFragment fragment = new VehINManualFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        rootView = inflater.inflate(R.layout.fragment_vehinman, container, false);
        String menutxt = "Get In(Manually)";
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + menutxt + " </font>"));

        initialization();
        arraylist();

        fullvehname_11 = "";

        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 1);
        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

      /*  appData = PreferenceManager.getDefaultSharedPreferences(getActivity());
        v_type_id = appData.getString("v_type_id", "");
        v_type_name = appData.getString("v_type_name", "");*/

        setAdapterVa();
        getUtilitydata();

        btnsubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());

                if (fullvehname_11.equalsIgnoreCase("সিলেক্ট করুন")) {
                    HelpingLib.showmessage1(mContext, "গাড়ির নম্বর সঠিক নয়");
                } else {

                    if (!fullvehname_11.equalsIgnoreCase("")) {
                        fullvehname = fullvehname_11;
                        boolean res = HelpingLib.isInternetConnected(mContext);
                        if (res) {
                            if (loadstatus.equalsIgnoreCase("সিলেক্ট করুন")) {
                                HelpingLib.showmessage1(mContext, "লোডের অবস্থা নির্বাচন করুন");
                            } else if (resval2.equalsIgnoreCase("সিলেক্ট করুন")) {
                                HelpingLib.showmessage1(mContext, "লোডের কারণ নির্বাচন করুন");
                            } else if (v_type_id.equalsIgnoreCase("0")) {
                                HelpingLib.showmessage1(mContext, "গাড়ি যাবে কোথায় নির্বাচন করুন");
                            } else {
                                sendDataToServer();
                            }
                        } else {
                            HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                        }
                    } else {
                        IsFormatedRegistrationNumber = "no";
                        if (IsFormatedRegistrationNumber.equalsIgnoreCase("no")) {
                            veh1 = etveh1.getText().toString();
                            veh2 = etveh2.getText().toString();
                            int lng1 = veh1.length();
                            int lng2 = veh2.length();
                            if (lng1 < 2) {
                                HelpingLib.showmessage1(mContext, "গাড়ির নম্বর সঠিক নয়");
                            } else {
                                fullvehname = disval + " " + metroval + "-" + regval + "-" + veh1 + "-" + veh2;
                            }
                            if (lng2 < 4) {
                                HelpingLib.showmessage1(mContext, "গাড়ির নম্বর সঠিক নয়");

                            } else {
                                if (loadstatus.equalsIgnoreCase("সিলেক্ট করুন")) {
                                    HelpingLib.showmessage1(mContext, "লোডের অবস্থা নির্বাচন করুন");
                                } else if (resval2.equalsIgnoreCase("সিলেক্ট করুন")) {
                                    HelpingLib.showmessage1(mContext, "লোডের কারণ নির্বাচন করুন");
                                } else if (v_type_id.equalsIgnoreCase("0")) {
                                    HelpingLib.showmessage1(mContext, "গাড়ি যাবে কোথায় নির্বাচন করুন");
                                } else {
                                    fullvehname = disval + " " + metroval + "-" + regval + "-" + veh1 + "-" + veh2;
                                    boolean res = HelpingLib.isInternetConnected(mContext);
                                    if (res) {
                                        sendDataToServer();
                                    } else {
                                        HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                                    }
                                }
                            }
                        } else {
                            fullvehname = scanresult;
                        }
                    }
                }
            }
        });

        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                // veh1 = etveh1.getText().toString();
                // veh2 = etveh2.getText().toString();
                SearchText = edt_search.getText().toString();
                //  int lng1 = veh1.length();
                //  int lng2 = veh2.length();
                int lng11 = SearchText.length();
                // fullvehname = disval + " " + metroval + "-" + regval + "-" + veh1 + "-" + veh2;
                fullvehname_11 = "";
                boolean res = HelpingLib.isInternetConnected(mContext);
                if (res) {
                    if (lng11 < 4) {
                        HelpingLib.showmessage1(mContext, "সর্বনিম্ন 4 অঙ্ক লিখুন");
                    } else {
                        sendDataToServer_Search(SearchText);
                    }
                } else {
                    HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                }
            }
        });


        return rootView;
    }

    private void sendDataToServer_Search(String SearchText) {
        VehGetInSearchOperation.sendGetInData(IsFormatedRegistrationNumber, SearchText, MainActivity.userid,
                datetime, mActivity, mContext, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getVchLocData) {
                        Log.e("Rs SearchText 1 ", getVchLocData.toString());
                        try {
                            String flag = getVchLocData.getString("flag");
                            Fragment fragment = new MenuFragment();

                            Log.e("Rs flag 10 ", flag.toString());

                            if (flag.equalsIgnoreCase("found")) {

                                list11 = new ArrayList<String>();
                                list11.add("সিলেক্ট করুন");
                                ll2.setVisibility(View.VISIBLE);
                                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ll2.getLayoutParams();
                                lp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                                ll2.setLayoutParams(lp);

                                vehsel.setVisibility(View.INVISIBLE);
                                LinearLayout.LayoutParams lpp = (LinearLayout.LayoutParams) vehsel.getLayoutParams();
                                lpp.height = 0;
                                vehsel.setLayoutParams(lpp);

                                JSONArray userArray = getVchLocData.getJSONArray("list");
                                Log.e("Rs SearchText list 2 ", userArray.toString());
                                int size = userArray.length();
                                for (int i = 0; i < size; i++) {
                                    JSONObject finalJsonData = userArray.getJSONObject(i);
                                    String RegistrationNumber1 = finalJsonData.getString("RegistrationNumber");

                                    list11.add(RegistrationNumber1);
                                    Log.e("Rs RegistrationNumber 3", RegistrationNumber1.toString());
                                }

                                set_list11Adapter();

                            } else if (flag.equalsIgnoreCase("not_found")) {

                                ll2.setVisibility(View.INVISIBLE);
                                LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ll2.getLayoutParams();
                                lp.height = 0;
                                ll2.setLayoutParams(lp);
                                vehsel.setVisibility(View.VISIBLE);
                                LinearLayout.LayoutParams lpp = (LinearLayout.LayoutParams) vehsel.getLayoutParams();
                                lpp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                                vehsel.setLayoutParams(lpp);
                                IsFormatedRegistrationNumber = "no";

                                String errmessage = ("কোন গাড়ি পাওয়া যায় নি");
                                HelpingLib.showmessage1(mContext, errmessage);
                                // redirect(fragment);
                            } else {

                                HelpingLib.showmessage1(mContext, "কোন গাড়ি পাওয়া যায় নি");
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Fragment fragment=new MenuFragment();
                        //redirect(fragment);
                    }
                });
    }

    public static int versionCode;
    public static String versionName;

    private void sendDataToServer() {
        try {
            final PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = (int) pInfo.getLongVersionCode(); // avoid huge version numbers and you will be ok
                versionName = pInfo.versionName;
            } else {
                //noinspection deprecation
                versionCode = pInfo.versionCode;
                versionName = pInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        VehGetInOperation.sendGetInData(IsFormatedRegistrationNumber, fullvehname, MainActivity.userid,
                resval, loadstatus, datetime, mActivity, mContext, v_type_id, versionCode, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getVchLocData) {
                        Log.e("getVchLocData", getVchLocData.toString());
                        try {
                            String flag = getVchLocData.getString("flag");
                            String message = getVchLocData.getString("message");
                            Fragment fragment = new MenuFragment();

                            if (flag.equalsIgnoreCase("success")) {
                                HelpingLib.showmessage(mContext, message);
                                redirect(fragment);
                            } else if (flag.equalsIgnoreCase("internal_error")) {
                                String errmessage = getVchLocData.getString("error_message");
                                HelpingLib.showmessage1(mContext, errmessage);
                                redirect(fragment);
                            } else {

                                HelpingLib.showmessage1(mContext, message);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        //Fragment fragment=new MenuFragment();
                        //redirect(fragment);
                    }
                });
    }


    private void setAdapterVa() {
        adt1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list1);
        adt1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1.setAdapter(adt1);
        sp1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    disval = list1.get(i);
                    Log.e("Rs vdisval", disval.toString());
                    if (disval.equalsIgnoreCase("ঢাকা/DHAKA")
                            || disval.equalsIgnoreCase("চট্টগ্রাম/CHITTAGONG")
                            || disval.equalsIgnoreCase("খুলনা/KHULNA")
                            || disval.equalsIgnoreCase("রাজশাহী/RAJSHAHI")
                            || disval.equalsIgnoreCase("সিলেট/SYLHET")) {
                        list3 = new ArrayList<String>();
                        list3.add("সিলেক্ট করুন");
                        list3.add("-");
                        list3.add("মেট্রো/Metro");
                        set_list3Adapter();
                    } else {
                        list3 = new ArrayList<String>();
                        list3.add("সিলেক্ট করুন");
                        list3.add("-");
                        set_list3Adapter();
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

      /*  adt2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list3);
        adt2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2.setAdapter(adt2);
        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 1) {
                    //metroval=list3.get(i);
                    String stringVal = list3.get(i);
                    ;
                    String[] parts = stringVal.split("/");
                    String part2 = parts[1];
                    metroval = part2;
                } else {
                    metroval = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        adt3 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list2);
        adt3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3.setAdapter(adt3);
        sp3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    regval = list2.get(i);

                    etveh1.requestFocus();

                    etveh1.addTextChangedListener(Item_Factor_Watcher);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private final TextWatcher Item_Factor_Watcher = new TextWatcher() {
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        public void afterTextChanged(Editable s) {

            if (s.length() == 2) {
                etveh2.requestFocus();
            } else {
                etveh1.requestFocus();
            }
        }
    };

    private void set_list11Adapter() {
        adt11 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list11);
        adt11.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp11.setAdapter(adt11);
        sp11.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                fullvehname_11 = "";
                IsFormatedRegistrationNumber = "no";
                //  if (i > 0) {

                fullvehname_11 = "";

                fullvehname_11 = list11.get(i);
                IsFormatedRegistrationNumber = "yes";

               /* } else {
                    fullvehname_11 = "";
                    IsFormatedRegistrationNumber = "no";
                }*/

                Log.e("Rs fullvehname 32", fullvehname_11.toString());
//                Log.e("Rs IsFormatedRegistrationNumber 32", IsFormatedRegistrationNumber.toString());

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void set_list3Adapter() {
        adt2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list3);
        adt2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2.setAdapter(adt2);
        sp2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 1) {
                    //metroval=list3.get(i);
                    String stringVal = list3.get(i);

                    String[] parts = stringVal.split("/");
                    String part2 = parts[1];
                    metroval = part2;
                } else {
                    metroval = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void arraylist() {
        list1 = new ArrayList<String>();
        list1.add("সিলেক্ট করুন");
        list1.add("ঢাকা/DHAKA");
        list1.add("বাগেরহাট/BAGERHAT");
        list1.add("বান্দরবান/BANDARBAN");
        list1.add("বরগুনা/BARGUNA");
        list1.add("বরিশাল/BARISAL");
        list1.add("ভোলা/BHOLA");
        list1.add("বগুড়া/BOGRA");
        list1.add("ব্রাহ্মণবাড়িয়া/BRAHMANBARIA");
        list1.add("চাঁদপুর/CHANDPUR");
        list1.add("চাঁপাইনবাবগঞ্জ/CHAPAI");
        list1.add("চট্টগ্রাম/CHITTAGONG");
        list1.add("চুয়াডাঙ্গা/CHUADANGA");
        list1.add("কুমিল্লা/COMILLA");
        list1.add("কক্সবাজার/COX'S BAZAR");

        list1.add("ফরিদপুর/FARIDPUR");
        list1.add("ফেনী/FENI");
        list1.add("গাইবান্ধা/GAIBANDHA");
        list1.add("গাজীপুর/GAZIPUR");
        list1.add("গোপালগঞ্জ/GOPALGANJ");
        list1.add("হবিগঞ্জ/HABIGANJ");
        list1.add("জামালপুর/JAMALPUR");
        list1.add("যশোর/JESSORE");
        list1.add("ঝালকাঠি/JHALOKATI");
        list1.add("ঝিনাইদহ/JHENAIDAH");
        list1.add("জয়পুরহাট/JOYPURHAT");
        list1.add("খাগড়াছড়ি/KHAGRACHHARI");
        list1.add("খুলনা/KHULNA");
        list1.add("কিশোরগঞ্জ/KISHOREGANJ");
        list1.add("কুড়িগ্রাম/KURIGRAM");
        list1.add("কুষ্টিয়া/KUSHTIA");
        list1.add("লক্ষ্মীপুর/LAKSHMIPUR");
        list1.add("লালমনিরহাট/LALMONIRHAT");
        list1.add("মাদারিপুর/MADARIPUR");
        list1.add("মাগুরা/MAGURA");
        list1.add("মানিকগঞ্জ/MANIKGANJ");
        list1.add("মেহেরপুর/MEHERPUR");
        list1.add("মৌলভীবাজার/MOULVIBAZAR");
        list1.add("মুন্সিগঞ্জ/MUNSHIGANJ");
        list1.add("ময়মনসিংহ/MYMENSINGH");
        list1.add("নওগাঁ/NAOGAON");
        list1.add("নড়াইল/NARAIL");
        list1.add("নারায়ণগঞ্জ/NARAYANGANJ");
        list1.add("নরসিংদি/NARSINGDI");
        list1.add("নাটোর/NATORE");
        list1.add("নেত্রকোনা/NETROKONA");
        list1.add("নীলফামারি/NILPHAMARI");
        list1.add("নোয়াখালি/NOAKHALI");
        list1.add("পাবনা/PABNA");
        list1.add("পঞ্চগড়/PANCHAGARH");
        list1.add("পটুয়াখালি/PATUAKHALI");
        list1.add("পিরোজপুর/PIROJPUR");
        list1.add("রাজবাড়ি/RAJBARI");
        list1.add("রাজশাহী/RAJSHAHI");
        list1.add("রাঙামাটি/RANGAMATI");
        list1.add("রংপুর/RANGPUR");
        list1.add("সাতক্ষীরা/SATKHIRA");
        list1.add("শরিয়তপুর/SHARIATPUR");
        list1.add("শেরপুর/SHERPUR");
        list1.add("সিরাজগঞ্জ/SIRAJGANJ");
        list1.add("সুনামগঞ্জ/SUNAMGANJ");
        list1.add("বাগেরহাট/BAGERHAT");
        list1.add("সিলেট/SYLHET");
        list1.add("টাঙ্গাইল/TANGAIL");
        list1.add("ঠাকুরগাঁও/THAKURGAON");


        list2 = new ArrayList<String>();
        list2.add("সিলেক্ট করুন");
        list2.add("এ/A");
        list2.add("অ/AU");
        list2.add("ব/BA");
        list2.add("ভ/BHA");
        list2.add("ছ/CAA");
        list2.add("চ/CHA");
        list2.add("ড/DA");
        list2.add("দ/DAW");
        list2.add("ঢ/DHA");
        list2.add("ই/E");
        //  list2.add("ফ/FA");
        list2.add("গ/GA");
        list2.add("ঘ/GHA");
        //  list2.add("হ/HA");
        list2.add("জ/JA");
        list2.add("ঝ/JHA");
        list2.add("ক/KA");
        list2.add("খ/KHA");
        //  list2.add("ল/LA");
        list2.add("ম/MA");
        list2.add("ন/NA");
        list2.add("প/PA");
        list2.add("র/RA");
        list2.add("স/SA");
        list2.add("শ/SHA");
        list2.add("ট/TA");
        list2.add("থ/TAW");
        list2.add("ঠ/THA");
        list2.add("ঊ/U");
        list2.add("ঙ/WUA");
        list2.add("য/ZA");

       /* list3 = new ArrayList<String>();
        list3.add("সিলেক্ট করুন");
        list3.add("-");*/
        //  list3.add("মেট্রো/Metro");

    }

    private void setAdapterVal() {
        resonAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, vehInReassonNameDB);
        resonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinreason.setAdapter(resonAdapter);

        spinreason.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                resval = "";
                resval2 = "";
                //  if (i > 0) {
                //resval=""+i;
                resval = "";
                resval2 = vehInReassonDB.get(i).getValue();
                resval = vehInReassonDB.get(i).getId();

                Log.e("Rs resval id ", resval);
                Log.e("Rs resval ReassonName ", resval2);
                //   }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, vehInloadingDB);
        loadAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spload.setAdapter(loadAdapter);
        spload.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                //  if (i > 0) {
                loadstatus = "";
                loadstatus = vehInloadingDB.get(i);

                // }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getUtilitydata() {
        VehUtilityOperation.getData(mActivity, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVchLocData) {
                try {
                    String flag = getVchLocData.getString("flag");
                    if (flag.equalsIgnoreCase("found")) {

                        HandleINManReasonJsonData.data(getVchLocData);
                        if (vehInReassonDB.size() > 0) {
                            setAdapterVal();

                        }
                    } else {
                        HelpingLib.showmessage1(mContext, "System error");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }


    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);
        lllt = (LinearLayout) rootView.findViewById(R.id.lllt);
        ll2 = (LinearLayout) rootView.findViewById(R.id.ll2);
        LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ll2.getLayoutParams();
        lp.height = 0;
        ll2.setLayoutParams(lp);

        vehsel = (LinearLayout) rootView.findViewById(R.id.vehsel);
        btnsubmit = (Button) rootView.findViewById(R.id.btnsubmit);

        tvresult = (TextView) rootView.findViewById(R.id.tvresult);
        spinreason = (Spinner) rootView.findViewById(R.id.spinreason);
        spload = (Spinner) rootView.findViewById(R.id.spload);

        sp11 = (Spinner) rootView.findViewById(R.id.sp11);
        sp1 = (Spinner) rootView.findViewById(R.id.sp1);
        sp2 = (Spinner) rootView.findViewById(R.id.sp2);
        sp3 = (Spinner) rootView.findViewById(R.id.sp3);
        spGroup = (Spinner) rootView.findViewById(R.id.spGroup);

        btn_search = (Button) rootView.findViewById(R.id.btn_search);
        edt_search = (EditText) rootView.findViewById(R.id.edt_search);

        etveh1 = (EditText) rootView.findViewById(R.id.etveh1);
        etveh2 = (EditText) rootView.findViewById(R.id.etveh2);

        sendDataToServer_Search();

        edt_search.addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {

                SearchText = edt_search.getText().toString();
                //  int lng1 = veh1.length();
                //  int lng2 = veh2.length();
                int lng11 = SearchText.length();
                // fullvehname = disval + " " + metroval + "-" + regval + "-" + veh1 + "-" + veh2;
                fullvehname_11 = "";
                boolean res = HelpingLib.isInternetConnected(mContext);
                if (res) {
                    if (lng11 < 4 && lng11 > 0) {
                        HelpingLib.showmessage1(mContext, "সর্বনিম্ন 4 অঙ্ক লিখুন");
                    } else if (lng11 == 4) {
                        sendDataToServer_Search(SearchText);
                    }

                } else {
                    HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (s.length() != 0) {
                    //  list11 = new ArrayList<String>();
                    //  list11.add("সিলেক্ট করুন");
                    // ll2.setVisibility(View.VISIBLE);
                    //vehsel.setVisibility(View.INVISIBLE);
                } else {
                    //  ll2.setVisibility(View.INVISIBLE);
                    //vehsel.setVisibility(View.VISIBLE);
                    ll2.setVisibility(View.INVISIBLE);
                    LinearLayout.LayoutParams lp = (LinearLayout.LayoutParams) ll2.getLayoutParams();
                    lp.height = 0;
                    ll2.setLayoutParams(lp);
                    vehsel.setVisibility(View.VISIBLE);
                    LinearLayout.LayoutParams lpp = (LinearLayout.LayoutParams) vehsel.getLayoutParams();
                    lpp.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                    vehsel.setLayoutParams(lpp);
                    IsFormatedRegistrationNumber = "no";
                }
            }
        });
    }

    private void sendDataToServer_Search() {

        V_TYPE_ID.clear();
        V_TYPE_NAME.clear();

        V_TYPE_ID.add("0");
        V_TYPE_NAME.add("সিলেক্ট করুন");

        GetVechileGroupData.sendGetInData(getActivity(), new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVchLocData) {
                Log.e("Rs SearchText 1 ", getVchLocData.toString());
                try {
                    String flag = getVchLocData.getString("flag");
                    Fragment fragment = new MenuFragment();

                    Log.e("Rs flag 10 ", flag.toString());

                    if (flag.equalsIgnoreCase("found")) {

                        JSONArray userArray = getVchLocData.getJSONArray("VehicleInManualReasons");
                        Log.e("Rs SearchText list 2 ", userArray.toString());
                        int size = userArray.length();
                        for (int i = 0; i < size; i++) {
                            JSONObject finalJsonData = userArray.getJSONObject(i);
                            String PK_PRG_Type = finalJsonData.getString("PK_PRG_Type");
                            String Title = finalJsonData.getString("Title");

                            V_TYPE_ID.add(PK_PRG_Type);
                            V_TYPE_NAME.add(Title);

                            // Log.e("Rs RegistrationNumber 3", RegistrationNumber1.toString());
                        }

                        SET_Route_Name();

                    } else if (flag.equalsIgnoreCase("not_found")) {


                        String errmessage = ("কোন গাড়ি পাওয়া যায় নি");
                        HelpingLib.showmessage1(getActivity(), errmessage);
                        // redirect(fragment);
                    } else {

                        HelpingLib.showmessage1(getActivity(), "কোন গাড়ি পাওয়া যায় নি");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Fragment fragment=new MenuFragment();
                //redirect(fragment);
            }
        });
    }

    public void SET_Route_Name() {

        dataAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_item, V_TYPE_NAME);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        spGroup.setAdapter(dataAdapter);
        spGroup.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                v_type_id = "";
                v_type_name = "";
                v_type_id = V_TYPE_ID.get(pos);
                v_type_name = V_TYPE_NAME.get(pos);

               /* SharedPreferences.Editor addData = appData.edit();
                addData.putString("v_type_id", v_type_id);
                addData.putString("v_type_name", v_type_name);
                addData.commit();*/

                Log.e("Rs v_type_id", v_type_id.toString());
                Log.e("Rs v_type_name", v_type_name.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public void handleResult(Result result) {
        // Do something with the result here
        Log.d("sd", "handleResult: " + result.getText());
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void redirect(Fragment fragment) {
        //Object Object = null;
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        //fragmentTransaction.remove((Fragment) Object);
        //fragmentTransaction.addToBackStack(null);
        //fragmentTransaction.pop
        //getFragmentManager().beginTransaction().remove((Fragment) fragment).commitAllowingStateLoss();
        fragmentTransaction.commit();
    }
}
