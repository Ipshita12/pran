package com.prangroup.thirdEyePlus.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
//import android.test.mock.MockPackageManager;
import android.test.mock.MockPackageManager;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.maps.android.clustering.ClusterItem;
import com.google.maps.android.clustering.ClusterManager;
import com.prangroup.thirdEyePlus.Adapter.OwnIconRendered;
import com.prangroup.thirdEyePlus.Adapter.VehSearchAdapter;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Default.RouteActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserVehLocInfoListJsonData;
import com.prangroup.thirdEyePlus.ServerOperation.UserVehListOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.Utilities.Salahuddinlib;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


public class LiveFragment extends Fragment implements ClusterManager.OnClusterItemInfoWindowClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    private OnFragmentInteractionListener mListener;
    private ConnectionDetector connectionDetector;
    public static GoogleMap mGoogleMap;
    public static Activity mActivity;
    public static Context mContext;
    MapView mMapView;
    private float previousZoomLevel = -1.0f;
    public static final int DEFAULT_ZOOM_LEVEL = 16;
    private LatLng centerLatLng = null;
    public static boolean isForground = false;
    public static int pdVal = 0;
    public static String vehID;
    public static String lat,lng;
    private static final int ZOOMLVL = 15;
    private FusedLocationProviderClient mFusedLocationClient;
    private BottomSheetBehavior bottomSheetBehavior;
    private ImageView bottomSheetImgView;
    private TextView bottomSheetHeading, bottomSheetAddress, bottomSheetStatus, bottomSheetSpeed, bottomSheetBody, bottomSheetDateTime;
    public static ClusterManager mClusterManager;
    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    public static VehSearchAdapter lAdapter;
    public static AutoCompleteTextView inputSearchBox;
    String DEFAULT_LOCALE_STRING = "bn";
    public LiveFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static LiveFragment newInstance(String param1, String param2) {
        LiveFragment fragment = new LiveFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mActivity = getActivity();
        mContext = getContext();
        pdVal = 1;
        Salahuddinlib kazilib = new Salahuddinlib();
        kazilib.displayLocationSettingsRequest(mContext, mActivity);
        HelpingLib.currentLocation(mContext, MainActivity.mFusedLocationClient, mActivity);
        Log.e("liveTrack", "Api hited Start  time== " + new Date(System.currentTimeMillis()));


        rootView = inflater.inflate(R.layout.fragment_live, container, false);

        initialization(savedInstanceState);
        String livetxt = getResources().getString(R.string.nav_text_live);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + livetxt+ " </font>"));

        getLocationPermision();
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(getActivity());
        try{
            centerLatLng = new LatLng(HelpingLib.curloc.latitude, HelpingLib.curloc.longitude);
            lat= String.valueOf(HelpingLib.curloc.latitude);
            lng= String.valueOf(HelpingLib.curloc.longitude);
        }catch(Exception e){
            Log.e("liveTrack", "error");
        };

        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() { // Function runs every MINUTES minutes.

                if (LiveFragment.isForground) {

                    Log.e("liveTrack", "Method Called Data time== " + new Date(System.currentTimeMillis()));
                    UserVehListOperation.getVehData(lat,lng,mActivity, mContext);
                }

            }
        }, 0, 120000);
        return rootView;
    }


    private void initialization(Bundle savedInstanceState) {

        mMapView = (MapView) rootView.findViewById(R.id.livemap);
        connectionDetector = new ConnectionDetector(MainActivity.context);
        init(savedInstanceState);
    }
    private void getLocationPermision() {
        try {
            if (ActivityCompat.checkSelfPermission(getContext(), mPermission)
                    != MockPackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{mPermission}, REQUEST_CODE_PERMISSION);

                // If any permission above not allowed by user, this condition will execute every time, else your else part will work
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void init(Bundle savedInstanceState) {

        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();

        MapsInitializer.initialize(getActivity().getApplicationContext());
        mMapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {

                mGoogleMap = googleMap;
                maptask(mGoogleMap);
            }
        });
        //
        inputSearchBox = (AutoCompleteTextView) rootView.findViewById(R.id.vehsearchSearchBox);

        inputSearchBox.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //InputMethodManager imm = (InputMethodManager) mContext.getSystemService(Context.INPUT_METHOD_SERVICE);
                //imm.hideSoftInputFromWindow(inputSearchBox.getWindowToken(), 0);
                String vehName = inputSearchBox.getText().toString();
                int id=HelpingLib.getVehIDByVehName(vehName);
                //Log.e("click",id+"--"+vehName);
                redirectToRoute(id);
            }
        });

        bottomSheetBehavior = BottomSheetBehavior.from(rootView.findViewById(R.id.bottomSheetLayout));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        bottomSheetImgView = (ImageView) rootView.findViewById(R.id.img1);
        bottomSheetHeading = (TextView) rootView.findViewById(R.id.tvTitle);
        bottomSheetAddress = (TextView) rootView.findViewById(R.id.tvPlace);
        bottomSheetStatus = (TextView) rootView.findViewById(R.id.tvStatus);
        bottomSheetSpeed = (TextView) rootView.findViewById(R.id.tvSpeed);
        bottomSheetBody = (TextView) rootView.findViewById(R.id.tvBodyVal);
        bottomSheetDateTime = (TextView) rootView.findViewById(R.id.tvDateTime);

    }

    public void maptask(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mGoogleMap.setMyLocationEnabled(true);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        //mGoogleMap.setMinZoomPreference(8.0f);
        //mGoogleMap.setMaxZoomPreference(17.0f);
        mGoogleMap.setTrafficEnabled(true);
        mClusterManager = new ClusterManager(mContext, mGoogleMap);

        UserVehListOperation.getVehData(lat,lng,mActivity, mContext);

        if (mMapView != null &&
                mMapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 150);
        }


        //centerLatLng = new LatLng(23.781017,90.426153);

        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLng(centerLatLng));
        setUpClusterer();
        mGoogleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Log.e("ClusterItem", "My Cluster Item clicked");
                //updateBottomSheetContent(marker);
                goSingleCar(marker);
                return true;
            }
        });
        mGoogleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });
        isForground = true;
    }

    private void goSingleCar(Marker marker) {
        try {
            int id = Integer.valueOf(marker.getTitle());
            redirectToRoute(id);
        }catch (Exception e){
            Log.e("liveTrack", "error");
        }
    }

    private void setUpClusterer() {
        //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(23.7814177,90.4253823), 6));
        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(HelpingLib.curloc.latitude, HelpingLib.curloc.longitude), 6));
        mClusterManager = new ClusterManager(mContext, mGoogleMap);
        previousZoomLevel = DEFAULT_ZOOM_LEVEL;
        mGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mGoogleMap.getUiSettings().setMapToolbarEnabled(true);
        mGoogleMap.setOnCameraIdleListener(mClusterManager);

        mClusterManager.setRenderer(new OwnIconRendered(mContext, mGoogleMap, mClusterManager));

    }

    @Override
    public void onClusterItemInfoWindowClick(ClusterItem clusterItem) {
        Log.e("ClusterItem", "My Cluster Item clicked");
    }


    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
        isForground = false;
    }

    private void redirectToRoute(int id){
        String lat = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(id).getLatitude();
        String lng = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(id).getLongitude();
        vehID = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(id).getPK_Vehicle();
       // Log.e("UPLOAD_URL","clicked vehID=="+vehID);
       // Log.e("UPLOAD_URL","clicked Current latlng=="+ new LatLng(Double.valueOf(lat), Double.valueOf(lng)));
//        RouteActivity.oldLatlng = new LatLng(Double.valueOf(lat), Double.valueOf(lng));
        Intent singleCarIntent = new Intent(mActivity, RouteActivity.class);
        singleCarIntent.putExtra("carID", vehID);
        singleCarIntent.putExtra("id", ""+id);
        singleCarIntent.putExtra("status", ""+HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(id).getStatus());
        if (lat.equalsIgnoreCase("") || lng.equalsIgnoreCase("")){
            HelpingLib.showmessage1(mContext,"No tracking info found");
        }else{
            singleCarIntent.putExtra("oldlatlng", new LatLng(Double.valueOf(lat), Double.valueOf(lng)));
            mActivity.startActivity(singleCarIntent);
        }

    }
}
