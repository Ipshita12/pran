package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Default.SimpleScannerActivity;
import com.prangroup.thirdEyePlus.Default.SimpleScannerExitActivity;
import com.prangroup.thirdEyePlus.Model.CompaniesDataModel;
import com.prangroup.thirdEyePlus.Model.DepartmentsDataModel;
import com.prangroup.thirdEyePlus.Model.LoadingBayDataModel;
import com.prangroup.thirdEyePlus.Model.LocationBuildingDataModel;
import com.prangroup.thirdEyePlus.Model.PurposesDataModel;
import com.prangroup.thirdEyePlus.Model.VehicleListDataModel;
import com.prangroup.thirdEyePlus.Model.VehicleUnloadListDataModel;
import com.prangroup.thirdEyePlus.Model.VisitorExitListDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.LoadUnloadUtilitiesSearchOperation;
import com.prangroup.thirdEyePlus.ServerOperation.VisitorExitSearchOperation;
import com.prangroup.thirdEyePlus.ServerOperation.VisitorInfoInitialSearchOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class MenuFragment extends Fragment {
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private View rootView;
    public static Context mContext;
    public static Activity mActivity;
    Fragment fragment;
    private OnFragmentInteractionListener mListener;
    private ConnectionDetector connectionDetector;
    public static LinearLayout lllt, llrt, llir, llvir, llexitman, llgetinman, llVisitorCreate, llVisitorExit, llLoadStart, llLoadEnd;
//    CardView cvGetIn, cvGetInM, cvGetExit, cvGetExitM, cvLoadUnloadStart, cvLoadUnloadEnd, cvVisitorCreate, cvVisitorExit, cvTracking;
    LinearLayout llOne, llTwo, llThree, llFour, llFive;
    String location;

    public static ArrayList<CompaniesDataModel> companyListData;
    public static ArrayList<DepartmentsDataModel> deptListData;
    public static ArrayList<PurposesDataModel> purposeListData;
    public static ArrayList<VehicleListDataModel> vehicleListData;
    public static ArrayList<LocationBuildingDataModel> locationBuildingListData;
    public static ArrayList<LoadingBayDataModel> loadingBayListData;

    public static ArrayList<VehicleUnloadListDataModel> vehicleUnloadListData;

    public static ArrayList<String> companyArrayList;
    public static ArrayList<String> departmentArrayList;
    public static ArrayList<String> purposeArrayList;
    public static ArrayList<String> vehicleArrayList = new ArrayList<>();
    public static ArrayList<String> locationBuildingArrayList = new ArrayList<>();
    public static ArrayList<String> loadingBayArrayList = new ArrayList<>();


    public static ArrayList<VisitorExitListDataModel> visitorListData = new ArrayList<>();

    public MenuFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static MenuFragment newInstance(String param1, String param2) {
        MenuFragment fragment = new MenuFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_iamenu, container, false);
        String menutxt = getResources().getString(R.string.menu_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + menutxt + " </font>"));

        initialization();

//        cvTracking.setVisibility(View.GONE);
//        cvGetIn.setVisibility(View.GONE);
//        cvGetInM.setVisibility(View.GONE);
//        cvGetExit.setVisibility(View.GONE);
//        cvGetExitM.setVisibility(View.GONE);
//        cvLoadUnloadStart.setVisibility(View.GONE);
//        cvLoadUnloadEnd.setVisibility(View.GONE);
//        cvVisitorCreate.setVisibility(View.GONE);
//        cvVisitorExit.setVisibility(View.GONE);

        llOne.setVisibility(View.GONE);
        llTwo.setVisibility(View.GONE);
        llThree.setVisibility(View.GONE);
        llFour.setVisibility(View.GONE);
        llFive.setVisibility(View.GONE);

        if (MainActivity.gate == 1) {
//            cvGetIn.setVisibility(View.VISIBLE);
//            cvGetInM.setVisibility(View.VISIBLE);
//            cvGetExit.setVisibility(View.VISIBLE);
//            cvGetExitM.setVisibility(View.VISIBLE);
            llOne.setVisibility(View.VISIBLE);
            llTwo.setVisibility(View.VISIBLE);
        }

        if(MainActivity.load == 1) {
//            cvLoadUnloadStart.setVisibility(View.VISIBLE);
//            cvLoadUnloadEnd.setVisibility(View.VISIBLE);
            llThree.setVisibility(View.VISIBLE);
        }

        if (MainActivity.visit == 1) {
//            cvVisitorCreate.setVisibility(View.VISIBLE);
//            cvVisitorExit.setVisibility(View.VISIBLE);
            llFour.setVisibility(View.VISIBLE);
        }

        if (MainActivity.track == 1) {
//            cvTracking.setVisibility(View.VISIBLE);
            llFive.setVisibility(View.VISIBLE);
        }

        lllt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragment = new LiveFragment();
                redirect(fragment);
            }
        });

        llrt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent myIntent = new Intent(mContext, SimpleScannerActivity.class);
                mActivity.startActivity(myIntent);
            }
        });

        llir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent(mContext, SimpleScannerExitActivity.class);
                mActivity.startActivity(myIntent);
            }
        });
        llgetinman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragment = new VehINManualFragment();
                redirect(fragment);
            }
        });
        llexitman.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                fragment = new VehExitManFragment();
                redirect(fragment);
            }
        });

        llVisitorCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean res = HelpingLib.isInternetConnected(mContext);
                if (res) {
                    handleVisitorInfoInitialSearch();
                } else {
                    HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                }
            }
        });

        llVisitorExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean res = HelpingLib.isInternetConnected(mContext);
                if (res) {
                    handleVisitorExitSearch();
                } else {
                    HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                }
            }
        });

        llLoadStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean res = HelpingLib.isInternetConnected(mContext);
                if (res) {
                    handleLoadUnloadStartUtilitiesSearch();
                } else {
                    HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                }
            }
        });

        llLoadEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                boolean res = HelpingLib.isInternetConnected(mContext);
                if (res) {
                    handleLoadUnloadEndUtilitiesSearch();
                } else {
                    HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                }
            }
        });
        return rootView;
    }

    private void initialization() {
        location = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.location, "location not found");
        mContext = getContext();
        mActivity = getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);
        lllt = (LinearLayout) rootView.findViewById(R.id.lllt);
        llrt = (LinearLayout) rootView.findViewById(R.id.llrt);
        llir = (LinearLayout) rootView.findViewById(R.id.llir);
        llvir = (LinearLayout) rootView.findViewById(R.id.llvir);
        llgetinman = (LinearLayout) rootView.findViewById(R.id.llgetinman);
        llexitman = (LinearLayout) rootView.findViewById(R.id.llexitman);
        llVisitorCreate = rootView.findViewById(R.id.llVisitorCreate);
        llVisitorExit = rootView.findViewById(R.id.llVisitorExit);
        llLoadStart = rootView.findViewById(R.id.llLoadStart);
        llLoadEnd = rootView.findViewById(R.id.llLoadEnd);
//        cvGetIn = rootView.findViewById(R.id.cvGetIn);
//        cvGetInM = rootView.findViewById(R.id.cvGetInMan);
//        cvGetExit = rootView.findViewById(R.id.cvGetExit);
//        cvGetExitM = rootView.findViewById(R.id.cvGetExitMan);
//        cvTracking = rootView.findViewById(R.id.cvlt);
//        cvLoadUnloadStart = rootView.findViewById(R.id.cvLoadUnloadStart);
//        cvLoadUnloadEnd = rootView.findViewById(R.id.cvLoadUnloadEnd);
//        cvVisitorCreate = rootView.findViewById(R.id.cvVisitorCreate);
//        cvVisitorExit = rootView.findViewById(R.id.cvVisitorExit);

        llOne = rootView.findViewById(R.id.ll1);
        llTwo = rootView.findViewById(R.id.ll2);
        llThree = rootView.findViewById(R.id.ll3);
        llFour = rootView.findViewById(R.id.ll4);
        llFive = rootView.findViewById(R.id.ll5);
    }

    private void redirect(Fragment fragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        //fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    public static void handleVisitorInfoInitialSearch() {
        String location_str = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.location, "location not found");
        VisitorInfoInitialSearchOperation.sendVisitorInitialInfoSearchData(mActivity, location_str, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVisitorData) {

                try {
                    companyListData = new ArrayList<>();
                    deptListData = new ArrayList<>();
                    purposeListData = new ArrayList<>();
                    companyArrayList = new ArrayList<>();
                    departmentArrayList = new ArrayList<>();
                    purposeArrayList = new ArrayList<>();

                    companyListData.clear();
                    deptListData.clear();
                    purposeListData.clear();
                    companyArrayList.clear();
                    departmentArrayList.clear();
                    purposeArrayList.clear();

                    JSONArray valuesCompanies = getVisitorData.getJSONArray("companies");
                    JSONArray valuesDept = getVisitorData.getJSONArray("departments");
                    JSONArray valuesPurpose = getVisitorData.getJSONArray("purposes");

                    for (int i = 0; i < valuesCompanies.length(); i++) {

                        JSONObject company = valuesCompanies.getJSONObject(i);

                        int company_id = company.getInt("id");
                        String company_name = company.getString("company_name");
                        companyArrayList.add(company_name);
                        CompaniesDataModel companyDataModel = new CompaniesDataModel(company_id, company_name);
                        companyListData.add(companyDataModel);
                    }

                    for (int i = 0; i < valuesDept.length(); i++) {

                        JSONObject department = valuesDept.getJSONObject(i);

                        int dept_id = department.getInt("id");
                        String dept_name = department.getString("department_name");
                        departmentArrayList.add(dept_name);
                        DepartmentsDataModel deptDataModel = new DepartmentsDataModel(dept_id, dept_name);
                        deptListData.add(deptDataModel);
                    }

                    for (int i = 0; i < valuesPurpose.length(); i++) {

                        JSONObject purpose = valuesPurpose.getJSONObject(i);

                        int purpose_id = purpose.getInt("id");
                        String purpose_name = purpose.getString("purpose_name");
                        purposeArrayList.add(purpose_name);
                        PurposesDataModel purposeDataModel = new PurposesDataModel(purpose_id, purpose_name);
                        purposeListData.add(purposeDataModel);
                    }
                    String tag = "Fragment 2 here";
                    Fragment fragment = new VisitorCreateFragment();
                    MainActivity.fragmentManager.beginTransaction()
                            .replace(R.id.frameLayout, fragment, tag)
                            .commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void handleVisitorExitSearch() {
        String location_str = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.location, "location not found");
        VisitorExitSearchOperation.sendVisitorSearchData(mActivity, location_str);
    }

    //Load_Unload Start
    public static void handleLoadUnloadStartUtilitiesSearch() {
        String pk_user = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_id, "user not found");

        LoadUnloadUtilitiesSearchOperation.getLoadUnloadStartUtilitiesSearchData(mActivity, pk_user, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getLoadData) {

                try {
                    String foundData = getLoadData.getString("flag");

                    if (foundData.equalsIgnoreCase("found")) {
                        vehicleListData = new ArrayList<>();
                        locationBuildingListData = new ArrayList<>();
                        loadingBayListData = new ArrayList<>();
//                        vehicleArrayList = new ArrayList<>();
//                        locationBuildingArrayList = new ArrayList<>();
//                        loadingBayArrayList = new ArrayList<>();

                        vehicleListData.clear();
                        locationBuildingListData.clear();
                        loadingBayListData.clear();
                        vehicleArrayList.clear();
                        locationBuildingArrayList.clear();
                        loadingBayArrayList.clear();

                        JSONArray valuesVehicleList = getLoadData.getJSONArray("VehicleList");
                        JSONArray valuesLocationBuildingList = getLoadData.getJSONArray("LocationBuildingList");
                        JSONArray valuesLoadingBayList = getLoadData.getJSONArray("LoadingBayList");

                        vehicleArrayList.add("গাড়ীর নাম্বার");
                        locationBuildingArrayList.add("বিল্ডিং নাম্বার");
                        loadingBayArrayList.add("বে নাম্বার");
                        for (int i = 0; i < valuesVehicleList.length(); i++) {

                            JSONObject vehicle = valuesVehicleList.getJSONObject(i);

                            String fk_vehicle = vehicle.getString("FK_Vehicle");
                            String reg_num = vehicle.getString("RegistrationNumber");
//                            int fk_parking_in_out = vehicle.getInt("FK_ParkingInOut");
                            String fk_parking_in_out;
                            if (vehicle.isNull("FK_ParkingInOut")) {
                                fk_parking_in_out = "null";
                            } else {
                                System.out.println("inside else part");
                                fk_parking_in_out = String.valueOf(vehicle.getInt("FK_ParkingInOut"));
                            }
                            int fk_location_building = vehicle.getInt("FK_LocationBuilding");
//                            int fk_loading_bay = vehicle.getInt("FK_LoadingBay");
                            //vehicleArrayList.add(reg_num);
                            VehicleListDataModel vehDataModel = new VehicleListDataModel(fk_vehicle, reg_num,
                                    fk_parking_in_out, fk_location_building);
                            vehicleListData.add(vehDataModel);
                        }

                        for (int i = 0; i < valuesLocationBuildingList.length(); i++) {

                            JSONObject locationBuilding = valuesLocationBuildingList.getJSONObject(i);

                            int pk_locationBuilding = locationBuilding.getInt("PK_LocationBuilding");
                            String building_name = locationBuilding.getString("LocationBuildingName");
                            //locationBuildingArrayList.add(building_name);
                            LocationBuildingDataModel locationDataDataModel = new LocationBuildingDataModel(pk_locationBuilding, building_name);
                            locationBuildingListData.add(locationDataDataModel);
                        }

                        for (int i = 0; i < valuesLoadingBayList.length(); i++) {

                            JSONObject loadingBay = valuesLoadingBayList.getJSONObject(i);

                            int pk_loading_bay = loadingBay.getInt("PK_LoadingBay");
                            int fk_location_building = loadingBay.getInt("FK_LocationBuilding");
                            String loading_bay_name = loadingBay.getString("LoadingBayName");
                            //loadingBayArrayList.add(loading_bay_name);
                            LoadingBayDataModel loadingBayDataDataModel = new LoadingBayDataModel(pk_loading_bay,
                                    fk_location_building, loading_bay_name);
                            loadingBayListData.add(loadingBayDataDataModel);
                        }

                        String tag = "Load/Unload Starts here";
                        Fragment fragment = new LoadUnloadStartFragment();
                        MainActivity.fragmentManager.beginTransaction()
                                .replace(R.id.frameLayout, fragment, tag)
                                .commit();
                    } else {
                        showCustomDialogError("Application error please, contact to MIS");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public static void handleLoadUnloadEndUtilitiesSearch() {
        String pk_user = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_id, "user not found");

        LoadUnloadUtilitiesSearchOperation.getLoadUnloadEndUtilitiesSearchData(mActivity, pk_user, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getUnloadData) {

                try {
                    Log.e("ipshi", getUnloadData.toString());
                    String foundData = getUnloadData.getString("flag");

                    if (foundData.equalsIgnoreCase("found")) {

                        JSONArray valuesVehicleList = getUnloadData.getJSONArray("VehicleList");

                        if(valuesVehicleList.length() == 0) {
                            Toast.makeText(mContext, "No data found", Toast.LENGTH_LONG).show();
                        } else {
                            vehicleUnloadListData = new ArrayList<>();
                            vehicleUnloadListData.clear();
                            for (int i = 0; i < valuesVehicleList.length(); i++) {

                                JSONObject vehicle = valuesVehicleList.getJSONObject(i);

                                int pkLoadingBayUtilization = vehicle.getInt("PK_LoadingBayUtilization");
                                String registrationNumber = vehicle.getString("RegistrationNumber");
                                String locationBuildingName = vehicle.getString("LocationBuildingName");
                                String loadingBayName = vehicle.getString("LoadingBayName");
                                String useType = vehicle.getString("UseType");
                                String startDateTime = vehicle.getString("StartDateTime");

                                VehicleUnloadListDataModel vehDataModel = new VehicleUnloadListDataModel(pkLoadingBayUtilization,
                                        registrationNumber, locationBuildingName, loadingBayName, useType, startDateTime);
                                vehicleUnloadListData.add(vehDataModel);
                            }

                            String tag = "Load/Unload End here";
                            Fragment fragment = new LoadUnloadEndFragment();
                            MainActivity.fragmentManager.beginTransaction()
                                    .replace(R.id.frameLayout, fragment, tag)
                                    .commit();
                        }
                    } else {
                        showCustomDialogError("Application error please, contact to MIS");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private static void showCustomDialogError(String text) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_error, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText(text);

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }
        });
        alertDialog.show();
    }
}
