package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.graphics.PorterDuff;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.LoadUnloadUtilitiesSearchOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class LoadUnloadStartFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    //Start here
    private View rootView;
    private OnFragmentInteractionListener mListener;
    public Context mContext;
    public Activity mActivity;
    Fragment fragment;
    private ConnectionDetector connectionDetector;
    private Button btnStart, btnEnd;
    private Spinner spn1, spn2, spn3;
    private EditText etRegNum;
    private ImageButton ibSearch;
    private String fk_vehicle = "";
    ArrayAdapter<String> adapter1, adapter2, adapter3;
    private String fk_parking_in_out;
    private int fk_location_building = 0, fk_loading_bay = 0, count = 0, location_count = 0, loading_count = 0;

    public LoadUnloadStartFragment() {
        // Required empty public constructor
    }

    public static LoadUnloadStartFragment newInstance(String param1, String param2) {
        LoadUnloadStartFragment fragment = new LoadUnloadStartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_load_unload_start, container, false);
        String menu_txt = getResources().getString(R.string.str_load_unload_start);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#01396C'>" + menu_txt + " </font>"));

        MainActivity.toolbar.getNavigationIcon().setColorFilter(getResources().getColor(R.color.back_btn), PorterDuff.Mode.SRC_ATOP);

        initialization();

        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);

        btnStart = rootView.findViewById(R.id.buttonStart);
        btnEnd = rootView.findViewById(R.id.buttonEnd);
        spn1 = rootView.findViewById(R.id.spinner1);
        spn2 = rootView.findViewById(R.id.spinner2);
        spn3 = rootView.findViewById(R.id.spinner3);
        etRegNum = rootView.findViewById(R.id.etSearchCar);
        ibSearch = rootView.findViewById(R.id.ibSearchBox);

        adapter1 = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_item, MenuFragment.vehicleArrayList);
        adapter2 = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_item, MenuFragment.locationBuildingArrayList);
        adapter3 = new ArrayAdapter<>(mActivity, android.R.layout.simple_spinner_item, MenuFragment.loadingBayArrayList);

        spn1.setAdapter(adapter1);
        spn2.setAdapter(adapter2);
        spn3.setAdapter(adapter3);

        btnStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spn1.getSelectedItem().toString().equals("গাড়ীর নাম্বার")) {
                    Toast.makeText(mContext, "প্রথমে গাড়ি নাম্বার সিলেক্ট করুন", Toast.LENGTH_LONG).show();
                } else if (spn2.getSelectedItem().toString().equals("বিল্ডিং নাম্বার")) {
                    Toast.makeText(mContext, "বিল্ডিং নাম্বার সিলেক্ট করুন", Toast.LENGTH_LONG).show();
                } else if (spn3.getSelectedItem().toString().equals("বে নাম্বার")) {
                    Toast.makeText(mContext, "বে নাম্বার সিলেক্ট করুন", Toast.LENGTH_LONG).show();
                } else {
                    boolean res = connectionDetector.isConnectedToInternet();
                    if (res) {
                        String spn1_txt = spn1.getSelectedItem().toString();
                        String spn2_txt = spn2.getSelectedItem().toString();
                        String spn3_txt = spn3.getSelectedItem().toString();
                        for (int i = 0; i < MenuFragment.vehicleListData.size(); i++) {
                            if (MenuFragment.vehicleListData.get(i).getRegistration_number().equals(spn1_txt)) {
                                fk_vehicle = MenuFragment.vehicleListData.get(i).getFk_vehicle();
                                fk_parking_in_out = MenuFragment.vehicleListData.get(i).getFk_parking_in_out();
                            }
                        }

                        for (int j = 0; j < MenuFragment.locationBuildingListData.size(); j++) {
                            if (MenuFragment.locationBuildingListData.get(j).getPk_location_building_name().equals(spn2_txt)) {
                                fk_location_building = MenuFragment.locationBuildingListData.get(j).getPk_location_building();
                                break;
                            }
                        }

                        for (int k = 0; k < MenuFragment.loadingBayListData.size(); k++) {
                            if (MenuFragment.loadingBayListData.get(k).getLoading_bay_name().equals(spn3_txt)) {
                                fk_loading_bay = MenuFragment.loadingBayListData.get(k).getPk_loading_bay();
                                break;
                            }
                        }
                        handleLoadUnloadStartEndSearch(1, fk_vehicle, fk_loading_bay, fk_parking_in_out);
                    } else {
                        HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                    }
                }
            }
        });

        btnEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (spn1.getSelectedItem().toString().equals("গাড়ীর নাম্বার")) {
                    Toast.makeText(mContext, "প্রথমে গাড়ি নাম্বার সিলেক্ট করুন", Toast.LENGTH_LONG).show();
                } else if (spn2.getSelectedItem().toString().equals("বিল্ডিং নাম্বার")) {
                    Toast.makeText(mContext, "বিল্ডিং নাম্বার সিলেক্ট করুন", Toast.LENGTH_LONG).show();
                } else if (spn3.getSelectedItem().toString().equals("বে নাম্বার")) {
                    Toast.makeText(mContext, "বে নাম্বার সিলেক্ট করুন", Toast.LENGTH_LONG).show();
                } else {
                    boolean res = HelpingLib.isInternetConnected(mContext);
                    if (res) {
                        String spn1_txt = spn1.getSelectedItem().toString();
                        String spn2_txt = spn2.getSelectedItem().toString();
                        String spn3_txt = spn3.getSelectedItem().toString();

                        for (int i = 0; i < MenuFragment.vehicleListData.size(); i++) {
                            if (MenuFragment.vehicleListData.get(i).getRegistration_number().equals(spn1_txt)) {
                                fk_vehicle = MenuFragment.vehicleListData.get(i).getFk_vehicle();
                                fk_parking_in_out = MenuFragment.vehicleListData.get(i).getFk_parking_in_out();
                            }
                        }

                        for (int j = 0; j < MenuFragment.locationBuildingListData.size(); j++) {
                            if (MenuFragment.locationBuildingListData.get(j).getPk_location_building_name().equals(spn2_txt)) {
                                fk_location_building = MenuFragment.locationBuildingListData.get(j).getPk_location_building();
                                break;
                            }
                        }

                        for (int k = 0; k < MenuFragment.loadingBayListData.size(); k++) {
                            if (MenuFragment.loadingBayListData.get(k).getLoading_bay_name().equals(spn3_txt)) {
                                fk_loading_bay = MenuFragment.loadingBayListData.get(k).getPk_loading_bay();
                                break;
                            }
                        }
                        handleLoadUnloadStartEndSearch(2, fk_vehicle, fk_loading_bay, fk_parking_in_out);
                    } else {
                        HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                    }
                }
            }
        });

        ibSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                MenuFragment.locationBuildingArrayList.clear();
                adapter2.clear();
                adapter2.notifyDataSetChanged();
                adapter2.notifyDataSetChanged();

                MenuFragment.locationBuildingArrayList.add("বিল্ডিং নাম্বার");
                adapter2.notifyDataSetChanged();
                if (etRegNum.getText().toString().isEmpty() || etRegNum.getText().toString().trim().length() < 4) {
                    Toast.makeText(mContext, "সর্বনিম্ন ৪ সংখ্যা দিন", Toast.LENGTH_LONG).show();
                } else {
                    spn1.setEnabled(true);
                    spn2.setEnabled(true);
                    spn3.setEnabled(true);

                    String txt = etRegNum.getText().toString().trim();

                    MenuFragment.vehicleArrayList.clear();
                    adapter1.clear();
                    MenuFragment.vehicleArrayList.add("গাড়ীর নাম্বার");

                    int location_id = 0, loading_id = 0;
                    for (int i = 0; i < MenuFragment.vehicleListData.size(); i++) {

                        if (MenuFragment.vehicleListData.get(i).getRegistration_number().toLowerCase().contains(txt.toLowerCase())) {
                            count++;

                            MenuFragment.vehicleArrayList.add(MenuFragment.vehicleListData.get(i).getRegistration_number());
                            location_id = MenuFragment.vehicleListData.get(i).getFk_location_building();
//                            loading_id = MenuFragment.vehicleListData.get(i).getFk_loading_bay();
                        }
                    }

//                    MenuFragment.loadingBayArrayList.clear();
//                    adapter3.clear();
//                    MenuFragment.loadingBayArrayList.add("বে নাম্বার");

                    if (count == 1) {
                        adapter1.remove("গাড়ীর নাম্বার");
                        //adapter2.remove("বিল্ডিং নাম্বার");
                        location_count = 0;
                        loading_count = 0;
                        MenuFragment.locationBuildingArrayList.clear();
                        MenuFragment.loadingBayArrayList.clear();
//                        adapter2.clear();
//                        adapter3.clear();
                        adapter2.notifyDataSetChanged();
                        if (location_id == 0) {
                            MenuFragment.locationBuildingArrayList.add("বিল্ডিং নাম্বার");
                        }
                        for (int j = 0; j < MenuFragment.locationBuildingListData.size(); j++) {
                            MenuFragment.locationBuildingArrayList.add(MenuFragment.locationBuildingListData.get(j).getPk_location_building_name());
                        }
                        if(location_id != 0) {
                            for (int j = 0; j < MenuFragment.locationBuildingListData.size(); j++) {
                                if (MenuFragment.locationBuildingListData.get(j).getPk_location_building() == location_id) {
                                    location_count = j;
                                    break;
                                }
                            }
                        }

//                        int d = 0;
                        MenuFragment.loadingBayArrayList.add("বে নাম্বার");
                        for (int k = 0; k < MenuFragment.loadingBayListData.size(); k++) {
                            if (location_id == MenuFragment.loadingBayListData.get(k).getFk_location_building()) {
                                MenuFragment.loadingBayArrayList.add(
                                        MenuFragment.loadingBayListData.get(k).getLoading_bay_name());
//                                d++;
                            }
//                            if (loading_id == MenuFragment.loadingBayListData.get(k).getPk_loading_bay()) {
//                                loading_count = d;
//                            }
                        }
                        adapter1.notifyDataSetChanged();
                        adapter2.notifyDataSetChanged();
                        adapter3.notifyDataSetChanged();

                        spn2.setSelection(location_count);
                        spn3.setSelection(loading_count);
                    }

                    if (count == 0) {
                        MenuFragment.vehicleArrayList.clear();
                        MenuFragment.locationBuildingArrayList.clear();
                        MenuFragment.loadingBayArrayList.clear();

                        adapter2.notifyDataSetChanged();

                        MenuFragment.vehicleArrayList.add("গাড়ীর নাম্বার");
                        MenuFragment.locationBuildingArrayList.add("বিল্ডিং নাম্বার");
                        MenuFragment.loadingBayArrayList.add("বে নাম্বার");

                        adapter1.notifyDataSetChanged();
                        adapter2.notifyDataSetChanged();
                        adapter3.notifyDataSetChanged();

                        Toast.makeText(mContext, "কোন গাড়ী পাওয়া যায়নি", Toast.LENGTH_LONG).show();
                    }

                    if (count > 1) {
                        spn1.setSelection(0);

                        MenuFragment.locationBuildingArrayList.clear();
                        MenuFragment.loadingBayArrayList.clear();

                        adapter2.notifyDataSetChanged();

                        MenuFragment.locationBuildingArrayList.add("বিল্ডিং নাম্বার");
                        MenuFragment.loadingBayArrayList.add("বে নাম্বার");
                        adapter1.notifyDataSetChanged();
                        adapter2.notifyDataSetChanged();
                        adapter3.notifyDataSetChanged();
                    }
                    count = 0;
                }
            }
        });


        spn1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (etRegNum.getText().toString().isEmpty()) {
                    spn1.setEnabled(false);
                    spn2.setEnabled(false);
                    spn3.setEnabled(false);
                } else {
                    spn1.setEnabled(true);
                    spn2.setEnabled(true);
                    spn3.setEnabled(true);
                    String txt = parent.getItemAtPosition(position).toString();
                    location_count = 0;
                    loading_count = 0;
                    int location_id = 0, loading_id = 0;
//                    adapter2.clear();
                    adapter3.clear();
                    MenuFragment.locationBuildingArrayList.clear();
                    MenuFragment.loadingBayArrayList.clear();

                    adapter2.notifyDataSetChanged();

                    MenuFragment.locationBuildingArrayList.add("বিল্ডিং নাম্বার");
                    MenuFragment.loadingBayArrayList.add("বে নাম্বার");

                    for (int i = 0; i < MenuFragment.vehicleListData.size(); i++) {
                        if (MenuFragment.vehicleListData.get(i).getRegistration_number().equals(txt)) {
                            fk_vehicle = MenuFragment.vehicleListData.get(i).getFk_vehicle();
                            fk_parking_in_out = MenuFragment.vehicleListData.get(i).getFk_parking_in_out();
                            location_id = MenuFragment.vehicleListData.get(i).getFk_location_building();
//                            loading_id = MenuFragment.vehicleListData.get(i).getFk_loading_bay();
                        }
                    }

                    for (int j = 0; j < MenuFragment.locationBuildingListData.size(); j++) {
                        MenuFragment.locationBuildingArrayList.add(MenuFragment.locationBuildingListData.get(j).getPk_location_building_name());
                        if (MenuFragment.locationBuildingListData.get(j).getPk_location_building() == location_id) {
                            location_count = j + 1;
                        }
                    }

                    int d = 0;
                    for (int k = 0; k < MenuFragment.loadingBayListData.size(); k++) {
                        if (location_id == MenuFragment.loadingBayListData.get(k).getFk_location_building()) {
                            MenuFragment.loadingBayArrayList.add(
                                    MenuFragment.loadingBayListData.get(k).getLoading_bay_name());
                            d++;
                        }
//                        if (loading_id == MenuFragment.loadingBayListData.get(k).getPk_loading_bay()) {
//                            loading_count = d;
//                        }
                    }
                    adapter2.notifyDataSetChanged();
                    adapter3.notifyDataSetChanged();

                    spn2.setSelection(location_count);
//                    spn3.setSelection(loading_count);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        spn2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (etRegNum.getText().toString().isEmpty()) {
                    spn1.setEnabled(false);
                    spn2.setEnabled(false);
                    spn3.setEnabled(false);
                } else {
                    spn1.setEnabled(true);
                    spn2.setEnabled(true);
                    spn3.setEnabled(true);
                    String txt = parent.getItemAtPosition(position).toString();
                    adapter3.clear();
                    MenuFragment.loadingBayArrayList.clear();
                    MenuFragment.loadingBayArrayList.add("বে নাম্বার");
                    int location_id = 0;
                    for (int j = 0; j < MenuFragment.locationBuildingListData.size(); j++) {
                        if (MenuFragment.locationBuildingListData.get(j).getPk_location_building_name().equals(txt)) {
                            fk_location_building = MenuFragment.locationBuildingListData.get(j).getPk_location_building();
                            location_id = fk_location_building;
                        }
                    }

                    for (int k = 0; k < MenuFragment.loadingBayListData.size(); k++) {
                        if (location_id == MenuFragment.loadingBayListData.get(k).getFk_location_building()) {
                            MenuFragment.loadingBayArrayList.add(
                                    MenuFragment.loadingBayListData.get(k).getLoading_bay_name());
                        }
                    }
                    adapter3.notifyDataSetChanged();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spn3.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (etRegNum.getText().toString().isEmpty()) {
                    spn1.setEnabled(false);
                    spn3.setEnabled(false);
                    spn3.setEnabled(false);
                } else {
                    spn1.setEnabled(true);
                    spn2.setEnabled(true);
                    spn3.setEnabled(true);
                    String txt = parent.getItemAtPosition(position).toString();

                    for (int j = 0; j < MenuFragment.loadingBayListData.size(); j++) {
                        if (MenuFragment.loadingBayListData.get(j).getLoading_bay_name().equals(txt)) {
                            fk_loading_bay = MenuFragment.loadingBayListData.get(j).getPk_loading_bay();
                            break;
                        }
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    public void handleLoadUnloadStartEndSearch(int type, String vehicle, int loadingBay, String parkingInOut) {
        String pk_user = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_id, "user not found");

        String version = "" + MainActivity.versionCode;
        String loading = "" + loadingBay;
        String parking = parkingInOut;

        LoadUnloadUtilitiesSearchOperation.getLoadUnloadStartSearchData(mActivity, type, version, pk_user,
                vehicle, loading, parking, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getLoadData) {
                        try {
                            String status = getLoadData.getString("flag");
                            if (status.equalsIgnoreCase("success")) {
                                showCustomDialogSuccess();
                            } else {
                                String msg = getLoadData.getString("message");
                                showCustomDialogError(msg);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void showCustomDialogSuccess() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_success, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText("এন্ট্রি সফল হয়েছে।");

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                fragment = new MenuFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment);
                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }

    private void showCustomDialogError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_error, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText(message);

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
//                fragment = new MenuFragment();
//                FragmentManager fm = getActivity().getSupportFragmentManager();
//                FragmentTransaction fragmentTransaction = fm.beginTransaction();
//                fragmentTransaction.replace(R.id.frameLayout, fragment);
//                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }
}