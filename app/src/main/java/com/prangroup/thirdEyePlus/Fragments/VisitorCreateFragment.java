package com.prangroup.thirdEyePlus.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.VisitorInfoSearchOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import static android.app.Activity.RESULT_OK;
import static com.prangroup.thirdEyePlus.Default.MainActivity.fragmentManager;


public class VisitorCreateFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private String mParam1;
    private String mParam2;

    private View rootView;
    private OnFragmentInteractionListener mListener;

    public int visitorType = 1;
    public String location = "";
    public Context mContext;
    public Activity mActivity;
    Fragment fragment;
    private ConnectionDetector connectionDetector;
    private RadioGroup rgVisitorType;
    private EditText etVisitorStaffId, etName, etPhn, etFrom, etMeetWith, etNoOfPerson, etCardNo, etRemarks;
    private Button btnConfirm;
    private CardView cvVisitorStaffId, cvVisitorInfo;
    public static int companyId, departmentId, purposeId;
    public static String companyStr = "Press to select...", departmentStr = "Press to select...", purposeStr = "Press to select...";
    private String phnNo, image = "";
    private ImageView imageView;
    private TextView tvUploadImg;
    public TextView tvCompany, tvDepartment, tvPurpose;
    private LinearLayout llImg;
    private TextInputLayout textInputLayout, tILCardView;
    public static String data, menuName;
    public static int selectorType = 0, imgUploadState = 0;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private Bitmap imageBitmap;

    public VisitorCreateFragment() {
        // Required empty public constructor
    }

    public static VisitorCreateFragment newInstance(String param1, String param2) {
        VisitorCreateFragment fragment = new VisitorCreateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_visitor_create, container, false);
        String menu_txt = getResources().getString(R.string.str_visitor_create_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + menu_txt + " </font>"));

        initialization();

        return rootView;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);
        textInputLayout = rootView.findViewById(R.id.tilStaffId);
        tILCardView = rootView.findViewById(R.id.tILCardView);

        rgVisitorType = rootView.findViewById(R.id.rgVisitorType);
        cvVisitorStaffId = rootView.findViewById(R.id.cvVisitorStaffId);
        cvVisitorInfo = rootView.findViewById(R.id.cvVisitorInfo);
        etVisitorStaffId = rootView.findViewById(R.id.etStaffId);
        etName = rootView.findViewById(R.id.etVisitorName);
        etPhn = rootView.findViewById(R.id.etVisitorPhn);
        etFrom = rootView.findViewById(R.id.etVisitorFrom);
        etMeetWith = rootView.findViewById(R.id.etVisitorMeet);
        etNoOfPerson = rootView.findViewById(R.id.etVisitorPersonNo);
        etCardNo = rootView.findViewById(R.id.etVisitorCardNo);
        etRemarks = rootView.findViewById(R.id.etVisitorRemarks);
        imageView = rootView.findViewById(R.id.ivTest);
        tvUploadImg = rootView.findViewById(R.id.tvUploadImage);
        btnConfirm = rootView.findViewById(R.id.buttonConfirm);
        llImg = rootView.findViewById(R.id.linearLayoutImg);
        tvCompany = rootView.findViewById(R.id.textViewCompany);
        tvDepartment = rootView.findViewById(R.id.textViewDepartment);
        tvPurpose = rootView.findViewById(R.id.textViewPurpose);

        tvCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuName = "Company";
                selectorType = 1;
                navigateToListFragment();
            }
        });

        tvDepartment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuName = "Department";
                selectorType = 2;
                navigateToListFragment();
            }
        });

        tvPurpose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                menuName = "Purpose";
                selectorType = 3;
                navigateToListFragment();
            }
        });

        rgVisitorType.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rb_internal:
                        visitorType = 1;
                        cvVisitorStaffId.setVisibility(View.VISIBLE);
                        tILCardView.setVisibility(View.GONE);
                        textInputLayout.setHint("");
                        etVisitorStaffId.setText("");
                        etName.setText("");
                        etPhn.setText("");
                        etFrom.setText("");
                        etMeetWith.setText("");
                        etRemarks.setText("");
                        textInputLayout.setHint("Enter staff id");
                        break;
                    case R.id.rb_external:
                        visitorType = 2;
                        cvVisitorStaffId.setVisibility(View.VISIBLE);
                        tILCardView.setVisibility(View.VISIBLE);
                        textInputLayout.setHint("");
                        etVisitorStaffId.setText("");
                        etName.setText("");
                        etPhn.setText("");
                        etFrom.setText("");
                        etMeetWith.setText("");
                        etCardNo.setText("");
                        etRemarks.setText("");
                        textInputLayout.setHint("Enter mobile no");
                        break;
                }
            }
        });

        etVisitorStaffId.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                final int DRAWABLE_RIGHT = 2;

                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    if (motionEvent.getRawX() >= (etVisitorStaffId.getRight() - etVisitorStaffId.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        // your action here
                        if (etVisitorStaffId.getText().toString().isEmpty()) {
                            etVisitorStaffId.setError("Please enter first");
                        } else {
                            etName.setText("");
                            etPhn.setText("");
                            etFrom.setText("");
                            etMeetWith.setText("");
                            if (visitorType == 1) {
                                handleVisitorInfoSearch(visitorType, etVisitorStaffId.getText().toString());
                            } else {
                                if (etVisitorStaffId.getText().toString().length() == 11) {
                                    if (etVisitorStaffId.getText().toString().startsWith("019") ||
                                            etVisitorStaffId.getText().toString().startsWith("013") ||
                                            etVisitorStaffId.getText().toString().startsWith("017") ||
                                            etVisitorStaffId.getText().toString().startsWith("014") ||
                                            etVisitorStaffId.getText().toString().startsWith("016") ||
                                            etVisitorStaffId.getText().toString().startsWith("018") ||
                                            etVisitorStaffId.getText().toString().startsWith("015")) {
                                        phnNo = etVisitorStaffId.getText().toString();
                                        handleVisitorInfoSearch(visitorType, etVisitorStaffId.getText().toString());
                                    } else {
                                        etVisitorStaffId.setError("Please enter a valid mobile number");
                                    }
                                } else {
                                    etVisitorStaffId.setError("Mobile number must be 11 digits");
                                }
                            }
                        }
                        return true;
                    }
                }
                return false;
            }
        });

        llImg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkPermission()) {
                    selectImage();
                } else {
                    requestPermission();
                }
            }
        });

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String id = etVisitorStaffId.getText().toString();
                String name = etName.getText().toString();
                String phn = etPhn.getText().toString();
                String come = etFrom.getText().toString();
                String meet = etMeetWith.getText().toString();
                String card = etCardNo.getText().toString();
                String person = etNoOfPerson.getText().toString();
                String remarks = etRemarks.getText().toString();

                if (visitorType == 1 && id.isEmpty()) {
                    etVisitorStaffId.setError("Please enter staff id");
                } else if (name.isEmpty()) {
                    etName.setError("Please enter name");
                } else if (phn.length() != 11) {
                    etPhn.setError("Mobile number must be 11 digits");
                } else if (come.isEmpty()) {
                    etFrom.setError("Please enter first");
                } else if (meet.isEmpty()) {
                    etMeetWith.setError("Please enter first");
                } else if (visitorType == 2 && card.isEmpty()) {
                    etCardNo.setError("Please enter card no");
                } else if (companyStr.equalsIgnoreCase("Press to select...")) {
                    Toast.makeText(mActivity, "Please select company", Toast.LENGTH_SHORT).show();
                } else if (departmentStr.equalsIgnoreCase("Press to select...")) {
                    Toast.makeText(mActivity, "Please select department", Toast.LENGTH_SHORT).show();
                } else if (purposeStr.equalsIgnoreCase("Press to select...")) {
                    Toast.makeText(mActivity, "Please select purpose", Toast.LENGTH_SHORT).show();
                } else {
                    if (phn.startsWith("019") ||
                            phn.startsWith("013") ||
                            phn.startsWith("017") ||
                            phn.startsWith("014") ||
                            phn.startsWith("016") ||
                            phn.startsWith("018") ||
                            phn.startsWith("015")) {
                        if(visitorType == 2 && image.isEmpty()) {
                            Toast.makeText(mActivity, "Please upload an image", Toast.LENGTH_SHORT).show();
                        } else {
                            handleVisitorCreate(visitorType, id, name, phn, come, meet, card, person, remarks);
                        }
                    } else {
                        etPhn.setError("Please enter a valid mobile number");
                    }
                }
            }
        });
    }

    private void handleVisitorInfoSearch(final int type, String txt) {
        location = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.location, "location not found");
        VisitorInfoSearchOperation.sendVisitorInfoSearchData(mActivity, type, txt, location, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVisitorLogData) {
                try {
                    cvVisitorInfo.setVisibility(View.VISIBLE);

                    if(type == 2) {
                        etPhn.setText("");
                        etPhn.setText(phnNo);
                    }
                    JSONObject visitorObj = getVisitorLogData.getJSONObject("visitor");
                    String ownObj = getVisitorLogData.getString("own");
                    String activeObj = getVisitorLogData.getString("is_active");

                    if (ownObj.equalsIgnoreCase("true")
                            && activeObj.equalsIgnoreCase("true")) {
                        String name = visitorObj.getString("name");
                        String phn = visitorObj.getString("mobile");
                        String come_from = visitorObj.getString("come_from");
                        String meet_to = visitorObj.getString("whom");
                        String company = visitorObj.getString("company_id");
                        String dept = visitorObj.getString("department_id");
                        String purpose = visitorObj.getString("visitor_purpose_id");

                        etName.setText("");
                        etPhn.setText("");
                        etFrom.setText("");
                        etMeetWith.setText("");

                        etName.setText(name);
                        etPhn.setText(phn);
                        etFrom.setText(come_from);
                        etMeetWith.setText(meet_to);

                        for (int i = 0; i < MenuFragment.companyListData.size(); i++) {
                            if (MenuFragment.companyListData.get(i).getId() == Integer.parseInt(company)) {
                                //spinnerCompany.setSelection(i);
                                companyId = MenuFragment.companyListData.get(i).getId();
                                companyStr = MenuFragment.companyListData.get(i).getName();
                                tvCompany.setText(MenuFragment.companyListData.get(i).getName());
                                break;
                            }
                        }

                        for(int i = 0; i < MenuFragment.deptListData.size(); i++) {
                            if(MenuFragment.deptListData.get(i).getId() == Integer.parseInt(dept)) {
                                //spinnerDepartment.setSelection(i);
                                departmentId = MenuFragment.deptListData.get(i).getId();
                                departmentStr = MenuFragment.deptListData.get(i).getName();
                                tvDepartment.setText(MenuFragment.deptListData.get(i).getName());
                                break;
                            }
                        }

                        for (int i = 0; i < MenuFragment.purposeListData.size(); i++) {
                            if (MenuFragment.purposeListData.get(i).getId() == Integer.parseInt(purpose)) {
                                purposeId = MenuFragment.purposeListData.get(i).getId();
                                purposeStr = MenuFragment.purposeListData.get(i).getName();
                                tvPurpose.setText(MenuFragment.purposeListData.get(i).getName());
                                break;
                            }
                        }
                    } else if(ownObj.equalsIgnoreCase("false")
                            && activeObj.equalsIgnoreCase("true")) {
                        String name = visitorObj.getString("staffname");
                        String phn = visitorObj.getString("contactno");
                        String come_from = visitorObj.getString("location");

                        etName.setText("");
                        etPhn.setText("");
                        etFrom.setText("");

                        etName.setText(name);
                        etPhn.setText(phn);
                        etFrom.setText(come_from);
                    } else if(visitorType == 2 && ownObj.equalsIgnoreCase("true")
                            && activeObj.equalsIgnoreCase("false")) {
                        String name = visitorObj.getString("name");
                        String phn = visitorObj.getString("mobile");
                        String come_from = visitorObj.getString("come_from");
                        String meet_to = visitorObj.getString("whom");
                        String company = visitorObj.getString("company_id");
                        String dept = visitorObj.getString("department_id");
                        String purpose = visitorObj.getString("visitor_purpose_id");

                        etName.setText("");
                        etPhn.setText("");
                        etFrom.setText("");
                        etMeetWith.setText("");

                        etName.setText(name);
                        etPhn.setText(phn);
                        etFrom.setText(come_from);
                        etMeetWith.setText(meet_to);

                        for (int i = 0; i < MenuFragment.companyListData.size(); i++) {
                            if (MenuFragment.companyListData.get(i).getId() == Integer.parseInt(company)) {
                                //spinnerCompany.setSelection(i);
                                companyId = MenuFragment.companyListData.get(i).getId();
                                companyStr = MenuFragment.companyListData.get(i).getName();
                                tvCompany.setText(MenuFragment.companyListData.get(i).getName());
                                break;
                            }
                        }

                        for(int i = 0; i < MenuFragment.deptListData.size(); i++) {
                            if(MenuFragment.deptListData.get(i).getId() == Integer.parseInt(dept)) {
                                //spinnerDepartment.setSelection(i);
                                departmentId = MenuFragment.deptListData.get(i).getId();
                                departmentStr = MenuFragment.deptListData.get(i).getName();
                                tvDepartment.setText(MenuFragment.deptListData.get(i).getName());
                                break;
                            }
                        }

                        for (int i = 0; i < MenuFragment.purposeListData.size(); i++) {
                            if (MenuFragment.purposeListData.get(i).getId() == Integer.parseInt(purpose)) {
                                //spinnerPurpose.setSelection(i);
                                purposeId = MenuFragment.purposeListData.get(i).getId();
                                purposeStr = MenuFragment.purposeListData.get(i).getName();
                                tvPurpose.setText(MenuFragment.purposeListData.get(i).getName());
                                break;
                            }
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void handleVisitorCreate(int type, String staffId, String visitorName, String visitorPhn,
                                     String visitorComeFrom, String visitorMeetTo, String visitorCardNo, String visitorPersonNo,
                                     String visitorRemarks) {
        location = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.location, "location not found");
        String id = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_id, "user not found");
        String data = WellcomeActivity.sharedpreferences.getString(WellcomeActivity.user_data, "user data not found");

        VisitorInfoSearchOperation.sendVisitorCreateData(mActivity, type, staffId, visitorName, visitorPhn, visitorComeFrom,
                visitorMeetTo, companyId, departmentId, purposeId, visitorCardNo, visitorPersonNo, visitorRemarks, image,
                location, id, data, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getVisitorLogData) {
                        try {
                            String status = getVisitorLogData.getString("status");
                            if(status.equalsIgnoreCase("success")) {
                                selectorType = 0;
                                imgUploadState = 0;
                                image = "";
                                companyStr = "Press to select...";
                                departmentStr = "Press to select...";
                                purposeStr = "Press to select...";
                                showCustomDialogSuccess();
                            } else {
                                showCustomDialogError(status);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
    }

    private void showCustomDialogSuccess() {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_success, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText("Visitor create request successfully submitted");

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                fragment = new MenuFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment);
                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }

    private void showCustomDialogError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        ViewGroup viewGroup = mActivity.findViewById(android.R.id.content);

        final View dialogView = LayoutInflater.from(mActivity).inflate(R.layout.alert_dialog_error, viewGroup, false);

        builder.setView(dialogView);
        final AlertDialog alertDialog = builder.create();

        TextView tvMsg = dialogView.findViewById(R.id.tvMsg);
        tvMsg.setText("Visitor create request submission failed");

        Button btn = dialogView.findViewById(R.id.buttonOk);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
                fragment = new MenuFragment();
                FragmentManager fm = getActivity().getSupportFragmentManager();
                FragmentTransaction fragmentTransaction = fm.beginTransaction();
                fragmentTransaction.replace(R.id.frameLayout, fragment);
                fragmentTransaction.commit();
            }
        });
        alertDialog.show();
    }

    private void selectImage() {

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            startActivityForResult(takePictureIntent, 1);
        } catch (ActivityNotFoundException e) {
            // display error state to the user
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 1) {
                Bundle extras = data.getExtras();
                imageBitmap = (Bitmap) extras.get("data");
                bitMapToString(imageBitmap);
                imageView.setImageBitmap(imageBitmap);
            }
        }
    }

    public String bitMapToString(Bitmap userImage1) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        userImage1.compress(Bitmap.CompressFormat.PNG, 60, baos);
        byte[] b = baos.toByteArray();
        image = Base64.encodeToString(b, Base64.DEFAULT);
        tvUploadImg.setText(R.string.uploaded_txt);
        imgUploadState = 1;
        return image;
    }

    private boolean checkPermission() {
        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {
            // Permission is not granted
            return false;
        }
        return true;
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity,
                new String[]{Manifest.permission.CAMERA},
                PERMISSION_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    selectImage();
                } else {
                    Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_SHORT).show();
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (ContextCompat.checkSelfPermission(mActivity, Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                            showMessageOKCancel("You need to allow access permissions",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermission();
                                            }
                                        }
                                    });
                        }
                    }
                }
                break;
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mActivity)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }


    private void navigateToListFragment() {
        if (fragmentManager == null) return;
        ListFragment fragment = new ListFragment();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                .replace(R.id.frameLayout, fragment)
                .addToBackStack("data")
                .commit();
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle args = getArguments();
        if (args != null) {
            data = args.getString("bundle_filter");

            if(selectorType == 0) {
                cvVisitorInfo.setVisibility(View.GONE);
            } else {
                cvVisitorInfo.setVisibility(View.VISIBLE);
                if(imgUploadState == 1) {
                    tvUploadImg.setText(R.string.uploaded_txt);
                    imageView.setImageBitmap(imageBitmap);
                }

                tvCompany.setText(companyStr);
                tvDepartment.setText(departmentStr);
                tvPurpose.setText(purposeStr);
            }
        }
    }
}