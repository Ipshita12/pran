package com.prangroup.thirdEyePlus.Fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.CardView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.prangroup.thirdEyePlus.DataHandler.HandleUserJsonData;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Default.SimpleScannerActivity;
import com.prangroup.thirdEyePlus.Default.SimpleScannerExitActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;


public class MenuFragment_InActive extends Fragment  {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    public static Context mContext;
    public static Activity mActivity;
    Fragment fragment;
    private OnFragmentInteractionListener mListener;
    private ConnectionDetector connectionDetector;
    public static LinearLayout lllt,llrt,llir,llvir,llexitman,llgetinman;
    int track=1,gate=1;
    CardView cvlt;
    public MenuFragment_InActive() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static MenuFragment_InActive newInstance(String param1, String param2) {
        MenuFragment_InActive fragment = new MenuFragment_InActive();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        rootView=inflater.inflate(R.layout.fragment_iamenu_inactive, container, false);
        String menutxt = getResources().getString(R.string.menu_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>"+menutxt+" </font>"));

        return rootView;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
