package com.prangroup.thirdEyePlus.Fragments;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.prangroup.thirdEyePlus.Adapter.AutoCompleteAdapter;
import com.prangroup.thirdEyePlus.DataHandler.HandleExitReasonManJsonData;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Default.SimpleScannerExitManGPActivity;
import com.prangroup.thirdEyePlus.Model.VehINReassonDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.VehExitUtilityManOperation;
import com.prangroup.thirdEyePlus.ServerOperation.VehGetExitOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;


public class VehExitManFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View rootView;
    public static Context mContext;
    public static Activity mActivity;
    Fragment fragment;
    private OnFragmentInteractionListener mListener;
    private ConnectionDetector connectionDetector;
    public static LinearLayout lllt, vehselexit;
    Button btnsubmitexit;
    static TextView tvresultexit, tvgp;
    String disval, metroval = "", regval, veh1, veh2, fullvehname = "not found", resval, resval2, loadstatus;
    public static String scanresult, loadingStatus, datetime, IsFormatedRegistrationNumber = "yes", GPNumber = "Not Given";
    public EditText etveh1exit, etveh2exit, etveh2exit3;

    Spinner sploadexit, spinreasonexit;
    Spinner sp1exit, sp2exit, sp3exit;
    ArrayAdapter<String> resonAdapter, loadAdapter, adt1, adt2, adt3;
    public static ArrayList<VehINReassonDataModel> vehInReassonDB = new ArrayList<VehINReassonDataModel>();
    public static ArrayList<String> vehInReassonNameDB = new ArrayList<String>();
    public static ArrayList<String> vehInloadingDB = new ArrayList<String>();
    public static ArrayList<String> vehListDB = new ArrayList<String>();
    List<String> list1, list2, list3;
    AutoCompleteTextView autoVehcle;
    ArrayAdapter autoadapter;
    AutoCompleteAdapter autoCompleteAdapter;

    public VehExitManFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static VehExitManFragment newInstance(String param1, String param2) {
        VehExitManFragment fragment = new VehExitManFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment


        rootView = inflater.inflate(R.layout.fragment_vehexitman, container, false);
        String menutxt = getResources().getString(R.string.str_vehexitman_txt);
        MainActivity.actionBar.setTitle(Html.fromHtml("<font color='#FFFFFF'>" + menutxt + " </font>"));

        initialization();

        arraylist();


        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 1);
        ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        setAdapterVa();
        getUtilitydata();

        btnsubmitexit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                datetime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
                fullvehname = autoVehcle.getText().toString();
                if (fullvehname.equalsIgnoreCase("")) {

                    HelpingLib.showmessage1(mContext, "গাড়ি নির্বাচন করুন");
                } else if (loadstatus.equalsIgnoreCase("সিলেক্ট করুন")) {
                    HelpingLib.showmessage1(mContext, "লোডের অবস্থা  নির্বাচন করুন");
                } else if (resval2.equalsIgnoreCase("সিলেক্ট করুন")) {
                    HelpingLib.showmessage1(mContext, "লোডের কারণ নির্বাচন করুন");
                } else {
                    boolean res = HelpingLib.isInternetConnected(mContext);
                    if (res) {
                        sendDataToServer();
                    } else {
                        HelpingLib.showmessage1(mContext, "ইন্টারনেট সংযোগ নেই");
                    }
                }

                Log.e("fullvehname", fullvehname + "//" + resval + "//" + loadstatus);
//                veh1="";
//                disval="";
//                metroval="";
//                regval="";
//                veh1="";
//                veh2="";

            }
        });
        tvgp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mActivity, SimpleScannerExitManGPActivity.class);
                mActivity.startActivity(intent);
            }
        });


        return rootView;
    }

    public static void setGPNumber(String value) {
        GPNumber = value;
        tvgp.setText(value);
    }

    private void arraylist() {
        list1 = new ArrayList<String>();
        list1.add("সিলেক্ট করুন");
        list1.add("ঢাকা/DHAKA");
        list1.add("বাগেরহাট/BAGERHAT");
        list1.add("বান্দরবান/BANDARBAN");
        list1.add("বরগুনা/BARGUNA");
        list1.add("বরিশাল/BARISAL");
        list1.add("ভোলা/BHOLA");
        list1.add("বগুড়া/BOGRA");
        list1.add("ব্রাহ্মণবাড়িয়া/BRAHMANBARIA");
        list1.add("চাঁদপুর/CHANDPUR");
        list1.add("চাঁপাইনবাবগঞ্জ/CHAPAI");
        list1.add("চট্টগ্রাম/CHITTAGONG");
        list1.add("চুয়াডাঙ্গা/CHUADANGA");
        list1.add("কুমিল্লা/COMILLA");
        list1.add("কক্সবাজার/COX'S BAZAR");

        list1.add("ফরিদপুর/FARIDPUR");
        list1.add("ফেনী/FENI");
        list1.add("গাইবান্ধা/GAIBANDHA");
        list1.add("গাজীপুর/GAZIPUR");
        list1.add("গোপালগঞ্জ/GOPALGANJ");
        list1.add("হবিগঞ্জ/HABIGANJ");
        list1.add("জামালপুর/JAMALPUR");
        list1.add("যশোর/JESSORE");
        list1.add("ঝালকাঠি/JHALOKATI");
        list1.add("ঝিনাইদহ/JHENAIDAH");
        list1.add("জয়পুরহাট/JOYPURHAT");
        list1.add("খাগড়াছড়ি/KHAGRACHHARI");
        list1.add("খুলনা/KHULNA");
        list1.add("কিশোরগঞ্জ/KISHOREGANJ");
        list1.add("কুড়িগ্রাম/KURIGRAM");
        list1.add("কুষ্টিয়া/KUSHTIA");
        list1.add("লক্ষ্মীপুর/LAKSHMIPUR");
        list1.add("লালমনিরহাট/LALMONIRHAT");
        list1.add("মাদারিপুর/MADARIPUR");
        list1.add("মাগুরা/MAGURA");
        list1.add("মানিকগঞ্জ/MANIKGANJ");
        list1.add("মেহেরপুর/MEHERPUR");
        list1.add("মৌলভীবাজার/MOULVIBAZAR");
        list1.add("মুন্সিগঞ্জ/MUNSHIGANJ");
        list1.add("ময়মনসিংহ/MYMENSINGH");
        list1.add("নওগাঁ/NAOGAON");
        list1.add("নড়াইল/NARAIL");
        list1.add("নারায়ণগঞ্জ/NARAYANGANJ");
        list1.add("নরসিংদি/NARSINGDI");
        list1.add("নাটোর/NATORE");
        list1.add("নেত্রকোনা/NETROKONA");
        list1.add("নীলফামারি/NILPHAMARI");
        list1.add("নোয়াখালি/NOAKHALI");
        list1.add("পাবনা/PABNA");
        list1.add("পঞ্চগড়/PANCHAGARH");
        list1.add("পটুয়াখালি/PATUAKHALI");
        list1.add("পিরোজপুর/PIROJPUR");
        list1.add("রাজবাড়ি/RAJBARI");
        list1.add("রাজশাহী/RAJSHAHI");
        list1.add("রাঙামাটি/RANGAMATI");
        list1.add("রংপুর/RANGPUR");
        list1.add("সাতক্ষীরা/SATKHIRA");
        list1.add("শরিয়তপুর/SHARIATPUR");
        list1.add("শেরপুর/SHERPUR");
        list1.add("সিরাজগঞ্জ/SIRAJGANJ");
        list1.add("সুনামগঞ্জ/SUNAMGANJ");
        list1.add("বাগেরহাট/BAGERHAT");
        list1.add("সিলেট/SYLHET");
        list1.add("টাঙ্গাইল/TANGAIL");
        list1.add("ঠাকুরগাঁও/THAKURGAON");


        list2 = new ArrayList<String>();
        list2.add("সিলেক্ট করুন");
        list2.add("এ/A");
        list2.add("অ/AU");
        list2.add("ব/BA");
        list2.add("ভ/BHA");
        list2.add("ছ/CAA");
        list2.add("চ/CHA");
        list2.add("ড/DA");
        list2.add("দ/DAW");
        list2.add("ঢ/DHA");
        list2.add("ই/E");
        //  list2.add("ফ/FA");
        list2.add("গ/GA");
        list2.add("ঘ/GHA");
        //  list2.add("হ/HA");
        list2.add("জ/JA");
        list2.add("ঝ/JHA");
        list2.add("ক/KA");
        list2.add("খ/KHA");
        // list2.add("ল/LA");
        list2.add("ম/MA");
        list2.add("ন/NA");
        list2.add("প/PA");
        list2.add("র/RA");
        list2.add("স/SA");
        list2.add("শ/SHA");
        list2.add("ট/TA");
        list2.add("থ/TAW");
        list2.add("ঠ/THA");
        list2.add("ঊ/U");
        list2.add("ঙ/WUA");
        list2.add("য/ZA");

       /* list3 = new ArrayList<String>();
        list3.add("সিলেক্ট করুন");
        list3.add("-");
        list3.add("মেট্রো/Metro");*/

    }

    private void setAdapterVa() {
        adt1 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list1);
        adt1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp1exit.setAdapter(adt1);
        sp1exit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    disval = list1.get(i);

                    Log.e("Rs vdisval", disval.toString());
                    if (disval.equalsIgnoreCase("ঢাকা/DHAKA")
                            || disval.equalsIgnoreCase("চট্টগ্রাম/CHITTAGONG")
                            || disval.equalsIgnoreCase("খুলনা/KHULNA")
                            || disval.equalsIgnoreCase("রাজশাহী/RAJSHAHI")
                            || disval.equalsIgnoreCase("সিলেট/SYLHET")) {
                        list3 = new ArrayList<String>();
                        list3.add("সিলেক্ট করুন");
                        list3.add("-");
                        list3.add("মেট্রো/Metro");
                        set_list3Adapter();
                    } else {
                        list3 = new ArrayList<String>();
                        list3.add("সিলেক্ট করুন");
                        list3.add("-");
                        set_list3Adapter();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        /*adt2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list3);
        adt2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2exit.setAdapter(adt2);
        sp2exit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 1) {
                    //metroval=list3.get(i);
                    String stringVal = list3.get(i);
                    ;
                    String[] parts = stringVal.split("/");
                    String part2 = parts[1];
                    metroval = part2;
                } else {
                    metroval = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });*/

        adt3 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list2);
        adt3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp3exit.setAdapter(adt3);
        sp3exit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 0) {
                    regval = list2.get(i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void set_list3Adapter() {
        adt2 = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, list3);
        adt2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sp2exit.setAdapter(adt2);
        sp2exit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (i > 1) {
                    //metroval=list3.get(i);
                    String stringVal = list3.get(i);
                    ;
                    String[] parts = stringVal.split("/");
                    String part2 = parts[1];
                    metroval = part2;
                } else {
                    metroval = "";
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void getUtilitydata() {
        VehExitUtilityManOperation.getData(MainActivity.userid, mActivity, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVchLocData) {
                try {
                    String flag = getVchLocData.getString("flag");
                    if (flag.equalsIgnoreCase("found")) {

                        HandleExitReasonManJsonData.data(getVchLocData);
                        if (vehInReassonDB.size() > 0) {
                            setAdapterVal();
                        }
                    } else {
                        HelpingLib.showmessage1(mContext, "System error");
                        Log.e("ipshi", "here");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    Log.e("ipshi1", "here");
                }
            }
        });
    }

    private void setAdapterVal() {
        resonAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, vehInReassonNameDB);
        resonAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinreasonexit.setAdapter(resonAdapter);

        spinreasonexit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               /* if (i > 0) {
                    //resval=""+i;
                    resval = "" + vehInReassonDB.get(i - 1).getId();
                }*/
                resval = "";
                resval2 = "";
                resval2 = vehInReassonDB.get(i).getValue();
                resval = vehInReassonDB.get(i).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        loadAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, vehInloadingDB);
        loadAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sploadexit.setAdapter(loadAdapter);
        sploadexit.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                // if (i > 0) {
                loadstatus = vehInloadingDB.get(i);
                // }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        autoadapter = new
                ArrayAdapter(mContext, android.R.layout.simple_list_item_1, vehListDB);

//        autoVehcle.setAdapter(autoadapter);
        autoCompleteAdapter = new AutoCompleteAdapter(mContext, vehListDB);
        autoVehcle.setAdapter(autoCompleteAdapter);
        autoVehcle.setThreshold(1);
        autoVehcle.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                fullvehname = vehListDB.get(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public static int versionCode;
    public static String versionName;

    private void sendDataToServer() {
        try {
            final PackageInfo pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = (int) pInfo.getLongVersionCode(); // avoid huge version numbers and you will be ok
                versionName = pInfo.versionName;
            } else {
                //noinspection deprecation
                versionCode = pInfo.versionCode;
                versionName = pInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        VehGetExitOperation.sendGetInData(GPNumber, IsFormatedRegistrationNumber, fullvehname, MainActivity.userid,
                resval, loadstatus, datetime, mActivity, mContext, versionCode, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getVchLocData) {
                        Log.e("getVchLocData", getVchLocData.toString());
                        GPNumber = "Not Given";
                        try {
                            String flag = getVchLocData.getString("flag");
                            String msg = getVchLocData.getString("message");
                            Fragment fragment = new MenuFragment();
                            if (flag.equalsIgnoreCase("success")) {

                                HelpingLib.showmessage(mContext, msg);
                                redirect(fragment);
                            } else if (flag.equalsIgnoreCase("internal_error")) {
                                String errormsg = getVchLocData.getString("error_message");
                                HelpingLib.showmessage1(mContext, errormsg);
                                redirect(fragment);
                            } else {
                                //String msg=getVchLocData.getString("message");
                                HelpingLib.showmessage1(mContext, msg);
                                redirect(fragment);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Log.e("ipshi2", "here");
                        }
                        //Fragment fragment=new MenuFragment();
                        //redirect(fragment);
                    }
                });
    }

    private void initialization() {
        mContext = getContext();
        mActivity = getActivity();
        fragment = null;
        connectionDetector = new ConnectionDetector(MainActivity.context);
        vehselexit = (LinearLayout) rootView.findViewById(R.id.vehselexit);
        btnsubmitexit = (Button) rootView.findViewById(R.id.btnsubmitexit);
        tvresultexit = (TextView) rootView.findViewById(R.id.tvresultexit);
        tvgp = (TextView) rootView.findViewById(R.id.tvgpman);

        spinreasonexit = (Spinner) rootView.findViewById(R.id.spinreasonexit);
        sploadexit = (Spinner) rootView.findViewById(R.id.sploadexit);

        sp1exit = (Spinner) rootView.findViewById(R.id.sp1exit);
        sp2exit = (Spinner) rootView.findViewById(R.id.sp2exit);
        sp3exit = (Spinner) rootView.findViewById(R.id.sp3exit);

        etveh1exit = (EditText) rootView.findViewById(R.id.etveh1exit);
        etveh2exit = (EditText) rootView.findViewById(R.id.etveh2exit);
        etveh2exit3 = (EditText) rootView.findViewById(R.id.etveh2exit3);
        autoVehcle = (AutoCompleteTextView) rootView.findViewById(R.id.autovehcle);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void redirect(Fragment fragment) {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
