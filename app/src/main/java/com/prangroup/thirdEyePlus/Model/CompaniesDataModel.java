package com.prangroup.thirdEyePlus.Model;

public class CompaniesDataModel {

    private int id;
    private String name;

    public CompaniesDataModel(int company_id, String company_name) {
        id = company_id;
        name = company_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
