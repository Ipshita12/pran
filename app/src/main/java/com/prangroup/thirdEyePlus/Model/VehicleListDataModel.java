package com.prangroup.thirdEyePlus.Model;

public class VehicleListDataModel {
    private String fk_vehicle;
    private String registration_number;
    private String fk_parking_in_out;
    private int fk_location_building;
//    private int fk_loading_bay;

    public VehicleListDataModel(String fk_vehicle, String registration_number, String fk_parking_in_out, int fk_location_building) {
        this.fk_vehicle = fk_vehicle;
        this.registration_number = registration_number;
        this.fk_parking_in_out = fk_parking_in_out;
        this.fk_location_building = fk_location_building;
//        this.fk_loading_bay = fk_loading_bay;
    }

    public String getFk_vehicle() {
        return fk_vehicle;
    }

    public void setFk_vehicle(String fk_vehicle) {
        this.fk_vehicle = fk_vehicle;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getFk_parking_in_out() {
        return fk_parking_in_out;
    }

    public void setFk_parking_in_out(String fk_parking_in_out) {
        this.fk_parking_in_out = fk_parking_in_out;
    }

    public int getFk_location_building() {
        return fk_location_building;
    }

    public void setFk_location_building(int fk_location_building) {
        this.fk_location_building = fk_location_building;
    }

//    public int getFk_loading_bay() {
//        return fk_loading_bay;
//    }
//
//    public void setFk_loading_bay(int fk_loading_bay) {
//        this.fk_loading_bay = fk_loading_bay;
//    }
}
