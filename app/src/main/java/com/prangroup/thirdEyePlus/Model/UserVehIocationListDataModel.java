package com.prangroup.thirdEyePlus.Model;

import com.google.android.gms.maps.model.BitmapDescriptor;

public class UserVehIocationListDataModel {
    private String PK_Vehicle,NearestMapLocation,NearestMapLocationDistance,Latitude,Longitude;
    private String Speed,UpdateTime,RegistrationNumber,Internal_VehicleContactNumber,DepoName,Status;
    private int id;
    private BitmapDescriptor carIcon;
    public UserVehIocationListDataModel(int id,String PK_Vehicle,BitmapDescriptor carIcon, String nearestMapLocation, String nearestMapLocationDistance,
                                        String latitude, String longitude, String speed, String updateTime,
                                        String registrationNumber, String internal_VehicleContactNumber,
                                        String depoName, String status) {
        this.id=id;
        this.PK_Vehicle = PK_Vehicle;
        this.carIcon=carIcon;
        NearestMapLocation = nearestMapLocation;
        NearestMapLocationDistance = nearestMapLocationDistance;
        Latitude = latitude;
        Longitude = longitude;
        Speed = speed;
        UpdateTime = updateTime;
        RegistrationNumber = registrationNumber;
        Internal_VehicleContactNumber = internal_VehicleContactNumber;
        DepoName = depoName;
        Status = status;
    }

    public String getPK_Vehicle() {
        return PK_Vehicle;
    }

    public String getNearestMapLocation() {
        return NearestMapLocation;
    }

    public String getNearestMapLocationDistance() {
        return NearestMapLocationDistance;
    }

    public String getLatitude() {
        return Latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public String getSpeed() {
        return Speed;
    }

    public String getUpdateTime() {
        return UpdateTime;
    }

    public String getRegistrationNumber() {
        return RegistrationNumber;
    }

    public String getInternal_VehicleContactNumber() {
        return Internal_VehicleContactNumber;
    }

    public String getDepoName() {
        return DepoName;
    }

    public String getStatus() {
        return Status;
    }

    public BitmapDescriptor getCarIcon() {
        return carIcon;
    }

    public int getId() {
        return id;
    }


}
