package com.prangroup.thirdEyePlus.Model;

public class LoggedUserDataModel {
    private String PK_User,FullName,UniqueIDNumber,ContactNumber,Email;

    public LoggedUserDataModel(String PK_User, String fullName, String uniqueIDNumber, String contactNumber, String email) {
        this.PK_User = PK_User;
        FullName = fullName;
        UniqueIDNumber = uniqueIDNumber;
        ContactNumber = contactNumber;
        Email = email;
    }

    public String getPK_User() {
        return PK_User;
    }

    public void setPK_User(String PK_User) {
        this.PK_User = PK_User;
    }

    public String getFullName() {
        return FullName;
    }

    public void setFullName(String fullName) {
        FullName = fullName;
    }

    public String getUniqueIDNumber() {
        return UniqueIDNumber;
    }

    public void setUniqueIDNumber(String uniqueIDNumber) {
        UniqueIDNumber = uniqueIDNumber;
    }

    public String getContactNumber() {
        return ContactNumber;
    }

    public void setContactNumber(String contactNumber) {
        ContactNumber = contactNumber;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }
}
