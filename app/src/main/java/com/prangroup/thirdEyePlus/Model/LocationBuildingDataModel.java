package com.prangroup.thirdEyePlus.Model;

public class LocationBuildingDataModel {
    private int pk_location_building;
    private String pk_location_building_name;

    public LocationBuildingDataModel(int pk_location_building, String pk_location_building_name) {
        this.pk_location_building = pk_location_building;
        this.pk_location_building_name = pk_location_building_name;
    }

    public int getPk_location_building() {
        return pk_location_building;
    }

    public void setPk_location_building(int pk_location_building) {
        this.pk_location_building = pk_location_building;
    }

    public String getPk_location_building_name() {
        return pk_location_building_name;
    }

    public void setPk_location_building_name(String pk_location_building_name) {
        this.pk_location_building_name = pk_location_building_name;
    }

}
