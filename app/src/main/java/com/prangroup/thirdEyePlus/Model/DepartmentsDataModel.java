package com.prangroup.thirdEyePlus.Model;

public class DepartmentsDataModel {

    private int id;
    private String name;

    public DepartmentsDataModel(int dept_id, String dept_name) {
        id = dept_id;
        name = dept_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
