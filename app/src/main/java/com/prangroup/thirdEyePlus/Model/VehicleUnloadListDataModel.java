package com.prangroup.thirdEyePlus.Model;

public class VehicleUnloadListDataModel {
    private int pk_loading_bay_utilization;
    private String registration_number;
    private String location_building_name;
    private String loading_bay_name;
    private String user_type;
    private String start_date_time;

    public VehicleUnloadListDataModel(int pk_loading_bay_utilization, String registration_number, String location_building_name,
                                      String loading_bay_name, String user_type, String start_date_time) {
        this.pk_loading_bay_utilization = pk_loading_bay_utilization;
        this.registration_number = registration_number;
        this.location_building_name = location_building_name;
        this.loading_bay_name = loading_bay_name;
        this.user_type = user_type;
        this.start_date_time = start_date_time;
    }

    public int getPk_loading_bay_utilization() {
        return pk_loading_bay_utilization;
    }

    public void setPk_loading_bay_utilization(int pk_loading_bay_utilization) {
        this.pk_loading_bay_utilization = pk_loading_bay_utilization;
    }

    public String getRegistration_number() {
        return registration_number;
    }

    public void setRegistration_number(String registration_number) {
        this.registration_number = registration_number;
    }

    public String getLocation_building_name() {
        return location_building_name;
    }

    public void setLocation_building_name(String location_building_name) {
        this.location_building_name = location_building_name;
    }

    public String getLoading_bay_name() {
        return loading_bay_name;
    }

    public void setLoading_bay_name(String loading_bay_name) {
        this.loading_bay_name = loading_bay_name;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getStart_date_time() {
        return start_date_time;
    }

    public void setStart_date_time(String start_date_time) {
        this.start_date_time = start_date_time;
    }
}
