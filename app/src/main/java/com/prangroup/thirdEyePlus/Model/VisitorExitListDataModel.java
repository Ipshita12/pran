package com.prangroup.thirdEyePlus.Model;

public class VisitorExitListDataModel {

    private int id;
    private String name;
    private String phn;
    private String inTime;
    private String outTime;
    private String image;

    public VisitorExitListDataModel(int user_id,
                                    String visitorName,
                                    String visitorPhn,
                                    String visitorInTime,
                                    String visitorOutTime,
                                    String img) {
        id = user_id;
        name = visitorName;
        phn = visitorPhn;
        inTime = visitorInTime;
        outTime = visitorOutTime;
        image = img;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhn() {
        return phn;
    }

    public void setPhn(String phn) {
        this.phn = phn;
    }

    public String getInTime() {
        return inTime;
    }

    public void setInTime(String inTime) {
        this.inTime = inTime;
    }

    public String getOutTime() {
        return outTime;
    }

    public void setOutTime(String outTime) {
        this.outTime = outTime;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
