package com.prangroup.thirdEyePlus.Model;

public class PurposesDataModel {

    private int id;
    private String name;

    public PurposesDataModel(int purpose_id, String purpose_name) {
        id = purpose_id;
        name = purpose_name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
