package com.prangroup.thirdEyePlus.Model;

public class LoadingBayDataModel {
    private int pk_loading_bay;
    private int fk_location_building;
    private String loading_bay_name;

    public LoadingBayDataModel(int pk_loading_bay, int fk_location_building, String loading_bay_name) {
        this.pk_loading_bay = pk_loading_bay;
        this.fk_location_building = fk_location_building;
        this.loading_bay_name = loading_bay_name;
    }

    public int getPk_loading_bay() {
        return pk_loading_bay;
    }

    public void setPk_loading_bay(int pk_loading_bay) {
        this.pk_loading_bay = pk_loading_bay;
    }

    public int getFk_location_building() {
        return fk_location_building;
    }

    public void setFk_location_building(int fk_location_building) {
        this.fk_location_building = fk_location_building;
    }

    public String getLoading_bay_name() {
        return loading_bay_name;
    }

    public void setLoading_bay_name(String loading_bay_name) {
        this.loading_bay_name = loading_bay_name;
    }
}
