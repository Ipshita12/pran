package com.prangroup.thirdEyePlus.Adapter;

import android.content.Context;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserVehLocInfoListJsonData;
import com.prangroup.thirdEyePlus.Utilities.MyItem;

public class OwnIconRendered extends DefaultClusterRenderer<MyItem> {
    public OwnIconRendered(Context context, GoogleMap map,
                           ClusterManager<MyItem> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onBeforeClusterItemRendered(MyItem item, MarkerOptions markerOptions) {
        int id=Integer.valueOf(item.getId());
        BitmapDescriptor carIcon= HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(id).getCarIcon();
        markerOptions.icon(carIcon);
        markerOptions.title(""+id);
        super.onBeforeClusterItemRendered(item, markerOptions);
    }
}
