package com.prangroup.thirdEyePlus.Adapter;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.MyItem;

public class MyCustomAdapterForItems implements GoogleMap.InfoWindowAdapter{

    private final View myContentsView;
    private MyItem clickedClusterItem;
    private Marker myMarker;
    public MyCustomAdapterForItems(Activity mActivity){
        myContentsView=mActivity.getLayoutInflater().inflate(R.layout.map_info_window,null);
    }

    @Override
    public View getInfoWindow(Marker marker) {
        TextView tvTitle = ((TextView) myContentsView
                .findViewById(R.id.textViewTitle));

        TextView tvLocationType = ((TextView) myContentsView
                .findViewById(R.id.textViewLocationType));

        TextView tvSnippet = ((TextView) myContentsView
                .findViewById(R.id.textViewSnippet));

        tvTitle.setText("item");

        //String locationType = "Address:  "+clickedClusterItem.getmLocationType();
        //tvLocationType.setText(locationType.toUpperCase());
        //tvSnippet.setText(clickedClusterItem.getmSnippet());

        myMarker = marker;

        return myContentsView;
    }

    @Override
    public View getInfoContents(Marker marker) {
        return null;
    }

}

