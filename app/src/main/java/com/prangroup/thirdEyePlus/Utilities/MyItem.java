package com.prangroup.thirdEyePlus.Utilities;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyItem implements ClusterItem {
    private final LatLng mPosition;
    private int id;

    public MyItem(double lat, double lng,int id) {
        mPosition = new LatLng(lat, lng);
        this.id=id;

    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return null;
    }

    @Override
    public String getSnippet() {
        return null;
    }

    public int getId() {
        return id;
    }
}

