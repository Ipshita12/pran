package com.prangroup.thirdEyePlus.Utilities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnSuccessListener;
import com.prangroup.thirdEyePlus.Auth.RegistrationActivity;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserVehLocInfoListJsonData;
import com.prangroup.thirdEyePlus.R;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by CS-MIS-HW-Altaf on 11/13/2017.
 */

public class HelpingLib {
    public static LatLng curloc = null;
    public static String otp = "no";

    public static boolean isInternetConnected(Context context) {
        boolean connected = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);
        if (connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
            //we are connected to a network
            connected = true;
        } else
            connected = false;
        return connected;
    }

    public static void showmessage(Context context, String msgBody) {
        final int LONG_DELAY = 5000;
        //final Toast toast=Toast.makeText(context, msgBody, Toast.LENGTH_LONG);
        // toast.show();
        final Toast toast = Toast.makeText(context, "  " + msgBody + "  ", Toast.LENGTH_LONG);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
       // v.setTextColor(Color.GREEN);
        v.setTextSize(20);
        v.setTextColor(Color.parseColor("#088a4b"));
        View vieew = toast.getView();
        vieew.setBackgroundResource(R.drawable.textinputborder_new);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(vieew);
        toast.show();
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 5000);
    }

    public static void showmessage1(Context context, String msgBody) {
        final int LONG_DELAY = 5000;
        //  final Toast toast=Toast.makeText(context, msgBody, Toast.LENGTH_LONG);
        // toast.show();
        final Toast toast = Toast.makeText(context, "  " + msgBody + "  ", Toast.LENGTH_LONG);
        TextView v = (TextView) toast.getView().findViewById(android.R.id.message);
        v.setTextColor(Color.RED);
        v.setTextSize(20);
        View vieew = toast.getView();
        vieew.setBackgroundResource(R.drawable.textinputborder_new);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.setView(vieew);
        toast.show();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                toast.cancel();
            }
        }, 5000);
    }

    public static String getCurrDate() {

        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String Date = format.format(curDate);
        return Date;
    }

    public static String getCurrTime() {

        Date curDate = new Date();
        SimpleDateFormat format = new SimpleDateFormat("hh:mm:ss a");
        String time = format.format(curDate);

        return time;
    }

    public static String deviceHW(Activity activity) {
        String deviceHW = "";
        DisplayMetrics displayMetrics = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;
        deviceHW = height + "-" + width;
        return deviceHW;
    }

//    public static String getAddressByLatLng(Context context, double lat, double lng) {
//        String address = "Not found";
//        Geocoder geocoder;
//        List<Address> addresses;
//        geocoder = new Geocoder(context, Locale.getDefault());
//        try {
//            addresses = geocoder.getFromLocation(lat, lng, 1);
//            address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
//            String city = addresses.get(0).getLocality();
//            String state = addresses.get(0).getAdminArea();
//            String country = addresses.get(0).getCountryName();
//            String postalCode = addresses.get(0).getPostalCode();
//
//            CurrentLocationDB currentLocationDB = new CurrentLocationDB(address, city, state, country, postalCode, lat, lng);
//            MainActivity.currentLocationDB.add(currentLocationDB);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//        return address;
//    }

    public static void animateGMap(GoogleMap mMap, LatLng location, int zoom) {

        CameraPosition cameraPosition = new CameraPosition.Builder().target(location).zoom(zoom).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    public static void currentLocation(final Context context, FusedLocationProviderClient mFusedLocationClient, Activity activity) {

        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        mFusedLocationClient.getLastLocation()
                .addOnSuccessListener(activity, new OnSuccessListener<Location>() {
                    @Override
                    public void onSuccess(Location curLoc) {
                        // Got last known location. In some rare situations this can be null.
                        if (curLoc != null) {
                            // Logic to handle location object
                            double lat = curLoc.getLatitude();
                            double lng = curLoc.getLongitude();
                            curloc = new LatLng(lat, lng);
//                            String address=HelpingLib.getAddressByLatLng(context,lat,lng);
//                            tvsource_point.setText(address);
                            //HelpingLib.animateGMap(mMap,curloc,ZOOMLVL);
                        }
                    }
                });

    }

//    public static int getVehIDByVehName(String vehName) {
//        int id = -1;
//        for (int i = 0; i < HandleUserVehLocInfoListJsonData.userVehLocListDataDB.size(); i++) {
//            String dbVehName = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getRegistrationNumber();
//            if (dbVehName.equalsIgnoreCase(vehName)) {
//                return id = i;
//            }
//        }
//        return id;
//    }
//
//    public static String getVehIDDByVehName(String vehName) {
//        String id = "-1";
//        for (int i = 0; i < HandleVehicleTypeCapacityJsonData.vehicleNameListDB.size(); i++) {
//            String dbVehName = HandleVehicleTypeCapacityJsonData.vehicleNameListDB.get(i).getRegistrationNumber();
//            if (dbVehName.equalsIgnoreCase(vehName)) {
//                id = HandleVehicleTypeCapacityJsonData.vehicleNameListDB.get(i).getId();
//            }
//        }
//        return id;
//    }
//
//    public static String getDriverByDriverName(String driverName) {
//        String id = "-1";
//        for (int i = 0; i < HandleVehicleTypeCapacityJsonData.driverNameListDB.size(); i++) {
//            String dbDriverName = HandleVehicleTypeCapacityJsonData.driverNameListDB.get(i).getName();
//            if (dbDriverName.equalsIgnoreCase(driverName)) {
//                id = HandleVehicleTypeCapacityJsonData.driverNameListDB.get(i).getId();
//            }
//        }
//        return id;
//    }

    public static String fetchLastMsg(final String mobNo, final Context context) {
        int GET_MY_PERMISSION = 1;
        if (ContextCompat.checkSelfPermission(RegistrationActivity.context, Manifest.permission.READ_SMS)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(RegistrationActivity.activity,
                    Manifest.permission.READ_SMS)) {
                /* do nothing*/

            } else {

                ActivityCompat.requestPermissions(RegistrationActivity.activity,
                        new String[]{Manifest.permission.READ_SMS}, GET_MY_PERMISSION);
                fetchLastMsg(mobNo, context);
            }
        } else {
            final String SMS_URI_INBOX = "content://sms/inbox";

            Uri uri = Uri.parse(SMS_URI_INBOX);
            String[] projection = new String[]{"_id", "address", "person", "body", "date", "type"};
            Cursor cur = context.getContentResolver().query(uri, projection, "address=" + mobNo, null, "date desc");
            //count=""+cur.getCount();
            if (cur.moveToFirst()) {
                String msg = cur.getString(3);
                String[] parts = msg.split(":");
                String part2 = parts[1];
                otp = part2.trim();
            }
            return otp;
        }

        return "";


    }


    public static int getVehIDByVehName(String vehName) {
        int id = -1;
        for (int i = 0; i < HandleUserVehLocInfoListJsonData.userVehLocListDataDB.size(); i++) {
            String dbVehName = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getRegistrationNumber();
            if (dbVehName.equalsIgnoreCase(vehName)) {
                return id = i;
            }
        }
        return id;
    }

    public static boolean isActiveInternet() {
        boolean connected = false;
        try {
            HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
            urlc.setRequestProperty("User-Agent", "Test");
            urlc.setRequestProperty("Connection", "close");
            urlc.setConnectTimeout(3000); //choose your own timeframe
            urlc.setReadTimeout(4000); //choose your own timeframe
            urlc.connect();
            //int networkcode2 = urlc.getResponseCode();
            if (urlc.getResponseCode() == 200) {
                connected = true;
            } else {
                connected = false;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return connected;
    }

}
