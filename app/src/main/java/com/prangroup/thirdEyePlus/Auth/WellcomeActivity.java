package com.prangroup.thirdEyePlus.Auth;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.prangroup.thirdEyePlus.DataHandler.HandleUserJsonData;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.UserLoginOperation;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;


public class WellcomeActivity extends AppCompatActivity {

    public static Activity activity;
    public static Context context;
    public static SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public static final String un = "username";
    public static final String pw = "password";
    public static final String location = "location";
    public static final String user_id = "userid";
    public static final String user_data = "json_data";
    String staffid, password;
    public static int versionCode;
    public static String versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wellcome);
        activity = (Activity) this;
        context = (Context) this;
        Intent aLoginIntent = new Intent(WellcomeActivity.this, LoginActivity.class);
        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        try {
            final PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = (int) pInfo.getLongVersionCode(); // avoid huge version numbers and you will be ok
                versionName = pInfo.versionName;
            } else {
                //noinspection deprecation
                versionCode = pInfo.versionCode;
                versionName = pInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        staffid = sharedpreferences.getString(un, "un not found");
        if (!staffid.equalsIgnoreCase("un not found")) {
            password = sharedpreferences.getString(pw, "pw not found");
            if (!password.equalsIgnoreCase("pw not found")) {

                UserLoginOperation.sendLogionData(staffid, password, versionCode, versionName, activity, context, new VolleyCallBack() {
                    @Override
                    public void onSuccess(JSONObject getVchLocData) {
                        int lngth = getVchLocData.length();
                        try {
                            String status = getVchLocData.getString("flag");
                            if (status.equalsIgnoreCase("Success")) {

                                String requisitiontxt = context.getResources().getString(R.string.login_suc_txt);
                                HelpingLib.showmessage(context, requisitiontxt);

                                if (lngth > 0) {
                                    HandleUserJsonData.loggedUserData(getVchLocData, staffid, password);
                                    if (HandleUserJsonData.loogedUserDataModelDB.size() > 0) {

                                        Intent dashboardIntent = new Intent(context, MainActivity.class);
                                        startActivity(dashboardIntent);

                                        // Intent dashboardIntent = new Intent(context, MiddleWearUserSelectActivity.class);
                                        // startActivity(dashboardIntent);
                                    }
                                }
                            } else {
                                String requisitiontxt = LoginActivity.context.getResources().getString(R.string.login_fail_txt);
                                HelpingLib.showmessage(context, requisitiontxt);//login fail
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });
            } else {
                //password not found
                startActivity(aLoginIntent);
            }
        } else {
            //staffid not found
            startActivity(aLoginIntent);
        }

    }
}
