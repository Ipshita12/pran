package com.prangroup.thirdEyePlus.Auth;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.UserRegistrationOperation;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.Utilities.Salahuddinlib;


public class RegistrationActivity extends AppCompatActivity {
    Button btnMob, btnOTP, btn_reg;
    public static EditText input_mob, input_otp, input_fn, input_pw, input_repw;
    public static Activity activity;
    public static Context context;
    public static String mobNo, otp, PK_User, otpdb, fn, pw, repw, deviceID,fid,otpInbox;
    public static LinearLayout linlayoutmob, linlayoutotp, linlayoutbasic;
    public static FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        initialize();

        Salahuddinlib kazilib = new Salahuddinlib();
        kazilib.displayLocationSettingsRequest(context, activity);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        HelpingLib.currentLocation(context, mFusedLocationClient, activity);

        btnMob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleMob();
            }
        });

        btnOTP.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleOtp();
            }
        });



    }



    private void handleOtp() {
        if (input_otp.getText().toString().equalsIgnoreCase("")) {
            String useriderrtxt = getResources().getString(R.string.userotp_err_txt);
            input_otp.setError(useriderrtxt);
            return;
        }

        otp = input_otp.getText().toString();
        if (RegistrationActivity.otpdb.equalsIgnoreCase(otp)) {
            linlayoutotp.setVisibility(View.GONE);
            register();
        } else {
            String otpError = RegistrationActivity.context.getString(R.string.userotp_sub_err_txt);
            HelpingLib.showmessage(RegistrationActivity.context, otpError);
        }

    }


    private void initialize() {

        activity = (Activity) this;
        context = (Context) this;
        btnMob = (Button) findViewById(R.id.btn_mob);
        input_mob = (EditText) findViewById(R.id.input_mob);
        linlayoutmob = (LinearLayout) findViewById(R.id.linlayoutmob);

        btnOTP = (Button) findViewById(R.id.btn_otp);
        input_otp = (EditText) findViewById(R.id.input_otp);
        linlayoutotp = (LinearLayout) findViewById(R.id.linlayoutotp);

        btn_reg = (Button) findViewById(R.id.btn_reg);
        input_fn = (EditText) findViewById(R.id.input_fn);
        input_pw = (EditText) findViewById(R.id.input_pw);
        input_repw = (EditText) findViewById(R.id.input_repw);
        linlayoutbasic = (LinearLayout) findViewById(R.id.linlayoutbasic);
    }

    private void handleMob() {
        fn = input_fn.getText().toString();
        mobNo = input_mob.getText().toString();

        if (mobNo.equalsIgnoreCase("")) {
            String useriderrtxt = getResources().getString(R.string.usermob_err_txt);
            input_mob.setError(useriderrtxt);
            return;
        }

        if (fn.equalsIgnoreCase("")) {
            String useriderrtxt = getResources().getString(R.string.fullname_txt);
            input_fn.setError(useriderrtxt);
            return;
        }


        if (HelpingLib.isInternetConnected(context)) {
            getIMEI(context, activity);
            UserRegistrationOperation.sendMobData();
        } else {
            String intconnerr = getString(R.string.INT_CON_ERR);
            HelpingLib.showmessage(context, intconnerr);
        }

    }

    @Override
    public void onBackPressed() {
        logoutConformation();
    }

    private void logoutConformation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(RegistrationActivity.this);
        builder.setMessage("Are you sure you want to close apps?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        finish();
                        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_left);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }

    public static void getIMEI(Context context, Activity activity) {

        deviceID = Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID); ;

    }

    public static void register(){
        //UserRegFinalOperation.sendRegFinalData(activity,context);
    }
}
