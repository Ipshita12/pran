package com.prangroup.thirdEyePlus.Auth;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserJsonData;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.UserLoginOperation;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.Utilities.Salahuddinlib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;


public class LoginActivity extends AppCompatActivity {
    Button btnLogin;
    int status;
    private EditText inputStaffId, inputPassword;
    public static Activity activity;
    public static Context context;
    public static String staffid, password;
    TextView txtwelcome, txtlogin, txtnotreg;
    public static int versionCode;
    public static String versionName;

    public static FusedLocationProviderClient mFusedLocationClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialize();

        Salahuddinlib kazilib = new Salahuddinlib();
        kazilib.displayLocationSettingsRequest(context, activity);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        HelpingLib.currentLocation(context, mFusedLocationClient, activity);
        fontInitialize();

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                handleLogin();
            }
        });

        txtnotreg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regIntent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(regIntent);
            }
        });

    }

    private void fontInitialize() {
        Typeface custom_font_robotobold = Typeface.createFromAsset(getAssets(), "RobotoBold.ttf");
        Typeface custom_font_robotobolditalic = Typeface.createFromAsset(getAssets(), "RobotoCondensed-BoldItalic.ttf");

        txtwelcome.setTypeface(custom_font_robotobold);
        txtlogin.setTypeface(custom_font_robotobolditalic);
        btnLogin.setTypeface(custom_font_robotobold);
    }

    private void initialize() {

        activity = (Activity) this;
        context = (Context) this;
        btnLogin = (Button) findViewById(R.id.btn_login);
        inputStaffId = (EditText) findViewById(R.id.input_staff_id);
        inputPassword = (EditText) findViewById(R.id.input_password);
        txtwelcome = (TextView) findViewById(R.id.txtwelcome);
        txtlogin = (TextView) findViewById(R.id.txtlogin);
        txtnotreg = (TextView) findViewById(R.id.txtnotreg);
    }

    private void handleLogin() {

        try {
            final PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = (int) pInfo.getLongVersionCode(); // avoid huge version numbers and you will be ok
                versionName = pInfo.versionName;
            } else {
                //noinspection deprecation
                versionCode = pInfo.versionCode;
                versionName = pInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        if (inputStaffId.getText().toString().equalsIgnoreCase("")) {
            String useriderrtxt = getResources().getString(R.string.userid_err_txt);
            inputStaffId.setError(useriderrtxt);
            return;
        }

        if (inputPassword.getText().toString().length() == 0) {
            String pwerrtxt = getResources().getString(R.string.pw_err_txt);
            inputPassword.setError(pwerrtxt);
            return;
        }

        staffid = inputStaffId.getText().toString();
        password = inputPassword.getText().toString();
        if (HelpingLib.isInternetConnected(context)) {
            UserLoginOperation.sendLogionData(staffid, password, versionCode, versionName, activity, context, new VolleyCallBack() {
                @Override
                public void onSuccess(JSONObject getVchLocData) {
                    int lngth = getVchLocData.length();

                    try {
                        String status = getVchLocData.getString("flag");
                        if (status.equalsIgnoreCase("Success")) {

                            String requisitiontxt = context.getResources().getString(R.string.login_suc_txt);
                            HelpingLib.showmessage(context,
                                    requisitiontxt);
                            if (lngth > 0) {
                                HandleUserJsonData.loggedUserData(getVchLocData, staffid, password);
                                if (HandleUserJsonData.loogedUserDataModelDB.size() > 0) {

                                    Intent dashboardIntent = new Intent(context, MainActivity.class);
                                    startActivity(dashboardIntent);
                                    // Intent dashboardIntent = new Intent(context, MiddleWearUserSelectActivity.class);
                                    //startActivity(dashboardIntent);
                                }
                            }
                        } else {
                            String requisitiontxt = LoginActivity.context.getResources().getString(R.string.login_fail_txt);
                            HelpingLib.showmessage1(context, requisitiontxt);//login fail
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } else {
            String intconnerr = getString(R.string.INT_CON_ERR);
            HelpingLib.showmessage(context, intconnerr);
        }


    }

    @Override
    public void onBackPressed() {
        //System.exit(0);
        //finish();
        logoutConformation();
    }

    private void logoutConformation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage("Are you sure you want to close apps?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        finishAffinity();
                        System.exit(0);
                        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_left);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();

    }
}
