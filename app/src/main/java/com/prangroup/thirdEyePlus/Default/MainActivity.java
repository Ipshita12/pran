package com.prangroup.thirdEyePlus.Default;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.prangroup.thirdEyePlus.Auth.LoginActivity;
import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserJsonData;
import com.prangroup.thirdEyePlus.Fragments.AboutFragment;
import com.prangroup.thirdEyePlus.Fragments.DefaultFragment;
import com.prangroup.thirdEyePlus.Fragments.ListFragment;
import com.prangroup.thirdEyePlus.Fragments.LiveFragment;
import com.prangroup.thirdEyePlus.Fragments.LoadUnloadEndFragment;
import com.prangroup.thirdEyePlus.Fragments.LoadUnloadStartFragment;
import com.prangroup.thirdEyePlus.Fragments.MenuFragment;
import com.prangroup.thirdEyePlus.Fragments.MenuFragment_InActive;
import com.prangroup.thirdEyePlus.Fragments.UserMenuFragment;
import com.prangroup.thirdEyePlus.Fragments.VehExitFragment;
import com.prangroup.thirdEyePlus.Fragments.VehExitManFragment;
import com.prangroup.thirdEyePlus.Fragments.VehINFragment;
import com.prangroup.thirdEyePlus.Fragments.VehINManualFragment;
import com.prangroup.thirdEyePlus.Fragments.VisitorCreateFragment;
import com.prangroup.thirdEyePlus.Fragments.VisitorExitFragment;
import com.prangroup.thirdEyePlus.Model.LoggedUserDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.GetUserControlOperation;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.Utilities.Salahuddinlib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        LiveFragment.OnFragmentInteractionListener,
        DefaultFragment.OnFragmentInteractionListener,
        MenuFragment.OnFragmentInteractionListener,
        VehINFragment.OnFragmentInteractionListener,
        VehINManualFragment.OnFragmentInteractionListener,
        VehExitFragment.OnFragmentInteractionListener,
        VehExitManFragment.OnFragmentInteractionListener,
        AboutFragment.OnFragmentInteractionListener,
        UserMenuFragment.OnFragmentInteractionListener,
        VisitorCreateFragment.OnFragmentInteractionListener,
        VisitorExitFragment.OnFragmentInteractionListener,
        ListFragment.OnFragmentInteractionListener,
        LoadUnloadStartFragment.OnFragmentInteractionListener,
        LoadUnloadEndFragment.OnFragmentInteractionListener {

    public static Activity activity;
    public static Context context;
    public static int myAPI, locationAction = 1, alert = 1, val, value, log = 2, reqType, liveApi;
    public static double latitude, longitude;
    NavigationView navigationView;
    public static View headerLayout;
    public static String imagePath, userType, userid, user_id;
    public static ImageView userPhoto;
    public static TextView userName, userEmail;
    public static ActionBar actionBar;
    public static Toolbar toolbar;
    public static LoggedUserDataModel loggedUserDataModel;
    public static FragmentManager fragmentManager;
    Fragment fragment;
    int fragVal;
    public static FusedLocationProviderClient mFusedLocationClient;
    public static ArrayList<String> menuList = new ArrayList<>();
    public static int track = 0, gate = 0, load = 0, visit = 0;

    SharedPreferences appData;
    public static String v_type_id;
    public static String v_type_name;
    public static final String MyPREFERENCES = "MyPrefs";
    public static SharedPreferences sharedpreferences;
    public static int versionCode;
    public static String versionName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        activity = (Activity) this;
        context = (Context) this;

        appData = PreferenceManager.getDefaultSharedPreferences(this);

        sharedpreferences = getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity);
        Salahuddinlib kazilib = new Salahuddinlib();
        kazilib.displayLocationSettingsRequest(context, activity);

        HelpingLib.currentLocation(context, mFusedLocationClient, activity);

        v_type_id = appData.getString("v_type_id", "");
        v_type_name = appData.getString("v_type_name", "");

        fragmentManager = getSupportFragmentManager();

        loggedUserDataModel = HandleUserJsonData.loogedUserDataModelDB.get(0);
        userid = loggedUserDataModel.getPK_User();

        user_id = sharedpreferences.getString("username", "un not found");

        initialize();
        fragment = new MenuFragment();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        sendDataToServer_Search(userid);
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//                // Do something after 5s = 5000ms
//                goTo(fragment);
//            }
//        }, 5000);
    }

    private void initialize() {
        actionBar = getSupportActionBar();
        navigationView = (NavigationView) findViewById(R.id.nav_view);
        headerLayout = navigationView.getHeaderView(0);
        userPhoto = (ImageView) headerLayout.findViewById(R.id.imageView);
        userName = (TextView) headerLayout.findViewById(R.id.txtName);
        userEmail = (TextView) headerLayout.findViewById(R.id.tvEmail);
        userName.setText(loggedUserDataModel.getFullName());
    }

    @Override
    public void onBackPressed() {
        // ExitConformation();
        if (getSupportFragmentManager().getBackStackEntryCount() > 1) {
            getSupportFragmentManager().popBackStack();
        }
    }

    private void ExitConformation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.context);
        builder.setMessage("আপনি কি করতে চান?")
                .setCancelable(false)
                .setPositiveButton("বাইরে যেতে চাই", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {

                        dialog.cancel();
                        finishAffinity();
                        System.exit(0);

                    }
                })
                .setNegativeButton("কিছু না", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        HelpingLib.showmessage(MainActivity.context, "এখানে থাকার জন্য আপনাকে ধন্যবাদ");
                    }
                }).setNeutralButton("লগআউট", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {

                SharedPreferences.Editor aEditor = WellcomeActivity.sharedpreferences.edit();
                aEditor.putString(WellcomeActivity.un, "un not found");
                aEditor.putString(WellcomeActivity.pw, "pw not found");
                aEditor.commit();

                Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(loginIntent);
                finish();
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_left);
            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void logoutConformation() {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.context);
        builder.setMessage("আপনি কি করতে চান?")
                .setCancelable(false)
                .setNeutralButton("লগআউট", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface builder, int id) {
                        fragVal = 1;
                        SharedPreferences.Editor aEditor = WellcomeActivity.sharedpreferences.edit();
                        aEditor.putString(WellcomeActivity.un, "un not found");
                        aEditor.putString(WellcomeActivity.pw, "pw not found");
                        aEditor.commit();

                        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(loginIntent);
                        finish();
                        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_left);

                    }
                })
                .setPositiveButton("বাইরে যেতে চাই", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface builder, int id) {
                        fragVal = 1;
                        builder.cancel();
                        finishAffinity();
                        System.exit(0);
                    }
                }).setNegativeButton(Html.fromHtml("কিছু না"), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface builder, int id) {
                /*Toast toast = Toast.makeText(MainActivity.context, "এখানে থাকার জন্য আপনাকে ধন্যবাদ", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();*/
                fragVal = 2;
                builder.cancel();
                HelpingLib.showmessage(MainActivity.context, "এখানে থাকার জন্য আপনাকে ধন্যবাদ");
            }
        });
        /* AlertDialog alert = builder.create();*/
        builder.show();

    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment = null;
        if (id == R.id.nav_ia_home) {
            sendDataToServer_Search(userid);

            fragVal = 2;
            fragment = new MenuFragment();

        } else if (id == R.id.nav_logout) {
            //fragment=new CAMenuFragment();
            fragVal = 1;
            logoutConformation();

        } else if (id == R.id.nav_about) {
            fragment = new AboutFragment();

            //  fragVal=1;
        }
        else if (id == R.id.nav_create) {
            fragVal = 1;
            boolean res = HelpingLib.isInternetConnected(MainActivity.context);
            if (res) {
                MenuFragment.handleVisitorInfoInitialSearch();
            } else {
                HelpingLib.showmessage1(MainActivity.context, "ইন্টারনেট সংযোগ নেই");
            }
        }
        else if (id == R.id.nav_exit) {
            fragVal = 1;
            boolean res = HelpingLib.isInternetConnected(MainActivity.context);
            if (res) {
                MenuFragment.handleVisitorExitSearch();
            } else {
                HelpingLib.showmessage1(MainActivity.context, "ইন্টারনেট সংযোগ নেই");
            }
        }
        else if (id == R.id.nav_live) {
            fragment = new LiveFragment();
        } else if (id == R.id.nav_create_return_req) {
            reqType = 1;
            //fragment=new ReturnTripFragment();
        } else if (id == R.id.nav_create_req) {
            reqType = 1;
            fragment = new DefaultFragment();
        } else if (id == R.id.nav_view_req) {
            //fragment=new ViewIndRequisitionListFragment();
        } else if (id == R.id.nav_text_create_bid) {
            //fragment=new BidOnRequisitionFragment();
        } else if (id == R.id.nav_create_con_req) {
            //fragment=new CreateContructRequisitionFragment();
            reqType = 3;
        } else if (id == R.id.nav_create_contruct) {
            //fragment=new CreateContructFragment();
        } else if (id == R.id.nav_view_cont_req) {
            //fragment=new ViewContructFragment();
        } else if (id == R.id.nav_ia_home_el) {
            //fragment=new ELUserMenuFragment();

        } else if (id == R.id.nav_live_el) {
            fragment = new LiveFragment();
        } else if (id == R.id.nav_create_req_el) {
            //fragment=new EndUserReqFragment();
        }
        if (fragVal != 1) {
            redirect(fragment);
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    public void sendDataToServer_Search(final String user_id) {

        try {
            final PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = (int) pInfo.getLongVersionCode(); // avoid huge version numbers and you will be ok
                versionName = pInfo.versionName;
            } else {
                //noinspection deprecation
                versionCode = pInfo.versionCode;
                versionName = pInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        GetUserControlOperation.sendGetInData(MainActivity.this, versionCode + "", user_id, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVchLocData) {

                try {
                    JSONObject jsonObject = getVchLocData;

                    String IsForceLogOut = jsonObject.getString("IsBanned");
                    String IsActive = jsonObject.getString("IsActive");
                    String IsUsableVersion = jsonObject.getString("IsUsableVersion");
                    String LatestVersion = jsonObject.getString("LatestVersion");

                    String vvv = versionCode + "";
                    if (IsForceLogOut.equalsIgnoreCase("true")) {
                        String errmessage = ("আপনাকে লগ আউট করা হয়েছে !!!");
                        HelpingLib.showmessage1(MainActivity.context, errmessage);
                        go_ForceLogOut();
                    } else if (IsUsableVersion.equalsIgnoreCase("false")) {
                        String errmessage = ("আপনাকে লগ আউট করা হয়েছে !!!");
                        HelpingLib.showmessage1(MainActivity.context, errmessage);
                        go_ForceLogOut();
                    } else if (IsActive.equalsIgnoreCase("false")) {
                        String errmessage = ("আপনি সাময়িকভাবে ইন একটিভ হয়েছেন !!!");
                        HelpingLib.showmessage1(MainActivity.context, errmessage);
                        go_inactive();
                    } else {
                        menuList.clear();
                        JSONArray RoleMenuList = jsonObject.getJSONArray("AccessibleModuleList");
                        int lngh = RoleMenuList.length();
                        for (int m = 0; m < lngh; m++) {
                            JSONObject aJsonObject = RoleMenuList.getJSONObject(m);
                            String menuName = aJsonObject.getString("ModuleName");
                            menuList.add(menuName);
                        }
                        String trackmenu = "Tracking";
                        String gatemenu = "Gate In Out";
                        String loadmenu = "Loading Bay Utilization";
                        String visitormenu = "Visitor In Out";
                        track = 0;
                        gate = 0;
                        load = 0;
                        visit = 0;

                        for (int i = 0; i < menuList.size(); i++) {
                            String tName = menuList.get(i);
                            if (tName.equalsIgnoreCase(trackmenu)) {
                                track = 1;
                                break;
                            } else {
                                track = 2;
                            }
                        }
                        for (int i = 0; i < menuList.size(); i++) {
                            String gName = menuList.get(i);
                            if (gName.equalsIgnoreCase(gatemenu)) {
                                gate = 1;
                                break;
                            } else {
                                gate = 2;
                            }
                        }
                        for (int i = 0; i < menuList.size(); i++) {
                            String lName = menuList.get(i);
                            if (lName.equalsIgnoreCase(loadmenu)) {
                                load = 1;
                                break;
                            } else {
                                load = 2;
                            }
                        }
                        for (int i = 0; i < menuList.size(); i++) {
                            String vName = menuList.get(i);
                            if (vName.equalsIgnoreCase(visitormenu)) {
                                visit = 1;
                                break;
                            } else {
                                visit = 2;
                            }
                        }
                        if (vvv.equalsIgnoreCase(LatestVersion)) {
                            //off thake go fragment on thakle
                            go_active();
                        } else {
                            String errmessage = ("আপনার আপস ভারসান টি পুরাতন !!!\n  নতুন ভারসান হলো : " + LatestVersion);
                            HelpingLib.showmessage1(MainActivity.context, errmessage);
                            //off thake go fragment on thakle
                            go_active();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    private void sendDataToServer_Search_home(final String user_id) {
        try {
            final PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                versionCode = (int) pInfo.getLongVersionCode(); // avoid huge version numbers and you will be ok
                versionName = pInfo.versionName;
            } else {
                //noinspection deprecation
                versionCode = pInfo.versionCode;
                versionName = pInfo.versionName;
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        GetUserControlOperation.sendGetInData(MainActivity.this, versionCode + "", user_id, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVchLocData) {
                try {

                    JSONObject jsonObject = getVchLocData;

                    String IsForceLogOut = jsonObject.getString("IsBanned");
                    String IsActive = jsonObject.getString("IsActive");
                    String IsUsableVersion = jsonObject.getString("IsUsableVersion");
                    String LatestVersion = jsonObject.getString("LatestVersion");

                    String vvv = versionCode + "";
                    if (IsForceLogOut.equalsIgnoreCase("true")) {
                        String errmessage = ("আপনাকে লগ আউট করা হয়েছে !!!");
                        HelpingLib.showmessage1(MainActivity.context, errmessage);
                        go_ForceLogOut();
                    } else if (IsUsableVersion.equalsIgnoreCase("false")) {
                        String errmessage = ("আপনাকে লগ আউট করা হয়েছে !!!");
                        HelpingLib.showmessage1(MainActivity.context, errmessage);
                        go_ForceLogOut();
                    } else if (IsActive.equalsIgnoreCase("false")) {
                        String errmessage = ("আপনি সাময়িকভাবে ইন একটিভ হয়েছেন !!!");
                        HelpingLib.showmessage1(MainActivity.context, errmessage);
                        go_inactive();
                    } else {
                        menuList.clear();
                        JSONArray RoleMenuList = jsonObject.getJSONArray("AccessibleModuleList");
                        int lngh = RoleMenuList.length();
                        for (int m = 0; m < lngh; m++) {
                            JSONObject aJsonObject = RoleMenuList.getJSONObject(m);
                            String menuName = aJsonObject.getString("ModuleName");
                            menuList.add(menuName);
                        }
                        String trackmenu = "Tracking";
                        String gatemenu = "Gate In Out";
                        String loadmenu = "Loading Bay Utilization";
                        String visitormenu = "Visitor In Out";
                        track = 0;
                        gate = 0;
                        load = 0;
                        visit = 0;

                        for (int i = 0; i < menuList.size(); i++) {
                            String tName = menuList.get(i);
                            if (tName.equalsIgnoreCase(trackmenu)) {
                                track = 1;
                                break;
                            } else {
                                track = 2;
                            }
                        }
                        for (int i = 0; i < menuList.size(); i++) {
                            String gName = menuList.get(i);
                            if (gName.equalsIgnoreCase(gatemenu)) {
                                gate = 1;
                                break;
                            } else {
                                gate = 2;
                            }
                        }
                        for (int i = 0; i < menuList.size(); i++) {
                            String lName = menuList.get(i);
                            if (lName.equalsIgnoreCase(loadmenu)) {
                                load = 1;
                                break;
                            } else {
                                load = 2;
                            }
                        }
                        for (int i = 0; i < menuList.size(); i++) {
                            String vName = menuList.get(i);
                            if (vName.equalsIgnoreCase(visitormenu)) {
                                visit = 1;
                                break;
                            } else {
                                visit = 2;
                            }
                        }
                        if (vvv.equalsIgnoreCase(LatestVersion)) {
                            go_active();
                        } else {
                            String errmessage = ("আপনার আপস ভারসান টি পুরাতন !!!\n  নতুন ভারসান হলো : " + LatestVersion);
                            HelpingLib.showmessage1(MainActivity.context, errmessage);
                            go_active();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    void go_inactive() {
        Fragment fragment = null;
        fragment = new MenuFragment_InActive();
        redirect(fragment);
    }

    void go_active() {
        fragVal = 2;
        fragment = new MenuFragment();
        redirect(fragment);
    }

    void go_ForceLogOut() {
        fragVal = 1;
        SharedPreferences.Editor aEditor = WellcomeActivity.sharedpreferences.edit();
        aEditor.putString(WellcomeActivity.un, "un not found");
        aEditor.putString(WellcomeActivity.pw, "pw not found");
        aEditor.commit();

        Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(loginIntent);
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_left);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.back, menu);

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
//        sendDataToServer_Search(userid);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.back:
                sendDataToServer_Search_home(userid);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void redirect(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void goTo(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
}
