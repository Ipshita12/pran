package com.prangroup.thirdEyePlus.Default;

import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.prangroup.thirdEyePlus.Fragments.MenuFragment;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.ServerOperation.GetVechileGroupData;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MiddleWearUserSelectActivity extends AppCompatActivity {

    public static Spinner sp_v_type;
    public static Button btn_Next;
    ArrayAdapter dataAdapter;
    SharedPreferences appData;
    public ArrayList<String> V_TYPE_ID = new ArrayList<String>();
    public ArrayList<String> V_TYPE_NAME = new ArrayList<String>();
    public static String v_type_id;
    public static String v_type_name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_middle_wear_user_select);

        appData = PreferenceManager.getDefaultSharedPreferences(this);

        sp_v_type = (Spinner) findViewById(R.id.sp_v_type);
        btn_Next = (Button) findViewById(R.id.btn_Next);

        //  ActivityCompat.requestPermissions(mActivity, new String[]{Manifest.permission.CAMERA}, 1);
        // ActivityCompat.requestPermissions(mActivity, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 2);

        btn_Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent dashboardIntent = new Intent(MiddleWearUserSelectActivity.this, MainActivity.class);
                startActivity(dashboardIntent);
            }
        });
        sendDataToServer_Search();
    }

    private void sendDataToServer_Search() {

        V_TYPE_ID.clear();
        V_TYPE_NAME.clear();

        GetVechileGroupData.sendGetInData(MiddleWearUserSelectActivity.this, new VolleyCallBack() {
            @Override
            public void onSuccess(JSONObject getVchLocData) {
                Log.e("Rs SearchText 1 ", getVchLocData.toString());
                try {
                    String flag = getVchLocData.getString("flag");
                    Fragment fragment = new MenuFragment();

                    Log.e("Rs flag 10 ", flag.toString());

                    if (flag.equalsIgnoreCase("found")) {

                        JSONArray userArray = getVchLocData.getJSONArray("VehicleInManualReasons");
                        Log.e("Rs SearchText list 2 ", userArray.toString());
                        int size = userArray.length();
                        for (int i = 0; i < size; i++) {
                            JSONObject finalJsonData = userArray.getJSONObject(i);
                            String PK_PRG_Type = finalJsonData.getString("PK_PRG_Type");
                            String Title = finalJsonData.getString("Title");

                            V_TYPE_ID.add(PK_PRG_Type);
                            V_TYPE_NAME.add(Title);

                            // Log.e("Rs RegistrationNumber 3", RegistrationNumber1.toString());
                        }

                        SET_Route_Name();

                    } else if (flag.equalsIgnoreCase("not_found")) {


                        String errmessage = ("কোন গাড়ি পাওয়া যায় নি");
                        HelpingLib.showmessage1(getApplicationContext(), errmessage);
                        // redirect(fragment);
                    } else {

                        HelpingLib.showmessage1(getApplicationContext(), "কোন গাড়ি পাওয়া যায় নি");
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                //Fragment fragment=new MenuFragment();
                //redirect(fragment);
            }
        });
    }

    public void SET_Route_Name() {

        dataAdapter = new ArrayAdapter(this, android.R.layout.simple_spinner_item, V_TYPE_NAME);
        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        // attaching data adapter to spinner
        sp_v_type.setAdapter(dataAdapter);
        sp_v_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1, int pos, long arg3) {

                v_type_id = "";
                v_type_name = "";
                v_type_id = V_TYPE_ID.get(pos);
                v_type_name = V_TYPE_NAME.get(pos);

                SharedPreferences.Editor addData = appData.edit();
                addData.putString("v_type_id", v_type_id);
                addData.putString("v_type_name", v_type_name);
                addData.commit();

                Log.e("Rs v_type_id", v_type_id.toString());
                Log.e("Rs v_type_name", v_type_name.toString());
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    @Override
    public void onBackPressed() {
       // super.onBackPressed();
    }
}
