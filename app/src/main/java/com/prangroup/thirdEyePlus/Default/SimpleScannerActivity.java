package com.prangroup.thirdEyePlus.Default;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
//import android.test.mock.MockPackageManager;
import android.test.mock.MockPackageManager;
import android.util.Log;

import com.google.zxing.Result;
import com.prangroup.thirdEyePlus.Fragments.VehINFragment;
import com.prangroup.thirdEyePlus.R;

import java.util.Calendar;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class SimpleScannerActivity extends Activity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;
    String mPermission = Manifest.permission.CAMERA;
    private static final int REQUEST_CODE_PERMISSION = 5;
    Activity mActivity;
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        mActivity=(Activity) this;
        getCameraPermision();
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        Log.e("Rs TAG timeOfDay ff", timeOfDay + "");

        if (timeOfDay >= 18 || timeOfDay < 6) {
            // Toast.makeText(this, "Good Morning night", Toast.LENGTH_SHORT).show();
            mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
            mScannerView.setFlash(true);// flash light on
            setContentView(mScannerView);
        } else if (timeOfDay >= 6 && timeOfDay < 18) {
            // Toast.makeText(this, "Good Afternoon day", Toast.LENGTH_SHORT).show();
            mScannerView = new ZXingScannerView(this);   // Programmatically initialize the scanner view
            mScannerView.setResultHandler(this);
            mScannerView.startCamera();
            // mScannerView.setFlash(true);// flash light on
            setContentView(mScannerView);
        }
       /* mScannerView = new ZXingScannerView(this);// Programmatically initialize the scanner view
        mScannerView.setResultHandler(this);
        mScannerView.startCamera();
      //  mScannerView.setFlash(true);// flash light on
      //  mScannerView = new ZXingScannerView(this);
        setContentView(mScannerView); */               // Set the scanner view as the content view
    }

    private void getCameraPermision() {
        try {
            if (ActivityCompat.checkSelfPermission(getApplicationContext(), mPermission)
                    != MockPackageManager.PERMISSION_GRANTED) {

                ActivityCompat.requestPermissions(mActivity,
                        new String[]{mPermission}, REQUEST_CODE_PERMISSION);

                // If any permission above not allowed by user, this condition will execute every time, else your else part will work
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void handleResult(Result rawResult) {
        // Do something with the result here
        Log.v("TAG", rawResult.getText()); // Prints scan results
        Log.v("TAG", rawResult.getBarcodeFormat().toString()); // Prints the scan format (qrcode, pdf417 etc.)

        VehINFragment.scanresult=rawResult.getText();
        // If you would like to resume scanning, call this method below:
        mScannerView.resumeCameraPreview(this);
        finish();
        Fragment fragment=new VehINFragment();
        redirect(fragment);
    }

    @Override
    public void onResume() {
        super.onResume();
        mScannerView.setResultHandler(this); // Register ourselves as a handler for scan results.
        mScannerView.startCamera();          // Start camera on resume
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();           // Stop camera on pause
    }
    private void redirect(Fragment fragment) {
        FragmentManager fm = MainActivity.fragmentManager;
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(R.id.frameLayout, fragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commitAllowingStateLoss();
    }
}