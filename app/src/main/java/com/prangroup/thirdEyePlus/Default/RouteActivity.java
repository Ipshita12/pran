package com.prangroup.thirdEyePlus.Default;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.transition.TransitionInflater;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.logicbeanzs.carrouteanimation.CarMoveAnim;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.DataHandler.HandleCarMovingData;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserVehLocInfoListJsonData;
import com.prangroup.thirdEyePlus.ServerOperation.SingleVehLocOperation;
import com.prangroup.thirdEyePlus.Utils.DirectionUtils;
import com.prangroup.thirdEyePlus.Utils.MapUtils;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class RouteActivity extends FragmentActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {
    GoogleApiClient mGoogleApiClient;
    public static List<LatLng> polyLineList;
    public String carID,status;
    String pos="";
    ImageButton back;
    LocationRequest mLocationRequest;
    private static final long INTERVAL = 1000 * 5; //1 minute
    private static final long FASTEST_INTERVAL = 1000 * 3;
    private BottomSheetBehavior bottomSheetBehavior;
    private Handler handler;
    private Marker carMarker;
    private int index;
    private int next;
    private LatLng startPosition;
    private LatLng endPosition;
    private static LatLng finalEndPosition;
    GoogleMap googleMap;
    public static List<String> ins;
    public static Context mContext;
    DirectionUtils utils;
    LinearLayout detLin;
    String htmlins = null;
    private ImageView bottomSheetImgView;
    private TextView bottomSheetHeading, bottomSheetAddress, bottomSheetStatus, bottomSheetSpeed, bottomSheetBody, bottomSheetDateTime;
    TextView direct,st_add,end_add,distance,duration;
    public static LatLng oldLatlng,newLatLng;
    public static boolean isForground = false;
    public static boolean isAnimated = false;
    public static boolean isRunning = false;
    public static boolean isFirst=true;
    String PreviousUpdateTime="null";
    Handler mHandler;
    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setStatusBarColor(Color.TRANSPARENT);
            getWindow().setSharedElementEnterTransition(TransitionInflater.from(this)
                    .inflateTransition(R.transition.changebounds));
        }
        setContentView(R.layout.activity_route);
        mContext=(Context) this;

        Bundle bundle = getIntent().getExtras();
        if(bundle!=null)
        {
            if (bundle.containsKey("carID")) {
                carID=bundle.getString("carID");
                Log.e("UPLOAD_URL","found vehID=="+carID);
            }
            if (bundle.containsKey("id")) {
                pos=bundle.getString("id");
            }
            if (bundle.containsKey("status")) {
                status=bundle.getString("status");
            }
            if (bundle.containsKey("oldlatlng")) {
                oldLatlng=bundle.getParcelable("oldlatlng");

                finalEndPosition = oldLatlng;
                Log.e("UPLOAD_URL", "get old lat lng: "+oldLatlng);
            }
//
             if (bundle.containsKey("data")) {
                polyLineList = (ArrayList<LatLng>) getIntent().getSerializableExtra("data");
                Log.d("POLYLINELISSST", "onCreate: "+polyLineList);
            }
            if (bundle.containsKey("inst"))
            {
                ins = bundle.getStringArrayList("inst");
            }
            if (bundle.containsKey("dir"))
            {
                utils = (DirectionUtils)getIntent().getSerializableExtra("dir");
            }
        }

        bottomSheetBehavior = BottomSheetBehavior.from(findViewById(R.id.bottomSheetLayout));
        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);

        bottomSheetImgView = (ImageView) findViewById(R.id.img1);
        bottomSheetHeading = (TextView) findViewById(R.id.tvTitle);
        bottomSheetAddress = (TextView) findViewById(R.id.tvPlace);
        bottomSheetStatus = (TextView) findViewById(R.id.tvStatus);
        bottomSheetSpeed = (TextView) findViewById(R.id.tvSpeed);
        bottomSheetBody = (TextView) findViewById(R.id.tvBodyVal);
        bottomSheetDateTime = (TextView) findViewById(R.id.tvDateTime);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        detLin = findViewById(R.id.detLin);
        direct = findViewById(R.id.direct);
        direct.setSelected(true);
        st_add = findViewById(R.id.st_add);
        st_add.setSelected(true);
        //st_add.setText(utils.getStAddress());
        end_add = findViewById(R.id.end_add);
        end_add.setSelected(true);
        //end_add.setText(utils.getEndAddress());
        distance = findViewById(R.id.distance);
        //distance.setText("Distance: "+utils.getDistance());
        duration = findViewById(R.id.duration);
        //duration.setText("Estimated Time: "+utils.getDuration());
        handler = new Handler();
        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              RouteActivity.super.onBackPressed();
            }
        });



    }
    @Override
    protected void onStart() {
        super.onStart();
        buildGoogleApiClient();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isForground = false;
        isAnimated = false;
        isRunning = false;
        isFirst=true;
    }

    @Override
    protected void onDestroy() {
        //mHandler.removeCallbacks(mRunnable);
        super.onDestroy();
        isForground = false;
        isAnimated = false;
        isRunning = false;
        isFirst=true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isForground = true;
        isAnimated = false;
        isRunning = false;
        isFirst=true;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isForground = true;
        isAnimated = false;
        isRunning = false;
        isFirst=true;
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }
    @Override
    protected void onStop() {
        if (mGoogleApiClient!=null && mGoogleApiClient.isConnected())
        {
            mGoogleApiClient.disconnect();
        }

        super.onStop();
    }
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        createLocationRequest();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        isForground = true;
        this.googleMap = googleMap;
        this.googleMap.getUiSettings().setCompassEnabled(true);
        this.googleMap.getUiSettings().setMapToolbarEnabled(true);
        this.googleMap.getUiSettings().setZoomControlsEnabled(true);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);
        this.googleMap.getUiSettings().setAllGesturesEnabled(true);
        this.googleMap.getUiSettings().setRotateGesturesEnabled(true);
        this.googleMap.getUiSettings().setScrollGesturesEnabled(true);
        this.googleMap.getUiSettings().setTiltGesturesEnabled(true);
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            boolean success = googleMap.setMapStyle(
                    MapStyleOptions.loadRawResourceStyle(
                            this, R.raw.custom_map));
        } catch (Resources.NotFoundException e) {
            Log.e("error", "Can't find style. Error: ", e);
        }
        this.googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                //Log.e("marker", "My marker Item clicked");
                //updateBottomSheetContent();

                return true;
            }
        });

        this.googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });


        polyLineList = new ArrayList<>();
        ins = new ArrayList<>();
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            int locationPermission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (locationPermission == PackageManager.PERMISSION_GRANTED) {

                stopRepeatingTask();
            }
        }

    }

//    private void updateBottomSheetContent() {
//        bottomSheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
//        int id = Integer.valueOf(pos);
//        UserVehIocationListDataModel aCar = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(id);
//
//
//        String[] separatedTitles = null;
//        String[] separatedSnippets = null;
//        String placeName = "";
//        String distanceFrom = "";
//        String place = "";
//        String speed = "";
//        String body = "";
//        String type = "";
//        String strStatus = "";
//        String strTime = "";
//        int drawable = R.drawable.ic_green_128;
//        ;
//
//        try {
//
//            String status = aCar.getStatus();
//
//            if (status.equals("0")) {
//                strStatus = "Egnition Off";
//                drawable = R.drawable.ic_red_128;
//            } else if (status.equals("1")) {
//                strStatus = "Stand By";
//                drawable = R.drawable.ic_yellow_128;
//            } else if (status.equals("2")) {
//                strStatus = "Running";
//                drawable = R.drawable.ic_green_128;
//            } else if (status.equals("-1")) {
//                strStatus = "Time lapsed";
//                drawable = R.drawable.ic_merun_129;
//            } else if (status.equals("-2")) {
//                strStatus = "Disconnected";
//                drawable = R.drawable.ic_merun_129;
//            }
//
//            bottomSheetImgView.setImageResource(drawable);
//            bottomSheetHeading.setText(aCar.getRegistrationNumber());
//            bottomSheetAddress.setText(aCar.getNearestMapLocationDistance() + " km from " + aCar.getNearestMapLocation());
//            bottomSheetStatus.setText(strStatus);
//            bottomSheetSpeed.setText("Speed: " + aCar.getSpeed() + " km");
//            bottomSheetBody.setText(aCar.getInternal_VehicleContactNumber());
//            bottomSheetDateTime.setText(aCar.getUpdateTime());
//
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//    }


    private void startCarAnimation(Double latitude, Double longitude) {
        LatLng latLng = new LatLng(latitude, longitude);
        if (carMarker!=null)
        {
            carMarker.remove();
        }
        index = -1;
        next = 1;
        carMarker = googleMap.addMarker(new MarkerOptions().position(latLng).
                flat(true).icon(BitmapDescriptorFactory.fromResource(R.mipmap.truckgreen)));

        MapUtils.createBottomUpAnimation(this, detLin, new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                detLin.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        handler.postDelayed(pol, 600);
    }
    Runnable pol = new Runnable() {
        @SuppressLint("SetTextI18n")
        @Override
        public void run() {

            isRunning = true;
            if (index < (polyLineList.size() - 1)) {
                index++;
                next = index + 1;

            } else {
                index = -1;
                next = 1;
                //direct.setText("Destination Reached.");
                stopRepeatingTask();
                return;
            }

            if (index < (polyLineList.size()-1)) {
                startPosition = carMarker.getPosition();
                endPosition = polyLineList.get(next);
                finalEndPosition = endPosition;
            }
            if (htmlins!=null) {
                if (!htmlins.equalsIgnoreCase(ins.get(index)))
                direct.setText(htmlins);
            }
            else
            {
                direct.setText(ins.get(index));
            }
            Log.e("UPLOAD_URL",""+startPosition+"/"+endPosition);

            CarMoveAnim.startcarAnimation(carMarker,googleMap, startPosition,endPosition,6000,new GoogleMap.CancelableCallback() {
                @Override
                public void onFinish() {

                    handler.postDelayed(pol,600);
                }

                @Override
                public void onCancel() {
//                    handler.postDelayed(pol,600);
                }
            });
            htmlins = ins.get(index);
        }
    };

    private void stopRepeatingTask() {
        if (pol != null) {
            handler.removeCallbacks(pol);
        }

        if(polyLineList != null){

            polyLineList.clear();
            ins.clear();
        }
        isRunning = false;

        PreviousUpdateTime=PreviousUpdateTime.replaceAll("\\s+","");
        SingleVehLocOperation.getVehLocInstantData((Activity) mContext,carID,PreviousUpdateTime, new VolleyCallBack() {

            @Override
            public void onSuccess(JSONObject getVchLocData) {
                // this is where you will call the geofire, here you have the response from the volley.

                Log.d("Complettttttt", "onSuccess: Complettttttt");
                LatLng newLatLng = null;
                LatLng oldLatLng = oldLatlng;
                if(finalEndPosition != null)
                    oldLatLng = finalEndPosition;
                try {
                    PreviousUpdateTime=getVchLocData.getString("UpdateTime");
                    newLatLng = new LatLng(Double.valueOf(getVchLocData.getString("Latitude")),Double.valueOf(getVchLocData.getString("Longitude")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                HandleCarMovingData aHandleCarMovingData=new HandleCarMovingData();
                List<List<HashMap<String, String>>> result = aHandleCarMovingData.handleData(
                        oldLatLng,newLatLng,mContext);
                Log.e("UPLOAD_URL", "run: CurrentLatLAn: "+oldLatLng +","+newLatLng);

                if (result!=null && result.size()>0) {
                    polyLineList = new ArrayList<>();
                    ins = new ArrayList<>();
                    for (int i = 0; i < result.size(); i++) {
                        List<HashMap<String, String>> path = result.get(i);
                        for (int j = 0; j < path.size(); j++) {
                            HashMap<String, String> point = path.get(j);
                            double lat = Double.parseDouble(point.get("lat"));
                            double lng = Double.parseDouble(point.get("lng"));
                            LatLng position = new LatLng(lat, lng);
                            polyLineList.add(position);
                            ins.add(point.get("instructions"));
                        }
                    }

                }
                if (polyLineList!=null && polyLineList.size()>1)
                {
                    Log.e("reach",""+polyLineList.get(0).latitude);
                    startCarAnimation(polyLineList.get(0).latitude,polyLineList.get(0).longitude);
                }else {
                    googleMap.clear();

                    BitmapDescriptor aIcon= HandleUserVehLocInfoListJsonData.getCarIcon(status);
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(oldLatlng);
                    markerOptions.icon(aIcon);
                    googleMap.addMarker(markerOptions);
                    LatLng curLatLng=new LatLng(polyLineList.get(0).latitude,polyLineList.get(0).longitude);
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(curLatLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(16), 2000, null);
                    detLin.setVisibility(View.INVISIBLE);
                    //stopRepeatingTask();
                }

            }
        });


    }

    @Override
    public void onBackPressed() {

        if (pol != null) {
            polyLineList.clear();
            handler.removeCallbacks(pol);
        }
        finish();
    }
}
