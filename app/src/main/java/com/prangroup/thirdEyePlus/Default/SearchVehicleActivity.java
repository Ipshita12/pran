package com.prangroup.thirdEyePlus.Default;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.RelativeLayout;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prangroup.thirdEyePlus.Adapter.AutoCompleteAdapter;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserVehLocInfoListJsonData;
import com.prangroup.thirdEyePlus.ServerOperation.SingleVehLocOperation;
import com.prangroup.thirdEyePlus.Utilities.ConnectionDetector;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import butterknife.ButterKnife;


public class SearchVehicleActivity extends AppCompatActivity implements OnMapReadyCallback, TextWatcher {

    public static SearchVehicleActivity activity;
    public static Context context;
    private ConnectionDetector connectionDetector;

    private static GoogleMap mMap;
    private View mapView;

    private Handler mHandler = new Handler();
    public static boolean isForground = false;
    public static boolean isAnimated = false;
    public static boolean isRunning = true;
    private static boolean mapState = false;

    public static LatLng oldLatLng = null;
    public static float crntDeg = 0;
    public static float currentZoom = 16;
    public double lat, lng;
    private LatLng newLatLng;

    private static Marker startMarker;

    private String vehicleId = "";
    AutoCompleteTextView inputSearchBox;
    AutoCompleteAdapter adapter;
    int x = 0;
    public static int poss = 0;
    String[] from;
    int[] to;
    public static String vehID = "";
    public static String vehLocUpTime = "";
    public static Activity mActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_vehicle);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // Toolbar :: Transparent
        toolbar.setBackgroundColor(Color.TRANSPARENT);

        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        // Status bar :: Transparent
        Window window = this.getWindow();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.addFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(Color.TRANSPARENT);
        }

        init();
        isForground = true;
        final Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() { // Function runs every MINUTES minutes.
                // Run the code you want here
                Log.e("LocationTime","Timer Called time== "+new Date(System.currentTimeMillis()));
//                Date currentDate = new Date(System.currentTimeMillis());
//                Date dataGetDate = new SimpleDateFormat("MM/DD/yy").parse(sDate1);  //02.01.19 06:46
//                long seconds = (currentDate.getTime()-dataGetDate.getTime())/1000;
                try{
                    if(isRunning)
                        Thread.sleep(100);
                    else
                        Thread.sleep(8000);

                    if (SearchVehicleActivity.isForground && !isAnimated ){
                        Log.e("LocationTime","Method Called Data time== "+new Date(System.currentTimeMillis()));
                        SingleVehLocOperation.getVehLocData(SearchVehicleActivity.activity,SearchVehicleActivity.vehID,SearchVehicleActivity.poss);
                    }
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }


            }
        }, 0,  1000);




    }

    public void init() {
        activity = SearchVehicleActivity.this;
        context = (Context) this;
        ButterKnife.bind(this);

        connectionDetector = new ConnectionDetector(activity);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        mapView = mapFragment.getView();

        inputSearchBox = (AutoCompleteTextView) findViewById(R.id.inputSearchBox);

        from = new String[]{"vehicle_name"};

        // Ids of views in listview_layout
        //to = new int[]{R.id.vehicle_name};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>
                (this, android.R.layout.select_dialog_item, HandleUserVehLocInfoListJsonData.userVehNameDataDB);
        //adapter=new SimpleAdapter(activity, vehiclesList,R.layout.search_vehicle_row,from,to);
        //adapter = new AutoCompleteAdapter(getApplicationContext(),HandleUserVehLocInfoListJsonData.userVehNameDataDB);
        AdapterView.OnItemClickListener itemClickListener = new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long id) {
//                getWindow().setSoftInputMode(
//                        WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(inputSearchBox.getWindowToken(), 0);
                String vehName = inputSearchBox.getText().toString();

                for (int i = 0; i < HandleUserVehLocInfoListJsonData.userVehLocListDataDB.size(); i++) {

                    String vehNameDB = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getRegistrationNumber();
                    if (vehNameDB.equalsIgnoreCase(vehName)) {
                        poss = i;
                        vehID = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getPK_Vehicle();
                        vehLocUpTime = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getUpdateTime();
                        lat = Double.parseDouble(HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getLatitude());
                        lng = Double.parseDouble(HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(i).getLongitude());
                        i = i + HandleUserVehLocInfoListJsonData.userVehLocListDataDB.size();
                    }

                }

                isAnimated = false;
                isRunning = true;
                mapState = false;
                oldLatLng = new LatLng(lat, lng);
                SingleVehLocOperation.getVehLocData(activity, vehID, poss);

                //Restart the map
                mMap.clear();
            }
        };
        inputSearchBox.setOnItemClickListener(itemClickListener);
        inputSearchBox.setAdapter(adapter);

        //Clear the field
        inputSearchBox.setOnTouchListener(new View.OnTouchListener() {
            final int DRAWABLE_LEFT = 0;
            final int DRAWABLE_TOP = 1;
            final int DRAWABLE_RIGHT = 2;
            final int DRAWABLE_BOTTOM = 3;

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_UP) {
                    int leftEdgeOfRightDrawable = inputSearchBox.getRight()
                            - inputSearchBox.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width();
                    if (event.getRawX() >= leftEdgeOfRightDrawable) {
                        // clicked on clear icon
                        inputSearchBox.setText("");
                        return true;
                    }
                }
                return false;
            }
        });

        //Updating the vehicle position now
        isForground = true;

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMinZoomPreference(8.0f);
        mMap.setMaxZoomPreference(20.0f);
        mMap.setTrafficEnabled(true);
        if (mapView != null &&
                mapView.findViewById(Integer.parseInt("1")) != null) {
            // Get the button view
            View locationButton = ((View) mapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));
            // and next place it, on bottom right (as Google Maps app)
            RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                    locationButton.getLayoutParams();
            // position on right bottom
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
            layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
            layoutParams.setMargins(0, 0, 30, 200);
        }
        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                CameraPosition pos = mMap.getCameraPosition();
                if (pos.zoom != currentZoom){
                    currentZoom = pos.zoom;
                    // do you action here
                }
            }
        });

    }

    public static void placeStartMarker(final LatLng newLatLng, String dateTime, MarkerOptions markerOptions, String course) {
        if (oldLatLng == null) {
            oldLatLng = newLatLng;
        }


        if (!mapState) {
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(oldLatLng, currentZoom));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoom), 2000, null);
            mapState = true;
        }

        animateMarker(oldLatLng, newLatLng, false, course, markerOptions, dateTime);

    }


    @Override
    protected void onPause() {
        super.onPause();
        isForground = false;
        isAnimated = false;
        isRunning = true;
        mapState = false;
    }

    @Override
    protected void onDestroy() {
        //mHandler.removeCallbacks(mRunnable);
        super.onDestroy();
        isForground = false;
        isAnimated = false;
        isRunning = true;
        mapState = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isForground = true;
        isAnimated = false;
        isRunning = true;
        mapState = false;
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        isForground = true;
        isAnimated = false;
        isRunning = true;
        mapState = false;
    }


    public static void animateMarker(LatLng oldLatLngP, final LatLng newLatLng,
                                     final boolean hideMarker, final String course, MarkerOptions markerOptions, String dateTime) {
        Log.e("LocationOnMarker= ", "old=" + oldLatLngP + " =new ==" + newLatLng + "preTime=" + vehLocUpTime + "newTime=" + dateTime);
        vehLocUpTime = dateTime;

        isAnimated = true;
        //clear map
        mMap.clear();
        markerOptions.position(oldLatLng);

        //oldLatLng = newLatLng;
        if (String.valueOf(oldLatLng).equalsIgnoreCase(String.valueOf(newLatLng))) {

            if(crntDeg != 0)
                markerOptions.flat(true).rotation(crntDeg);
            mMap.addMarker(markerOptions);

            // Zoom in, animating the camera.
            if (!mapState) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(oldLatLng, currentZoom));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(currentZoom), 2000, null);
                mapState = true;
            }

            isAnimated = false;
            isRunning = false;
        } else {
            crntDeg = getBearing(oldLatLng, newLatLng);
            markerOptions.flat(true).rotation(crntDeg);
            final Marker marker = mMap.addMarker(markerOptions);
            final Handler handler = new Handler();
            final long start = SystemClock.uptimeMillis();

            Projection proj = mMap.getProjection();
            Point startPoint = proj.toScreenLocation(marker.getPosition());
            final LatLng startLatLng = proj.fromScreenLocation(startPoint);

            final long duration = 30 * 1000;//30000 second

            final Interpolator interpolator = new LinearInterpolator();

            handler.post(new Runnable() {
                @Override
                public void run() {
                    try {
                        long elapsed = SystemClock.uptimeMillis() - start;
                        float t = interpolator.getInterpolation((float) elapsed
                                / duration);
                        if (newLatLng != null && startLatLng != null) {
                            double lng = t * newLatLng.longitude + (1 - t)
                                    * startLatLng.longitude;
                            double lat = t * newLatLng.latitude + (1 - t)
                                    * startLatLng.latitude;
                            marker.setPosition(new LatLng(lat, lng));

                            if (t < 1.0) {
                                // Post again 16ms later.
                                handler.postDelayed(this, 1);
                            } else {
                                isAnimated = false;
                                isRunning = true;
                                if (hideMarker) {
                                    //marker.setVisible(false);
                                } else {
                                    //marker.setVisible(true);
                                }
                            }
                        } else {
                            //Null value
                            //GLog.logMsg("NUll value received:",""+toPosition.toString()+startLatLng.toString());
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
            });
        }

        //Log.e("LocationChgbefore",""+oldLatLng+""+newLatLng);
//        if(oldLatLng != newLatLng)
        oldLatLng = newLatLng;
        //Log.e("LocationChgAfter",""+oldLatLng+""+newLatLng);


    }

    @Override
    public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

    }

    @Override
    public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
       /* if (x==0){
           x++;
        }else{
            Toast.makeText(getApplicationContext(),charSequence,Toast.LENGTH_SHORT).show();
            vehiclesListCopy=vehiclesList;
            vehiclesList.clear();
            HashMap<String, String> hm = new HashMap<String,String>();
        for (int m=0;m< vehiclesList.size();m++){
           String name= DashboardActivity.vehiclesListCopy.get(m).get("vehicle_name");
            int val=name.indexOf(String.valueOf(charSequence));
            if (val>=0){
                hm=DashboardActivity.vehiclesListCopy.get(i);
                vehiclesList.add(i,hm);
            }
        }
            adapter=new SimpleAdapter(activity, vehiclesList,R.layout.search_vehicle_row,from,to);
        inputSearchBox.setAdapter(adapter);
        //adapter.notifyDataSetChanged();
        }*/


    }

    @Override
    public void afterTextChanged(Editable editable) {

    }

    private static float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
}