package com.prangroup.thirdEyePlus.DataHandler;

import android.content.SharedPreferences;
import android.util.Log;


import com.google.android.gms.maps.model.BitmapDescriptor;
import com.prangroup.thirdEyePlus.Auth.WellcomeActivity;
import com.prangroup.thirdEyePlus.Model.LoggedUserDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class HandleUserJsonData {
    public static double lat, lng;
    BitmapDescriptor carIcon;
    public static ArrayList<LoggedUserDataModel> loogedUserDataModelDB = new ArrayList<LoggedUserDataModel>();
    public static ArrayList<String> menuList = new ArrayList<String>();

    public static void loggedUserData(JSONObject loogedUserRootJsonObj, String staffid, String password) {
        //String FK_Location;
        try {
            JSONObject aLoogedUserJsonObj = loogedUserRootJsonObj.getJSONObject("data");
            menuList.clear();
            loogedUserDataModelDB.clear();
            String PK_User = aLoogedUserJsonObj.getString("PK_User");
            String FullName = aLoogedUserJsonObj.getString("FullName");
            String UniqueIDNumber = aLoogedUserJsonObj.getString("UniqueIDNumber");
            String ContactNumber = aLoogedUserJsonObj.getString("ContactNumber");
            String Email = aLoogedUserJsonObj.getString("Email");
            String FK_Location = aLoogedUserJsonObj.getString("FK_Location");

            LoggedUserDataModel aUserInfoObj = new LoggedUserDataModel(PK_User, FullName, UniqueIDNumber, ContactNumber, Email);
            loogedUserDataModelDB.add(aUserInfoObj);

            JSONArray RoleMenuList = aLoogedUserJsonObj.getJSONArray("RoleMenuList");
            int lngh = RoleMenuList.length();
            for (int m = 0; m < lngh; m++) {
                JSONObject aJsonObject = RoleMenuList.getJSONObject(m);
                String menuName = aJsonObject.getString("FullName");
                menuList.add(menuName);
            }

            SharedPreferences.Editor aEditor = WellcomeActivity.sharedpreferences.edit();
            aEditor.putString(WellcomeActivity.un, staffid);
            aEditor.putString(WellcomeActivity.pw, password);
            aEditor.putString(WellcomeActivity.location, FK_Location);
            aEditor.putString(WellcomeActivity.user_id, PK_User);
            aEditor.putString(WellcomeActivity.user_data, aLoogedUserJsonObj.toString());
            aEditor.commit();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
