package com.prangroup.thirdEyePlus.DataHandler;

import com.prangroup.thirdEyePlus.Fragments.VehINFragment;
import com.prangroup.thirdEyePlus.Fragments.VehINManualFragment;
import com.prangroup.thirdEyePlus.Model.VehINReassonDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HandleINReasonJsonData {


    public static void data(JSONObject loogedUserRootJsonObj) {

        try {
            JSONArray aLoogedUserJsonArray = loogedUserRootJsonObj.getJSONArray("VehicleInManualReasons");
            int lng = aLoogedUserJsonArray.length();
            VehINFragment.vehInReassonDB.clear();
            VehINFragment.vehInReassonNameDB.clear();
            VehINReassonDataModel aUserInfoObj1 = new VehINReassonDataModel("2", "সিলেক্ট করুন");
            VehINFragment.vehInReassonDB.add(aUserInfoObj1);
            VehINFragment.vehInReassonNameDB.add("সিলেক্ট করুন");
            // for (int i=0;i<lng;i++){
            for (int i = 0; i < aLoogedUserJsonArray.length(); i++) {
                JSONObject aJsonObject = aLoogedUserJsonArray.getJSONObject(i);

                String PK_VehicleInOutManualReason = aJsonObject.getString("PK_VehicleInOutManualReason");
                String TitleBangla = aJsonObject.getString("TitleBangla");

                VehINReassonDataModel aUserInfoObj = new VehINReassonDataModel(PK_VehicleInOutManualReason, TitleBangla);
                VehINFragment.vehInReassonDB.add(aUserInfoObj);
                VehINFragment.vehInReassonNameDB.add(TitleBangla);
            }
            JSONArray jsonArray = loogedUserRootJsonObj.getJSONArray("LoadUnloadStatus");
            int size = jsonArray.length();
            VehINFragment.vehInloadingDB.clear();
            VehINFragment.vehInloadingDB.add("সিলেক্ট করুন");
            for (int m = 0; m < size; m++) {
                JSONObject aJsonObject = jsonArray.getJSONObject(m);
                String Title = aJsonObject.getString("Title");
                VehINFragment.vehInloadingDB.add(Title);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
