package com.prangroup.thirdEyePlus.DataHandler;

import com.prangroup.thirdEyePlus.Fragments.VehExitManFragment;
import com.prangroup.thirdEyePlus.Fragments.VehINFragment;
import com.prangroup.thirdEyePlus.Model.VehINReassonDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HandleExitReasonManJsonData {


    public static void data(JSONObject loogedUserRootJsonObj) {

        try {
            JSONArray aLoogedUserJsonArray = loogedUserRootJsonObj.getJSONArray("VehicleOutManualReasons");
            int lng = aLoogedUserJsonArray.length();
            VehExitManFragment.vehInReassonDB.clear();
            VehExitManFragment.vehInReassonNameDB.clear();
            VehINReassonDataModel aUserInfoObj1 = new VehINReassonDataModel("2", "সিলেক্ট করুন");
            VehExitManFragment.vehInReassonDB.add(aUserInfoObj1);
            VehExitManFragment.vehInReassonNameDB.add("সিলেক্ট করুন");
            //  for (int i=0;i<lng;i++){
            for (int i = 0; i < aLoogedUserJsonArray.length(); i++) {
                JSONObject aJsonObject = aLoogedUserJsonArray.getJSONObject(i);

                String PK_VehicleInOutManualReason = aJsonObject.getString("PK_VehicleInOutManualReason");
                String TitleBangla = aJsonObject.getString("TitleBangla");

                VehINReassonDataModel aUserInfoObj = new VehINReassonDataModel(PK_VehicleInOutManualReason, TitleBangla);
                VehExitManFragment.vehInReassonDB.add(aUserInfoObj);
                VehExitManFragment.vehInReassonNameDB.add(TitleBangla);
            }
            JSONArray jsonArray = loogedUserRootJsonObj.getJSONArray("LoadUnloadStatus");
            int size = jsonArray.length();
            VehExitManFragment.vehInloadingDB.clear();
            VehExitManFragment.vehInloadingDB.add("সিলেক্ট করুন");
            for (int m = 0; m < size; m++) {
                JSONObject aJsonObject = jsonArray.getJSONObject(m);
                String Title = aJsonObject.getString("Title");
                VehExitManFragment.vehInloadingDB.add(Title);
            }

            JSONArray ajsonArray = loogedUserRootJsonObj.getJSONArray("InsideVehicleList");
            int asize = ajsonArray.length();
            VehExitManFragment.vehListDB.clear();
            for (int m = 0; m < asize; m++) {
                JSONObject aJsonObject = ajsonArray.getJSONObject(m);
                String RegistrationNumber = aJsonObject.getString("RegistrationNumber");
                VehExitManFragment.vehListDB.add(RegistrationNumber);

//                String lastFourDigits = RegistrationNumber.substring(RegistrationNumber.length() - 4);
//                VehExitManFragment.vehListDB.add(RegistrationNumber);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
