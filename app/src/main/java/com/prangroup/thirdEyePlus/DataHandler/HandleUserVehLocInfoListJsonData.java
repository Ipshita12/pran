package com.prangroup.thirdEyePlus.DataHandler;

import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.prangroup.thirdEyePlus.Adapter.MyCustomAdapterForItems;
import com.prangroup.thirdEyePlus.Adapter.VehSearchAdapter;
import com.prangroup.thirdEyePlus.Fragments.LiveFragment;
import com.prangroup.thirdEyePlus.Model.UserVehIocationListDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.Utilities.MyItem;
import com.prangroup.thirdEyePlus.Utilities.MyTextWatcherSearchVeh;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;

public class HandleUserVehLocInfoListJsonData {

    public static ArrayList<UserVehIocationListDataModel> userVehLocListDataDB=new ArrayList<UserVehIocationListDataModel>();
    public static ArrayList<String> userVehNameDataDB=new ArrayList<String>();

    public static void vehLocData(JSONArray depoRootJsonArry){

        userVehLocListDataDB.clear();
        userVehNameDataDB.clear();
        LiveFragment.mClusterManager.clearItems();

        try {
            for (int i=0;i<depoRootJsonArry.length();i++){
                JSONObject aLoogedUserJsonObj = depoRootJsonArry.getJSONObject(i);

                String PK_Vehicle = aLoogedUserJsonObj.getString("PK_Vehicle");
                String nearestMapLocation = aLoogedUserJsonObj.getString("NearestMapLocation");
                String nearestMapLocationDistance = aLoogedUserJsonObj.getString("NearestMapLocationDistance");
                String latitude = aLoogedUserJsonObj.getString("Latitude");

                String longitude = aLoogedUserJsonObj.getString("Longitude");
                String speed = aLoogedUserJsonObj.getString("Speed");
                String updateTime = aLoogedUserJsonObj.getString("UpdateTime");
                String registrationNumber = aLoogedUserJsonObj.getString("RegistrationNumber");

                String internal_VehicleContactNumber = aLoogedUserJsonObj.getString("Internal_VehicleContactNumber");
                String depoName = aLoogedUserJsonObj.getString("DepoName");
                String status = aLoogedUserJsonObj.getString("Status");
                BitmapDescriptor carIcon=getCarIcon(status);

                UserVehIocationListDataModel aUserVehLocObj = new UserVehIocationListDataModel(i,PK_Vehicle,carIcon,nearestMapLocation,nearestMapLocationDistance,latitude,longitude,speed,updateTime,registrationNumber,internal_VehicleContactNumber,depoName,status);
                userVehLocListDataDB.add(aUserVehLocObj);
                userVehNameDataDB.add(registrationNumber);

                if (latitude.equalsIgnoreCase("") || longitude.equalsIgnoreCase("") ){

                }else{
                    MyItem offsetItem = new MyItem(Double.parseDouble(latitude),Double.parseDouble(longitude),i);
                    LiveFragment.mClusterManager.addItem(offsetItem);

                }

            }
            Log.e("liveTrack","Api hited end  time== "+new Date(System.currentTimeMillis()));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.e("liveTrack","start marker load  time== "+new Date(System.currentTimeMillis()));
        LiveFragment.mClusterManager.getMarkerCollection().setOnInfoWindowAdapter(new MyCustomAdapterForItems(LiveFragment.mActivity));
                if (LiveFragment.pdVal==1) {
            LiveFragment.pdVal=2;
            LiveFragment.mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(HelpingLib.curloc.latitude,HelpingLib.curloc.longitude), LiveFragment.DEFAULT_ZOOM_LEVEL));
        }
        LiveFragment.lAdapter = new VehSearchAdapter(LiveFragment.mContext, userVehNameDataDB);
        LiveFragment.inputSearchBox.setAdapter(LiveFragment.lAdapter);
        LiveFragment.inputSearchBox.addTextChangedListener(new MyTextWatcherSearchVeh(LiveFragment.lAdapter));
        LiveFragment.inputSearchBox.setThreshold(0);


        Log.e("liveTrack","end marker load  time== "+new Date(System.currentTimeMillis()));
        if (LiveFragment.pdVal==1) {
            //UserVehListOperation.loading.dismiss();
        }


    }

    public static BitmapDescriptor getCarIcon(String status){
        BitmapDescriptor aIcon=null;
        if (status.equalsIgnoreCase("0")){
            aIcon=BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( LiveFragment.mContext.getResources(),
                            R.mipmap.trucklightred));
        }else if (status.equalsIgnoreCase("1")){
            aIcon=BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( LiveFragment.mContext.getResources(),
                            R.mipmap.truckyellow));
        }
        else if (status.equalsIgnoreCase("2")){
                    aIcon=BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( LiveFragment.mContext.getResources(),
                            R.mipmap.truckgreen));
        }else if (status.equalsIgnoreCase("-1")){
            aIcon=BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( LiveFragment.mContext.getResources(),
                            R.mipmap.icondarkred));
        }else if (status.equalsIgnoreCase("-2")){
            aIcon=BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( LiveFragment.mContext.getResources(),
                            R.mipmap.icondarkred));
        }
        return aIcon;
    }


}
