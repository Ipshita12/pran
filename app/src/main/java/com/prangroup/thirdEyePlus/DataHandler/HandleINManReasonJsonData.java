package com.prangroup.thirdEyePlus.DataHandler;

import com.prangroup.thirdEyePlus.Fragments.VehINManualFragment;
import com.prangroup.thirdEyePlus.Model.VehINReassonDataModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class HandleINManReasonJsonData {


    public static void data(JSONObject loogedUserRootJsonObj) {

        try {
            JSONArray aLoogedUserJsonArray = loogedUserRootJsonObj.getJSONArray("VehicleInManualReasons");
            int lng = aLoogedUserJsonArray.length();
            VehINManualFragment.vehInReassonDB.clear();
            VehINManualFragment.vehInReassonNameDB.clear();
            VehINReassonDataModel aUserInfoObj1 = new VehINReassonDataModel("2", "সিলেক্ট করুন");
            VehINManualFragment.vehInReassonDB.add(aUserInfoObj1);
            VehINManualFragment.vehInReassonNameDB.add("সিলেক্ট করুন");
            // for (int i=0;i<lng;i++){
            for (int i = 0; i < aLoogedUserJsonArray.length() ; i++) {
                JSONObject aJsonObject = aLoogedUserJsonArray.getJSONObject(i);

                String PK_VehicleInOutManualReason = aJsonObject.getString("PK_VehicleInOutManualReason");
                String TitleBangla = aJsonObject.getString("TitleBangla");

                VehINReassonDataModel aUserInfoObj = new VehINReassonDataModel(PK_VehicleInOutManualReason, TitleBangla);
                VehINManualFragment.vehInReassonDB.add(aUserInfoObj);
                VehINManualFragment.vehInReassonNameDB.add(TitleBangla);
            }
            JSONArray jsonArray = loogedUserRootJsonObj.getJSONArray("LoadUnloadStatus");
            int size = jsonArray.length();
            VehINManualFragment.vehInloadingDB.clear();
            VehINManualFragment.vehInloadingDB.add("সিলেক্ট করুন");
            for (int m = 0; m < size; m++) {
                JSONObject aJsonObject = jsonArray.getJSONObject(m);
                String Title = aJsonObject.getString("Title");
                VehINManualFragment.vehInloadingDB.add(Title);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


}
