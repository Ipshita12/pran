package com.prangroup.thirdEyePlus.DataHandler;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.android.gms.maps.model.LatLng;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utils.DirectionsJSONParser;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class HandleCarMovingData {
    List<LatLng> polyLineList;
    List<String> instruct;
    static List<List<HashMap<String, String>>> parserResult ;
    public List<List<HashMap<String, String>>> handleData(LatLng oldLatLng, LatLng newLatLng, Context context){
        String url = getDirectionsUrl(oldLatLng,newLatLng, context);
//        DownloadTask downloadTask = new DownloadTask();
//        downloadTask.execute(url);

        ParserTask parserTask = new ParserTask();
//        Log.d("Resssssssss11", "handleData: "+downloadTask.execute(url));
        try {
            List<List<HashMap<String, String>>> directionsData = parserTask.execute(url).get();
            Log.d("Resssssssss11", "handleData: "+directionsData);
            return directionsData;
        } catch (InterruptedException e) {
            Log.d("Resssssssss11", "handleData: "+e.getMessage());
            e.printStackTrace();
        } catch (ExecutionException e) {
            Log.d("Resssssssss11", "handleData: "+e.getMessage());
            e.printStackTrace();
        }

        return null;
    }

    private static String getDirectionsUrl(LatLng origin, LatLng dest,Context context) {
        String str_origin = "origin="+origin.latitude+","+origin.longitude;
        String str_dest = "destination="+dest.latitude+","+dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin+"&"+str_dest+"&key="+context.getString(R.string.google_maps_key);
        String output = "json";
        String url = "https://maps.googleapis.com/maps/api/directions/"+output+"?"+parameters;
        Log.e("Route UrL", url);
        return url;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        // Downloading data in non-ui thread
        @Override
//        protected String doInBackground(String... url) {
        protected  String doInBackground(String... url) {
            // For storing data from web service
            String data = "";
            try{
                // Fetching the data from web service
                data = downloadUrl(url[0]);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            // Invokes the thread for parsing the JSON data

            try {
                parserResult =  parserTask.execute(result).get();
                Log.d("Resssssssss", "handleData: "+parserResult);
            } catch (InterruptedException e) {
                Log.d("Resssssssss", "handleData: "+e.getMessage());
                e.printStackTrace();
            } catch (ExecutionException e) {
                Log.d("Resssssssss", "handleData: "+e.getMessage());
                e.printStackTrace();
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try{
            URL url = new URL(strUrl);
            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();
            // Connecting to url
            urlConnection.connect();
            // Reading data from url
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuilder sb  = new StringBuilder();
            String line = "";
            while( ( line = br.readLine())  != null){
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        }catch(Exception e){
            Log.d("Exceptiondownloading", e.toString());
        }finally{
            if (iStream != null) {
                iStream.close();
            }
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return data;
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String,String>>> > {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try{
                String data = "";
                try{
                    // Fetching the data from web service
                    data = downloadUrl(jsonData[0]);
                }catch(Exception e){
                    Log.d("Background Task",e.toString());
                }
                jObject = new JSONObject(data);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                Log.e("JsonObject", jObject.toString());
                routes = parser.parse(jObject);

            }catch(Exception e){
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            Log.d("onPostExecuteRsut", "onPostExecuteRsut: "+result);

            if (result!=null && result.size()>0) {
                polyLineList = new ArrayList<>();
                instruct = new ArrayList<>();
                for (int i = 0; i < result.size(); i++) {
                    List<HashMap<String, String>> path = result.get(i);
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        polyLineList.add(position);
                        instruct.add(point.get("instructions"));
                    }
                }
                //staticPolyLine();
            }

        }
    }
//
//    public List<LatLng> getPolyLineList (){
//        if(!polyLineList.isEmpty()){
//            return polyLineList;
//        }
//        return null;
//    }
//    public List<String> getInstruct (){
//        if(!polyLineList.isEmpty()){
//            return instruct;
//        }
//        return null;
//    }

//    void staticPolyLine() {
//        googleMap.clear();
//        Log.e("Size", String.valueOf(polyLineList.get(0)));
//        final int MAP_BOUND_PADDING = 180;
//        LatLngBounds.Builder builder = new LatLngBounds.Builder();
//        for (LatLng latLng:polyLineList)
//        {
//            builder.include(latLng);
//        }
//        final LatLngBounds bounds = builder.build();
////        googleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
////            @Override
////            public void onMapLoaded() {
//        CameraUpdate mCameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, MAP_BOUND_PADDING);
//        googleMap.animateCamera(mCameraUpdate);
////            }
////        });
//
//        MapAnimator.getInstance().animateRoute(googleMap, polyLineList);
//        fab.show();
//        startCarAnimation(polyLineList.get(0).latitude,polyLineList.get(0).longitude);
//    }
}
