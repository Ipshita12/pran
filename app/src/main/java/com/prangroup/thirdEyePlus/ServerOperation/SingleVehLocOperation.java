package com.prangroup.thirdEyePlus.ServerOperation;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.prangroup.thirdEyePlus.DataHandler.HandleUserVehLocInfoListJsonData;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Default.RouteActivity;
import com.prangroup.thirdEyePlus.Model.UserVehIocationListDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class SingleVehLocOperation {
    public static String Course,updateTime;
    private  static String UPLOAD_URL ;
    static StringRequest stringRequest;
    static String message;
    static Intent loginsuc;
    static int MY_SOCKET_TIMEOUT_MS=30000;
    static JSONObject rootObj=new JSONObject();


    public static JSONObject getVehLocInstantData(final Activity activity, final String vehid,final String PreviousUpdateTime, final VolleyCallBack callBack)  {

        UPLOAD_URL=ApiLinks.get_singleVehLocDataApi+vehid+"&PreviousUpdateTime="+PreviousUpdateTime;
        Log.e("UPLOAD_URL",UPLOAD_URL);
        //Showing the progress dialog
        //final ProgressDialog loading = ProgressDialog.show(activity,"Processing...","Please wait...",false,false);
        stringRequest = new StringRequest(Request.Method.GET, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        //loading.dismiss();
                        UPLOAD_URL="";
                        if (s.equalsIgnoreCase("NotFound")){
                            String requisitiontxt = MainActivity.context.getResources().getString(R.string.data_not_found_txt);
                            HelpingLib.showmessage(MainActivity.context,requisitiontxt);
                        }else{
                            try {
                                //Log.e("Location",s);
//                                JSONParser parser = new JSONParser();
                                rootObj = new JSONObject(s);
                                callBack.onSuccess(rootObj);
//                                Intent singleCarIntent=new Intent(LiveFragment.mActivity,RouteActivity.class);
//                                singleCarIntent.putExtra("carID",vehid);
//                                LiveFragment.mActivity.startActivity(singleCarIntent);
//
//                                singleCarIntent.putExtra("lat",lat);
//                                singleCarIntent.putExtra("lng",lng);
//

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        UPLOAD_URL="";
//                        NetworkResponse response = volleyError.networkResponse;
//                        int statuscode=response.statusCode;
                        Log.e("data","statuscode="+volleyError);
//
//                        Toast.makeText(MainActivity.activity, message, Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters


                //Log.e("data","location="+Utilities.latitude+"/"+Utilities.longitude);
                //returning parameters
                return params;
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                headers.put("charset", "utf-8");
//                return headers;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
        return rootObj;
    }

    public static  void getVehLocData(final Activity activity, final String vehid, final int pos )  {

        UPLOAD_URL=ApiLinks.get_singleVehLocDataApi+vehid;

        //Showing the progress dialog
//        final ProgressDialog loading = ProgressDialog.show(activity,"Processing...","Please wait...",false,false);
        stringRequest = new StringRequest(Request.Method.GET, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        //loading.dismiss();
                        UPLOAD_URL="";
                        if (s.equalsIgnoreCase("NotFound")){}else{
                            try {
                                //Log.e("Location",s);
                                JSONObject rootObj=new JSONObject(s);
                                getData (rootObj,  pos, activity);
//                                Intent singleCarIntent=new Intent(LiveFragment.mActivity,RouteActivity.class);
//                                singleCarIntent.putExtra("carID",vehid);
//                                LiveFragment.mActivity.startActivity(singleCarIntent);
//
//                                singleCarIntent.putExtra("lat",lat);
//                                singleCarIntent.putExtra("lng",lng);
//

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        //loading.dismiss();
                        UPLOAD_URL="";
//                        NetworkResponse response = volleyError.networkResponse;
//                        int statuscode=response.statusCode;
                        Log.e("data","statuscode="+volleyError);
//
//                        Toast.makeText(MainActivity.activity, message, Toast.LENGTH_LONG).show();
                    }
                }){


            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters


                //Log.e("data","location="+Utilities.latitude+"/"+Utilities.longitude);
                //returning parameters
                return params;
//                HashMap<String, String> headers = new HashMap<String, String>();
//                headers.put("Content-Type", "application/json");
//                headers.put("charset", "utf-8");
//                return headers;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public static void getData(JSONObject rootObj, int pos,Context context) {
        LatLng latLng = null;
        UserVehIocationListDataModel aUserVehLocObj = HandleUserVehLocInfoListJsonData.userVehLocListDataDB.get(pos);
        String registrationNumber=null,latitude=null,longitude=null,status=null;
        try {
            String PK_Vehicle = rootObj.getString("PK_Vehicle");
            String nearestMapLocation = rootObj.getString("NearestMapLocation");
            String nearestMapLocationDistance = rootObj.getString("NearestMapLocationDistance");
            latitude = rootObj.getString("Latitude");
            longitude = rootObj.getString("Longitude");
            String speed = rootObj.getString("Speed");
            updateTime = rootObj.getString("UpdateTime");
            String EngineStatus = rootObj.getString("EngineStatus");
            Course = rootObj.getString("Course");
            RouteActivity.newLatLng=new LatLng(Double.valueOf(latitude),Double.valueOf(longitude));

            if (EngineStatus.equalsIgnoreCase("0") && speed.equalsIgnoreCase("0")){
                status="0";
            }else if (EngineStatus.equalsIgnoreCase("1") && speed.equalsIgnoreCase("0")){
                status="1";
            }else if (EngineStatus.equalsIgnoreCase("1") && !speed.equalsIgnoreCase("0")){
                status="2";
            }
            //UserVehIocationListDataModel aUserVehLocObj = new UserVehIocationListDataModel(PK_Vehicle,nearestMapLocation,nearestMapLocationDistance,latitude,longitude,speed,updateTime,registrationNumber,internal_VehicleContactNumber,depoName,status);
            //HandleUserVehLocInfoListJsonData.userVehLocListDataDB.add(pos,aUserVehLocObj);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        latLng=new LatLng(Double.valueOf(latitude),Double.valueOf(longitude));
        MarkerOptions markerOptions=new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title(registrationNumber);
        // markerOptions.snippet();
        //markerOptions.flat(true).rotation(angle);
        if (status.equalsIgnoreCase("0")){
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( context.getResources(),
                            R.mipmap.trucklightred)));
        }else if (status.equalsIgnoreCase("1")){
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( context.getResources(),
                            R.mipmap.truckyellow)));
        }else if (status.equalsIgnoreCase("2")){
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( context.getResources(),
                            R.mipmap.truckgreen)));
        }else if (status.equalsIgnoreCase("-1")){
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( context.getResources(),
                            R.mipmap.icondarkred)));
        }else if (status.equalsIgnoreCase("-2")){
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(
                    BitmapFactory.decodeResource( context.getResources(),
                            R.mipmap.icondarkred)));
        }

    }

}
