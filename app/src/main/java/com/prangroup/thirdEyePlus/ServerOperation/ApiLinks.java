package com.prangroup.thirdEyePlus.ServerOperation;


public class ApiLinks {
    public static String MY_API_KEY = "AIzaSyBundcQx9I0SzOjywAEgJKMMUY4qK8HFvE";
    public static String base="http://3rdEye.prangroup.com:7698/";
//    public static String base = "http://103.206.184.122:7698/";
    public static String get_loginApi = base + "AccessAPI/Login";
    public static String get_regApi = base + "AccessAPI/EndLevelRequisitionAgent_PreCreate";
    public static String get_userVehListApi = base + "TrackingAPI/GetVehicleTrackingNear?PK_User=";
    public static String get_singleVehLocDataApi = base + "GetData_Single?PK_Vehicle=";
    public static String get_VehINApi = base + "VehicleGateNewAPI/GetGateIn_Utilities"; // VehINFragment   IN Both used
    public static String get_VehNumApi = base + "VehicleGateNewAPI/CheckVehicleForInOut?RegistrationNumber=";
   // public static String get_VehGetInApi = base + "VehicleGateNewAPI/GateIn_Test";
    public static String get_VehGetInApi = base + "VehicleGateNewAPI/GateIn";
    public static String get_VehGetInSearchApi = base + "VehicleGateNewAPI/FindVehicle?RegistrationNumber=";
    public static String get_VehExitApi = base + "VehicleGateNewAPI/GetGateOut_Utilities";//VehExitFragment used
   // public static String get_VehGetExitApi = base + "VehicleGateNewAPI/GateOut_Test";
    public static String get_VehGetExitApi = base + "VehicleGateNewAPI/GateOut";
    public static String get_VehExitManApi = base + "VehicleGateNewAPI/GetGateOutManual_Utilities?PK_User=";//VehExitManFragment used
    public static String get_VehGroupTypeApi = base + "VehicleGateNewAPI/GetCommon_Utilities";

    public static String get_UserControlOperationApi = base + "AccessAPI/RecheckUserActivityStatus?PK_User=";
//    public static String get_UserControlOperationApi = base + "AccessAPI/RecheckUserActivityStatus2?PK_User=";
    // public static String get_UserControlOperationApi = base +"AccessAPI/RecheckUserActivityStatus_Test?PK_User=";

    public static String base_visitor = "http://visitor.prangroup.com/";
//    public static String base_visitor = "https://visitor.prangroup.com/";
    public static String get_visitor_initial_info_api = base_visitor + "api/visitor/create/info/";
    public static String get_visitor_info_api = base_visitor + "api/visitor/info";
    public static String get_visitor_create_api = base_visitor + "api/visitor";
    public static String get_visitor_list_api = base_visitor + "api/visitors/";
    public static String get_visitor_exit_api = base_visitor + "api/visitor/leave";

    public static String base_load_unload = base + "LoadingBayUtilizationAPI/";
    public static String get_load_unload_utilities_search_api = base_load_unload + "GetLoadUnloadStart_Utilities";
//    public static String get_load_unload_utilities_search_api = base_load_unload + "GetLoadUnloadStart_Utilities2";
//    public static String get_load_unload_utilities_search_api = base_load_unload + "GetLoadUnloadStart_Utilities3";
    public static String get_load_unload_start_search_api = base_load_unload + "StartLoading";
    public static String get_load_unload_end_search_api = base_load_unload + "StartUnloading";

    public static String get_end_unload_search_api = base_load_unload + "GetVehiclesBeingUsed";
    public static String get_end_unload_api = base_load_unload + "EndLoadingUnloading";
}
