package com.prangroup.thirdEyePlus.ServerOperation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class LoadUnloadUtilitiesSearchOperation {
    private static String UPLOAD_URL;
    static StringRequest stringRequest;
    static int MY_SOCKET_TIMEOUT_MS = 10000;

    public static void getLoadUnloadStartUtilitiesSearchData(final Activity activity,
                                                             final String pkUser,
                                                             final VolleyCallBack volleyCallBack) {

        UPLOAD_URL = ApiLinks.get_load_unload_utilities_search_api + "?pk_user=" + pkUser;

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.GET, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        if (s.equalsIgnoreCase("{}")) {
                            String requisitiontxt = MainActivity.context.getResources().getString(R.string.data_not_found_txt);
                            HelpingLib.showmessage(MainActivity.context, requisitiontxt);
                        } else {
                            try {
                                JSONObject rootObj = new JSONObject(s);
                                volleyCallBack.onSuccess(rootObj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Toast.makeText(MainActivity.activity, "Error", Toast.LENGTH_LONG).show();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public static void getLoadUnloadStartSearchData(final Activity activity,
                                                    final int type,
                                                    final String version,
                                                    final String pkUser,
                                                    final String vehicle,
                                                    final String loadingBay,
                                                    final String parking,
                                                    final VolleyCallBack volleyCallBack) {

        if (type == 1) {
            UPLOAD_URL = ApiLinks.get_load_unload_start_search_api;
        } else {
            UPLOAD_URL = ApiLinks.get_load_unload_end_search_api;
        }

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        try {
                            JSONObject rootObj = new JSONObject(s);
                            volleyCallBack.onSuccess(rootObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Toast.makeText(MainActivity.activity, "Error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<>();

                //Adding parameters
                params.put("AppVersionCode", version);
                params.put("FK_StartByUser", pkUser);
                params.put("FK_Vehicle", vehicle);
                params.put("FK_LoadingBay", loadingBay);
                params.put("FK_ParkingInOut", parking);


                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public static void getLoadUnloadEndUtilitiesSearchData(final Activity activity,
                                                           final String pkUser,
                                                           final VolleyCallBack volleyCallBack) {

        UPLOAD_URL = ApiLinks.get_end_unload_search_api + "?pk_user=" + pkUser;

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.GET, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        if (s.equalsIgnoreCase("{}")) {
                            String requisitiontxt = MainActivity.context.getResources().getString(R.string.data_not_found_txt);
                            HelpingLib.showmessage(MainActivity.context, requisitiontxt);
                        } else {
                            try {
                                JSONObject rootObj = new JSONObject(s);
                                volleyCallBack.onSuccess(rootObj);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Toast.makeText(MainActivity.activity, "Error", Toast.LENGTH_LONG).show();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public static void getLoadUnloadEndSearchData(final Activity activity,
                                                    final String version,
                                                    final String loadingBay,
                                                    final String pkUser,
                                                    final VolleyCallBack volleyCallBack) {

        UPLOAD_URL = ApiLinks.get_end_unload_api;

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        try {
                            JSONObject rootObj = new JSONObject(s);
                            volleyCallBack.onSuccess(rootObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Toast.makeText(MainActivity.activity, "Error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<>();

                //Adding parameters
                params.put("AppVersionCode", version);
                params.put("PK_LoadingbayUtilization", loadingBay);
                params.put("FK_EndByUser", pkUser);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
