package com.prangroup.thirdEyePlus.ServerOperation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

public class VisitorInfoSearchOperation {

    private static String UPLOAD_URL;
    static StringRequest stringRequest;
    static int MY_SOCKET_TIMEOUT_MS = 10000;
//    static String location = "20F370CA-FE1B-4B7E-B7F5-A14FC89A279A";

    public static void sendVisitorInfoSearchData(final Activity activity,
                                                 final int type,
                                                 final String searchText,
                                                 final String locationTxt,
                                                 final VolleyCallBack volleyCallBack) {

        UPLOAD_URL = ApiLinks.get_visitor_info_api;

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.e("myData", s);
                        loading.dismiss();
                        try {
                            JSONObject rootObj = new JSONObject(s);
                            volleyCallBack.onSuccess(rootObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        //Log.e("data", "statuscode=" + volleyError);
                        Toast.makeText(MainActivity.activity, "Error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<>();

                //Adding parameters
                if (type == 1) {
                    params.put("staff_id", searchText);
                    params.put("location_id", locationTxt);
                } else {
                    params.put("mobile", searchText);
                    params.put("location_id", locationTxt);
                }

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public static void sendVisitorCreateData(final Activity activity,
                                             final int VisitorType,
                                             final String staffId,
                                             final String visitorName,
                                             final String visitorPhn,
                                             final String visitorComeFrom,
                                             final String visitorMeetTo,
                                             final int companyId,
                                             final int departmentId,
                                             final int purposeId,
                                             final String visitorCardNo,
                                             final String visitorPerson,
                                             final String visitorRemarks,
                                             final String imageLocation,
                                             final String locationTxt,
                                             final String userId,
                                             final String userData,
                                             final VolleyCallBack volleyCallBack) {

        UPLOAD_URL = ApiLinks.get_visitor_create_api;
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        //Log.e("myLog", s);
                        loading.dismiss();
                        try {
                            JSONObject rootObj = new JSONObject(s);
                            volleyCallBack.onSuccess(rootObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Log.e("myLog", "statuscode=" + volleyError);
                        Toast.makeText(MainActivity.activity, "Error", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<>();

                //Adding parameters
                params.put("visitor_type", "" + VisitorType);
                params.put("staff_id", staffId);
                params.put("name", visitorName);
                params.put("mobile", visitorPhn);
                params.put("from", visitorComeFrom);
                params.put("meet_with", visitorMeetTo);
                params.put("company_id", "" + companyId);
                params.put("department_id", "" + departmentId);
                params.put("visitor_purpose_id", "" + purposeId);
                params.put("card_no", visitorCardNo);
                params.put("person_no", visitorPerson);
                params.put("remarks", visitorRemarks);
                params.put("image", imageLocation);
                params.put("location_id", locationTxt);
                params.put("created_by", userId);
                params.put("visitor_create_auth", userData);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
