package com.prangroup.thirdEyePlus.ServerOperation;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.prangroup.thirdEyePlus.Default.MainActivity;
import com.prangroup.thirdEyePlus.Fragments.MenuFragment;
import com.prangroup.thirdEyePlus.Fragments.VisitorExitFragment;
import com.prangroup.thirdEyePlus.Model.VisitorExitListDataModel;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;
import com.prangroup.thirdEyePlus.interfaces.VolleyCallBack;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;

public class VisitorExitSearchOperation {

    private static String UPLOAD_URL;
    static StringRequest stringRequest;
    static int MY_SOCKET_TIMEOUT_MS = 10000;

    public static void sendVisitorSearchData(final Activity activity, final String locationTxt) {

        UPLOAD_URL = ApiLinks.get_visitor_list_api + locationTxt;
        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.GET, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        loading.dismiss();
                        Log.e("myLog", "statuscode=" + s);
                        if (s.equalsIgnoreCase("[]")) {
                            String requisitiontxt = MainActivity.context.getResources().getString(R.string.data_not_found_txt);
                            HelpingLib.showmessage(MainActivity.context, requisitiontxt);
                        } else {
                            try {
                                MenuFragment.visitorListData.clear();

                                JSONArray rootArray = new JSONArray(s);

                                for (int i = 0; i < rootArray.length(); i++) {
                                    JSONObject obj = rootArray.getJSONObject(i);

                                    String id = obj.getString("id");
                                    String name = obj.getString("name");
                                    String phn = obj.getString("mobile");
                                    String in_time = obj.getString("in_time");
                                    String out_time = obj.getString("out_time");
                                    String image = obj.getString("image_location");
                                    VisitorExitListDataModel dataModel = new VisitorExitListDataModel(Integer.parseInt(id), name, phn, in_time, out_time, image);
                                    MenuFragment.visitorListData.add(dataModel);
                                }
                                Fragment fragment = new VisitorExitFragment();
                                MainActivity.fragmentManager.beginTransaction()
                                        .replace(R.id.frameLayout, fragment, null)
                                        .commit();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Log.e("data", "statuscode=" + volleyError);
//                        Toast.makeText(MainActivity.activity, message, Toast.LENGTH_LONG).show();
                    }
                });

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

    public static void sendVisitorExitData(final Activity activity,
                                           final int position,
                                           final String locationTxt,
                                           final String userId,
                                           final VolleyCallBack volleyCallBack) {

        UPLOAD_URL = ApiLinks.get_visitor_exit_api;

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(activity, "Processing...", "Please wait...", false, false);
        stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {
                        Log.e("myData", s);
                        loading.dismiss();
                        try {
                            JSONObject rootObj = new JSONObject(s);
                            volleyCallBack.onSuccess(rootObj);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
                        Log.e("data", "statuscode=" + volleyError);
                        Toast.makeText(MainActivity.activity, "message", Toast.LENGTH_LONG).show();
                    }
                }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String, String> params = new Hashtable<>();

                //Adding parameters
                params.put("visitor_id", "" + MenuFragment.visitorListData.get(position).getId());
                params.put("location_id", locationTxt);
                params.put("leave_by", userId);

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(activity);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }
}
