package com.prangroup.thirdEyePlus.ServerOperation;

import android.app.ProgressDialog;
import android.content.Intent;
import android.util.Log;
import android.view.View;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.prangroup.thirdEyePlus.Auth.RegistrationActivity;
import com.prangroup.thirdEyePlus.R;
import com.prangroup.thirdEyePlus.Utilities.HelpingLib;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Hashtable;
import java.util.Map;


/**
 * Created by droid on 10/9/2016.
 */
public class UserRegistrationOperation {
    private final static String UPLOAD_URL = ApiLinks.get_regApi;
    static StringRequest stringRequest;
    static String message;
    static Intent loginsuc;
    static int MY_SOCKET_TIMEOUT_MS=10000;
    public static  void sendMobData(){

        //Showing the progress dialog
        final ProgressDialog loading = ProgressDialog.show(RegistrationActivity.context,"Processing...","Please wait...",false,false);
        stringRequest = new StringRequest(Request.Method.POST, UPLOAD_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String s) {

                        loading.dismiss();
                        try {
                            JSONObject rootObj=new JSONObject(s);
                            String status=rootObj.getString("status");
                            if (status.equalsIgnoreCase("OK")){
                                RegistrationActivity.linlayoutmob.setVisibility(View.GONE);
                                RegistrationActivity.linlayoutotp.setVisibility(View.VISIBLE);
                                RegistrationActivity. fid = FirebaseInstanceId.getInstance().getToken();
                                RegistrationActivity.otpdb=rootObj.getString("OTP");


                            }else{
                                String requisitiontxt = RegistrationActivity.context.getResources().getString(R.string.bid_conf_fail_txt);
                                HelpingLib.showmessage(RegistrationActivity.context,requisitiontxt);//login fail
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError volleyError) {
                        //Dismissing the progress dialog
                        loading.dismiss();
//                        NetworkResponse response = volleyError.networkResponse;
//                        int statuscode=response.statusCode;
                        Log.e("data","statuscode="+volleyError);
//
//                        Toast.makeText(MainActivity.activity, message, Toast.LENGTH_LONG).show();
                    }
                }){
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                //Converting Bitmap to String

                //Creating parameters
                Map<String,String> params = new Hashtable<String, String>();

                //Adding parameters
                params.put("ContactNumber", RegistrationActivity.mobNo);
                //Log.e("data","location="+Utilities.latitude+"/"+Utilities.longitude);
                //returning parameters
                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                MY_SOCKET_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        //Creating a Request Queue
        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.context);

        //Adding request to the queue
        requestQueue.add(stringRequest);
    }

}
