package com.prangroup.thirdEyePlus.interfaces;

import org.json.JSONObject;

public interface VolleyCallBack {
    void onSuccess(JSONObject getVchLocData);
}